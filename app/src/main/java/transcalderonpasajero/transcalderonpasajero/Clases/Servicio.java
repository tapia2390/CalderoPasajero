package transcalderonpasajero.transcalderonpasajero.Clases;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import transcalderonpasajero.transcalderonpasajero.R;
import transcalderonpasajero.transcalderonpasajero.sistema.*;
import transcalderonpasajero.transcalderonpasajero.sistema.Utility;

/**
 * Created by tactomotion on 2/08/16.
 */
public class Servicio {

    public String origen;    //ciudad origen
    public String destino;      //ciudad destino

    public String fechaEnOrigen;
    public String horaEnOrigen;
    public String fechaEnDestino;
    public String horaEnDestino;

    public String direccionOrigen;

    public Ubicacion ubiOrigen;
    public Ubicacion ubiDestino;

    public boolean modoResureccion = false;
    public boolean yaCalificado = false;


    public String id;
    public String estado;
    public String ultimoEstado= "";
    public String asignadoPor;
    public String idConductor;
    public Conductor conductor = new Conductor();
    public String cosecutivoOrden;
    public String fechaGeneracion;
    public String horaGeneracion;
    public String hora;
    public String direccionDestino;
    public String idCliente;
    public String matricula;
    public String ruta = "";
    public String solicitadoPor;
    public String tarifa;
    public Long timeStamp;
    public String observaciones;
    public ArrayList<String> trayectos = new ArrayList<>();

    public boolean abierto = false;
    public String horasServicio = "";
    public String diasServicio = "";
    public boolean indefinido = false;
    public String tipoVehiculo = "Sin preferencia";

    public ArrayList<Pasajero> pasajerosEmp = new ArrayList<>();
    public ArrayList<Pasajero> pasajerosPart = new ArrayList<>();


    public ArrayList<Mensajes> notificaciones = new ArrayList<Mensajes>();


    public ArrayList<Calificacion> calificaciones = new ArrayList<Calificacion>();
    Modelo modelo = Modelo.getInstance();
    public ArrayList<UbicacionPasajero> ubicacionPasajeroGPsses = new ArrayList<UbicacionPasajero>();

    public Servicio(){

    }


    public boolean origenEstaFull(){
        if (origen == null || origen.equals("")){
            return false;
        }

        if (direccionOrigen == null || direccionOrigen.equals("")){
            return false;
        }

        if (ubiOrigen == null || ubiOrigen.longitud == null || ubiOrigen.longitud == 0){
            return false;
        }

        return true;

    }


    public boolean destinoEstaFull(){
        if (destino == null || destino.equals("")){
            return false;
        }

        if (direccionDestino == null || direccionDestino.equals("")){
            return false;
        }

        if (ubiDestino == null || ubiDestino.longitud == null || ubiDestino.longitud == 0){
            return false;
        }

        return true;


    }



    //Quita todo menos dirOrigen, CiudadOrigen, y ubicacion.
    public void reset(){
        ubiDestino = null;
        direccionDestino = null;
        abierto = false;
        conductor = new Conductor();
        pasajerosEmp.clear();
        pasajerosPart.clear();
        observaciones = null;
        ruta = "";
        horasServicio = "";
        diasServicio = "";
        indefinido = false;
        tipoVehiculo = "Sin preferencia";
        modoResureccion = false;
        destino = origen;

    }


    public ArrayList<Pasajero> getFullPasajeros(){
        ArrayList<Pasajero> res = new ArrayList<>();
        for (Pasajero pasajero: pasajerosEmp){
            res.add(pasajero);
        }

        for (Pasajero pasajero: pasajerosPart){
            res.add(pasajero);
        }
        return res;

    }


    public boolean hasSetParadasAdicionales(){
        if (getRuta().equals("")){
            return false;
        }
        return true;
    }


    public boolean hasSetVehiculo(){
        if (!tipoVehiculo.equals("Sin preferencia")){
            return  true;
        }

        return false;
    }

    public boolean hasPasajeros(){
        if (pasajerosPart.size() > 0 || pasajerosEmp.size() > 0 ){
            return true;
        }
        return false;
    }


    public boolean hasObservaciones(){

        if (observaciones != null ) {
            return true;
        }
        return false;
    }

    public boolean hasConductor(){
        if (conductor.nombre.equals("")){
            return false;
        }

        return true;

    }



    public boolean faltaConfigurarAbierto(){
        if (!abierto){
            return  false;
        }

        if (indefinido){
            return false;
        }

        if (horasServicio.equals("") && diasServicio.equals("")) {
            return true;
        }

        return  false;


    }

    public String getValorAproximado(){
        if (abierto) {
            return "";
        }

        if (direccionOrigen == null || direccionDestino == null){
            return  "";
        }

        if (direccionDestino.equals("") || direccionOrigen.equals("")){
            return  "";
        }

        if (ubiOrigen == null || ubiDestino == null) {
            return  "";
        }

        if (ubiOrigen.longitud == 0 || ubiDestino.longitud == 0 ){
            return  "";
        }


        float distancia = Utility.getDistancia(ubiDestino, ubiOrigen);

        float valor = modelo.params.precioKilometro * distancia/1000;

        return "Tarifa estimada: " + Utility.convertToMoney((int)valor);
    }


    public String getDuracionCadena() {



        if (indefinido ){
            return  "Duración indefinida";
        }

        if (!horasServicio.equals("")){
            return "Duración " + horasServicio + " horas";
        }

        if (!diasServicio.equals("")){
            return "Duración " + diasServicio + " días";
        }

        return "";

    }


    public String getRuta(){

        String res = "";

        for (String tray : trayectos){
            if (res.equals("")){
                res = tray;
            }
            else {
                res = res + "-" + tray;
            }
        }
        return res;

    }




    public void setTimeStamp(Long timeStamp){
        this.timeStamp = timeStamp;
    }

    public Long getTimeStamp(){
        return this.timeStamp;
    }

    public void setOrigen(String origen){
        this.origen = origen;
    }

    public String getOrigen( ){
        return this.origen;
    }

    public void setEstado(String estado){
        this.estado = estado;
    }

    public String getEstado( ){
        return this.estado;
    }

    public void setDestino(String destino){
        this.destino = destino;
    }

    public String getDestino( ){
        return this.destino;
    }

    public String getDireccionDestino() {
        return direccionDestino;
    }

    public void setDireccionDestino(String direccionDestino) {
        this.direccionDestino = direccionDestino;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public void setFechaEnOrigen(String fechaEnOrigen){
        this.fechaEnOrigen = fechaEnOrigen;
    }

    public String getFechaEnOrigen(){
        return this.fechaEnOrigen;
    }





    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }


    public void setAsignadoPor(String asignadoPor){
        this.asignadoPor = asignadoPor;
    }

    public String getAsignadoPor(){
        return this.asignadoPor;
    }




    public void setCosecutivoOrden(String cosecutivoOrden){
        this.cosecutivoOrden = cosecutivoOrden;
    }

    public String getCosecutivoOrden(){
        return this.cosecutivoOrden;
    }

    public void setDireccionOrigen(String direccionOrigen){
        this.direccionOrigen = direccionOrigen;
    }

    public String getDireccionOrigen(){
        return this.direccionOrigen;
    }

    public String getIdConductor() {
        return idConductor;
    }

    public void setIdConductor(String idConductor) {
        this.idConductor = idConductor;
    }

    public void setFechaEnDestino(String fechaEnDestino){
        this.fechaEnDestino = fechaEnDestino;
    }

    public String getFechaEnDestino(){
        return this.fechaEnDestino;
    }


    public void setHoraEnDestino(String horaEnDestino){
        this.horaEnDestino = horaEnDestino;
    }

    public String getHoraEnDestino(){
        return this.horaEnDestino;
    }

    public void setHoraGeneracion(String horaGeneracion){
        this.horaGeneracion = horaGeneracion;
    }

    public String getHoraGeneracion(){
        return this. horaGeneracion;
    }

    public void setIdCliente(String idCliente){
        this.idCliente = idCliente;
    }

    public String getIdCliente(){
        return this.idCliente;
    }
    public void setMatricula(String matricula){
        this.matricula = matricula;
    }

    public String getMatricula(){
        return this.matricula;
    }



    public void setSolicitadoPor(String solicitadoPor){
        this.solicitadoPor = solicitadoPor;
    }

    public String getSolicitadoPor(){
        return this.solicitadoPor;
    }

    public void setTarifa(String tarifa){
        this.tarifa = tarifa;
    }

    public String getTarifa(){
        return this.tarifa;
    }



    //metodo para mostrar una sola  orden segun el id de la orden
    public Mensajes getNotificaciones(String idMensaje){
        Iterator<Mensajes> iterator = notificaciones.iterator();
        while (iterator.hasNext()) {
            Mensajes notificacion = iterator.next();
            if (notificacion.getId().equals(idMensaje)) {
                return  notificacion;
            }
        }
        return null;
    }


    //metodo para evitar item repetidos
    public void adicionarNuevoMensaje(Mensajes nuevoMensaje){
        Iterator<Mensajes> iterator = notificaciones.iterator();
        while (iterator.hasNext()){
            Mensajes  notificacion = iterator.next();
            if(notificacion.getId().equals(nuevoMensaje.getId())){
                iterator.remove();
            }
        }
        notificaciones.add(nuevoMensaje);
    }


    public Calificacion getCalificacion(String idNotificacion){
        Iterator<Calificacion> iterator = calificaciones.iterator();
        while (iterator.hasNext()) {
            Calificacion notificacion = iterator.next();
            if (notificacion.getId().equals(idNotificacion)) {
                return  notificacion;
            }
        }
        return null;
    }

    public int contarMisCalificaciones(){


        try{
            if(calificaciones == null){
                return 0;
            }
            int contador =0;
            for(int i=0; i< calificaciones.size(); i++ ){

                if(calificaciones.get(i).getPasajero().equals(modelo.uid)){
                    contador = contador+1;
                }
            }
            return contador;

        }catch (Exception e){
            return 0;
        }
    }

    public Calificacion getMiCalificacion(){

        for(int i=0; i< calificaciones.size(); i++ ){

            if(calificaciones.get(i).getPasajero().equals(modelo.uid)){
                return calificaciones.get(i);
            }
        }
        return null;
    }


    public UbicacionPasajero getUbicaionGps(String uid){
        Iterator<UbicacionPasajero> iterator = ubicacionPasajeroGPsses.iterator();
        while (iterator.hasNext()) {
            UbicacionPasajero ubicacionPasajero = iterator.next();
            if (ubicacionPasajero.getPasajero().equals(uid)) {
                return ubicacionPasajero;
            }
        }
        return null;
    }


    //metodo para evitar item repetidos
    public void adicionarUbicaionGps(UbicacionPasajero nuevaUbicaionGps){
        Iterator<UbicacionPasajero> iterator = ubicacionPasajeroGPsses.iterator();
        while (iterator.hasNext()){
            UbicacionPasajero ubicacionPasajero = iterator.next();
            if(ubicacionPasajero.getPasajero().equals(nuevaUbicaionGps.getPasajero())){
                iterator.remove();
            }
        }
        ubicacionPasajeroGPsses.add(nuevaUbicaionGps);
    }

    public String getEstadoLeible(){
        if (estado.equals("SinConfirmar") || estado.equals("NoAsignado") || estado.equals("Cotizar")){
            return  "No asignado";
        }
        if (estado.equals("Asignado") ){
            return  "Confirmado";
        }

        if (estado.equals("En Camino") ){
            return  "En camino";
        }

        if (estado.equals("Transportando") ){
            return  "Transportando";
        }

        if (estado.equals("Finalizado") ){
            return  "Finalizado";
        }
        if ((estado.equals("Anulado")) || (estado.equals("Cancelado")  )){
            return  "Cancelado";
        }

        return "Desconocido";

    }

    public String getListaPasajeros(String tipo){
        if (tipo.equals("EMPRESARIAL")){
            return  getListaPasajerosEmp();
        }
        return  getListaPasajerosPart();

    }

    public String getListaPasajerosCompleta(){
        String part  = getListaPasajerosPart();
        if (part.equals("")) {
            return  getListaPasajerosEmp();
        }
        String emp  = getListaPasajerosEmp();
        if (emp.equals("")) {
            return  getListaPasajerosPart();
        }

        return  part + ", " + emp;
    }

    public String getListaPasajerosEmp(){

        String res = "";
        for (Pasajero pas: pasajerosEmp) {
            if (res.equals("")) {
                res = pas.getNombre() + " " + pas.getApellido();
            } else {
                res = res + ", " + pas.getNombre() + " " + pas.getApellido();
            }
        }
        return  res;

    }

    public String getListaPasajerosPart(){

        String res = "";
        for (Pasajero pas: pasajerosPart) {
            if (res.equals("")) {
                res = pas.getNombre() + " " + pas.getApellido();
            } else {
                res = res + ", " + pas.getNombre() + " " + pas.getApellido();
            }
        }
        return  res;

    }

    public void addEliminarPasajero(String tipo, Pasajero pasajero){

        if (tipo.equals("EMPRESARIAL")){

            int index = getIndexPasajeroEmp(pasajero);
            if (index  >= 0 ) {
                pasajerosEmp.remove(index);
                return;
            }

            pasajerosEmp.add(pasajero);

        }

        if (tipo.equals("PARTICULAR")){

            int index = getIndexPasajeroPart(pasajero);
            if (index  >= 0 ) {
                pasajerosPart.remove(index);
                return;
            }

            pasajerosPart.add(pasajero);

        }

    }

    public void addEliminarTrayecto(String trayecto){


            int index = getIndexTrayecto(trayecto);
            if (index  >= 0 ) {
                trayectos.remove(index);
                return;
            }

        trayectos.add(trayecto);



    }

    private int getIndexTrayecto(String trayecto) {
        int index = 0;
        for (String tray: trayectos) {
            if (tray.equals(trayecto) ){
                return index ;
            }
            index++;
        }
        return  -1;
    }



    private int getIndexPasajeroEmp(Pasajero pasajero) {
        int index = 0;
        for (Pasajero pas: pasajerosEmp) {
            if (pas.uid.equals(pasajero.uid) ){
                return index ;
            }
            index++;
        }
        return  -1;
    }

    private int getIndexPasajeroPart(Pasajero pasajero) {
        int index = 0;
        for (Pasajero pas: pasajerosPart) {
            if (pas.uid.equals(pasajero.uid) ){
                return index ;
            }
            index++;
        }
        return  -1;
    }

    public void eliminarPasajeros(){
        pasajerosEmp.clear();
        pasajerosPart.clear();;
    }


    public void eliminarTrayectos(){
        trayectos.clear();
    }

    public boolean hasDestinoDefinido(){

        if (direccionDestino == null){
            return false;
        }

        if (destino.equals("ABIERTO") && getDuracionCadena().equals("")) {
            return false;
        }

        if (!destino.equals("ABIERTO") && direccionDestino.equals("")){
            return false;
        }
        return true;
    }








}
