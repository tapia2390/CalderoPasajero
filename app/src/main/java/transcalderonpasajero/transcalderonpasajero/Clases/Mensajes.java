package transcalderonpasajero.transcalderonpasajero.Clases;

import java.util.Date;

/**
 * Created by tactomotion on 10/10/16.
 */
public class Mensajes {

    String id;
    String fecha;
    String hora;
    String texto;
    Long timestamp;
    String estado;

    public Mensajes(){

    }

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setFecha(String fecha){
        this.fecha = fecha;
    }

    public String getFecha(){
        return this.fecha;
    }

    public void setHora(String hora){
        this.hora = hora;
    }

    public String getHora(){
        return this.hora;
    }

    public void setTexto(String texto){
        this.texto = texto;
    }

    public String getTexto(){
        return this.texto;
    }

    public void setTimestamp(Long timestamp){
        this.timestamp = timestamp;
    }

    public Long getTimestamp(){
        return this.timestamp;
    }


    public void setEstado(String estado){
        this.estado = estado;
    }

    public String getEstado(){
        return this.estado;
    }

}
