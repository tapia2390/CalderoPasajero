package transcalderonpasajero.transcalderonpasajero.Clases;

import java.util.ArrayList;

/**
 * Created by tacto on 7/10/17.
 */

public class Empresa {
    public  String id = "";
    public boolean activo = false;
    public  String celular = "";
    public  String consecutivoOrden = "";
    public  String correo = "";
    public  String direccion = "";
    public  String inicialesConsecutivo = "";
    public  String logo = "";
    public  String nit = "";
    public  String nombreContacto = "";
    public  String razonSocial = "";
    public  boolean exclusivoMujer = false;
    public int horasRespuesta = 10;
    public boolean permitirAutoRegistro = false;
    public ArrayList<Vehiculos> tiposVehiculo = new ArrayList<Vehiculos>();
    public ArrayList<Pasajero> empresariales = new ArrayList<>();



    public boolean puedePedirYa(){

        if (horasRespuesta == 0 ){
            return true;
        }
        return false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getConsecutivoOrden() {
        return consecutivoOrden;
    }

    public void setConsecutivoOrden(String consecutivoOrden) {
        this.consecutivoOrden = consecutivoOrden;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getInicialesConsecutivo() {
        return inicialesConsecutivo;
    }

    public void setInicialesConsecutivo(String inicialesConsecutivo) {
        this.inicialesConsecutivo = inicialesConsecutivo;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getNombreContacto() {
        return nombreContacto;
    }

    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }


    public Pasajero getPasajeroEmpresarialById(String id){

        for (Pasajero pas : empresariales){

            if (pas.uid.equals(id)){
                return  pas;
            }
        }
        return  null;
    }


}
