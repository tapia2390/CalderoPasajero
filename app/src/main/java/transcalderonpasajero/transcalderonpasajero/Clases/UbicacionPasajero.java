package transcalderonpasajero.transcalderonpasajero.Clases;

/**
 * Created by tacto on 7/07/17.
 */

public class UbicacionPasajero {

    double lat;
    double lon;
    String pasajero;
    Long timestamp;
    String pathUbicacion;


    public UbicacionPasajero(){

    }


    public void setLat(double lat){
        this.lat = lat;
    }

    public double getLat(){
        return this.lat;
    }


    public void setLon(double lon){
        this.lon = lon;
    }

    public double getLon(){
        return this.lon;
    }

    public void setPasajero(String pasajero){
        this.pasajero = pasajero;
    }

    public String getPasajero(){
        return this.pasajero;
    }

    public void setTimestamp(Long timestamp){
        this.timestamp = timestamp;
    }

    public Long getTimestamp(){
        return this.timestamp;
    }


    public void setPathUbicacion(String pathUbicacion){
        this.pathUbicacion = pathUbicacion;
    }

    public String getPathUbicacion(){
        return this.pathUbicacion;
    }



}

