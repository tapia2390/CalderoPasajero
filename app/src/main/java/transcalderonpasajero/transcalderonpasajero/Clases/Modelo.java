package transcalderonpasajero.transcalderonpasajero.Clases;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.WindowManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import transcalderonpasajero.transcalderonpasajero.Comandos.CmdEmpresa;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdEmpresa.OnEmpresaListener;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoOrdenesPasajeroUltimosDiez;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoPasajero;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoPasajero.OnPasajeroChangeListener;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoValidarUsuario;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoValidarUsuario.OnOValidarUsuarioChangeListener;
import transcalderonpasajero.transcalderonpasajero.MainActivity;
import transcalderonpasajero.transcalderonpasajero.R;
import transcalderonpasajero.transcalderonpasajero.Servicios;
import transcalderonpasajero.transcalderonpasajero.home.NewHomeNav;

/**
 * Created by tactomotion on 27/07/16.
 */
public class Modelo  implements  ComandoOrdenesPasajeroUltimosDiez.OnOrdenesPasajeroHistorialChangeListener{
    private static Modelo ourInstance = new Modelo();


    ComandoOrdenesPasajeroUltimosDiez comandoOrdenesPasajeroUltimosDiez = new ComandoOrdenesPasajeroUltimosDiez(this);

    public Pasajero pasajero = new Pasajero();
    public Conductor conductor = new Conductor();

    private Modelo.OnModeloChangelistener mListener;
    public String uid ="";
    public String idUltimoServicioFinalizado ="";

    public String validaUsuario ="";
    //para utilizar en la seleccion de fechas
    public String[] fechas = {"Hoy", "Mañana", "Pasado Mañana"};
    private SimpleDateFormat df = new SimpleDateFormat("EEEE, MMMM dd");
    public SimpleDateFormat dfull = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
    public SimpleDateFormat dfull2 = new SimpleDateFormat("dd/MM/yyyy hh:mm");
    public SimpleDateFormat dfsimple = new SimpleDateFormat("dd/MM/yyyy");

    public int selectPosition = -1;
    public int selectPositionAnterior = 0;
    public boolean selectImge = false;
    public int notificacacion  =0;

    public Params params = new Params();

    public int catidadOrdenes =0;
    public  boolean appcerradaAbierta = false;
    public double latitud =0.0;
    public double longitud =0.0;
    public String idGpsUbicacion  = "";
    //public Ciudad cDestino, cOrigen = null;
    public String lastCiudad = "ORIGEN";
    public Servicio currentService = new Servicio();
    public Empresa empresa = new Empresa();
    public ArrayList<Servicio> servicios = new ArrayList<>();
    public ArrayList<Servicio> serviciosHistorial = new ArrayList<>();
    public ArrayList<Vehiculos> listavehiculos = new ArrayList<Vehiculos>();


    public HashMap<Query, ChildEventListener> hijosListener = new HashMap<>();
    public HashMap<Query, ValueEventListener> hijosListenerValue = new HashMap<>();
    public HashMap<DatabaseReference, ChildEventListener> hijosListenerRef = new HashMap<>();
    public Context context;



    public static Modelo getInstance() {
        return ourInstance;
    }

    private Modelo() {
    }


    public void setModeloListener(OnModeloChangelistener mListener){

        this.mListener  = mListener;
    }

    //inicio carga las ordendes del servicio pasajero
    private ArrayList<Servicio> ordenes = new ArrayList<Servicio>();
    private ArrayList<Servicio> viejitas = new ArrayList<Servicio>();

    private ArrayList<Servicio> historial = new ArrayList<Servicio>();
    public ArrayList<Servicio> filtrohistorialmes = new ArrayList<Servicio>();
    public ArrayList<Empresa> clientesEmpresases = new ArrayList<Empresa>();

    public ArrayList<Ciudad> ciudades = new ArrayList<Ciudad>();
    public List<Ciudad> data= new ArrayList<Ciudad>();

    public List<Pasajero> dataPasajerosE= new ArrayList<Pasajero>();



    public int sinCalificar(){

        int i = 0;
        for (Servicio serv : viejitas){
            if (!serv.yaCalificado){
                i++;
            }
        }
        return i;
    }

    ///Return true si hubo cambio de estado
    public boolean addServicio(Servicio servicio) {

        int index = getIndexServicio(servicio);
        if (index  >= 0 ) {
            boolean cambioEstado = false;
            if (!servicio.estado.equals(servicios.get(index).estado) ){
                cambioEstado = true;
            }

            servicios.set(index,servicio);
            return cambioEstado;
        }

        servicios.add(servicio);
        return false;

    }

    ///Return true si hubo cambio de estado
    public boolean addServicioViejitas(Servicio servicio) {

        int index = getIndexViejitas(servicio);
        if (index  >= 0 ) {
            boolean cambioEstado = false;
            if (!servicio.estado.equals(viejitas.get(index).estado) ){
                cambioEstado = true;
            }

            viejitas.set(index,servicio);
            return cambioEstado;
        }

        viejitas.add(servicio);
        return false;

    }



    ///Return true si hubo cambio de estado
    public boolean addServicioH(Servicio servicio) {

        int index = getIndexServicio(servicio);
        if (index  >= 0 ) {
            boolean cambioEstado = false;
            if (!servicio.estado.equals(servicios.get(index).estado) ){
                cambioEstado = true;
            }

            servicios.set(index,servicio);
            return cambioEstado;
        }

        serviciosHistorial.add(servicio);
        return false;

    }


    private int getIndexServicio(Servicio servicio) {
        int index = 0;
        for (Servicio serv: servicios) {
            if (serv.id.equals(servicio.id) ){
                return index ;
            }
            index++;
        }
        return  -1;
    }




    private int getIndexViejitas(Servicio servicio) {
        int index = 0;
        for (Servicio serv: viejitas) {
            if (serv.id.equals(servicio.id) ){
                return index ;
            }
            index++;
        }
        return  -1;
    }



    //guardamos las ordenes en arrarlist
    public ArrayList<Servicio> getOrdenes(){
        return ordenes;
    }


    //metodo para mostrar una sola  orden segun el id de la orden
    public Servicio getOrdenById(String idOrden){
        Iterator<Servicio> iterator = servicios.iterator();
        while (iterator.hasNext()) {
            Servicio orden = iterator.next();
            if (orden.getId().equals(idOrden)) {
                return  orden;
            }
        }
        return null;
    }
    //fin carga de las ordenes

    //inicio carga las ordendes historial del servicio pasajero


    public void adicionarNuevaOrdenHistorial(Servicio nuevaOrdenHistorial){
        Iterator<Servicio> iterator = historial.iterator();
        while (iterator.hasNext()){
            Servicio orden = iterator.next();
            if(orden.getId().equals(nuevaOrdenHistorial.getId())){
                iterator.remove();
            }
        }
        historial.add(nuevaOrdenHistorial);
    }


    public ArrayList<Servicio> getHistorial(){
        return historial;
    }

    public Servicio getOrdenHistorial(String idOrden){


        Iterator<Servicio> iterator = historial.iterator();
        while (iterator.hasNext()) {
            Servicio orden = iterator.next();
            if (orden.getId().equals(idOrden)) {
                Log.v("orden","orden"+orden);
                return  orden;

            }
            Log.v("orden","orden"+orden);
        }
        return null;

    }

    public Servicio getOrdenHistorialNuevo(String idOrden){

        for (int i= 0; i<getHistorial().size(); i++ ){
            if(idOrden.equals(historial.get(i).getId())){
                return historial.get(i);
            }
        }
        return null ;
    }

    //metodo para cargar los  ultimos 10 del historial
    public void filtrarUltimosDiezHistorial(){

        Collections.sort(historial, new Comparator<Servicio>() {
            @Override
            public int compare(Servicio lhs, Servicio rhs) {
                return ((Long) lhs.getTimeStamp()).compareTo(rhs.getTimeStamp());
            }
        });

        Collections.reverse(historial);
        //recorremos el tamaño del historail hasta 10 Error
        while (historial.size() > 10) {
            historial.remove(historial.size()-1);
        }
    }



    //fin carga de las ordenes historial

    //Utilitarios
    /**
     * Convierte la fecha de String a tipo Datime
     * Tiene en cuenta que el texto puede ser Hoy, Mañana, Pasado Mañana
     * @param valor
     */
    public DateTime convertToFecha(String valor) {

        if (valor.equals(fechas[0])) {
            return DateTime.now();
        } else if (valor.equals(fechas[1])) {
            return DateTime.now().plusDays(1);
        } else if (valor.equals(fechas[2])) {
            return DateTime.now().plusDays(2);
        }

        DateTimeFormatter formatter = DateTimeFormat.forPattern("EEEE, MMMM dd");
        DateTime dt = formatter.parseDateTime(valor);

        return  dt;
    }

    /**
     * MEtodo que dada una fecha la convierte al formato hoy , mañana y pasado mañana o EEE MMMM dd
     * @return
     */
    public String formatearFecha(Date f)
    {


        if (df.format(f).equals(df.format(new Date())))
        {
            return fechas[0];
        }

        GregorianCalendar gc = new GregorianCalendar();
        gc.add(Calendar.DATE, 1);

        if (df.format(f).equals(df.format(gc.getTime())))
        {
            return fechas[1];
        }

        gc.add(Calendar.DATE, 1);
        if (df.format(f).equals(df.format(gc.getTime())))
        {
            return fechas[2];
        }

        return df.format(f);

    }



    //metodo para filtrar mes y año y razon social

    public  void filtrarPorFechaYCliente(String fechaResivida){


        /// String fechaResivida ="01/"+fecha;
        Date fechas = null;
        try {
            fechas = dfsimple.parse(fechaResivida);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        filtrohistorialmes.clear();
        Iterator<Servicio> iterator = historial.iterator();
        while (iterator.hasNext()) {
            Servicio orden = iterator.next();
            String fechaOrigen = orden.getFechaEnOrigen();

            try {
                Date fechaOrden = null;
                fechaOrden = dfsimple.parse(fechaOrigen);

                if (fechas.getYear()==fechaOrden.getYear() && fechas.getMonth()== fechaOrden.getMonth()){
                    filtrohistorialmes.add(orden);
                }
            }catch (ParseException ex) {
                ex.printStackTrace();
            }
            //filtrohistorialmes.add(orden);
        }

    }

    @Override
    public void cargoUnaOrdenesPasajeroHistorial() {

        // mListener.setActualizoListadoDeServiciosHistoricos();



    }

    @Override
    public void cargoHisTorialP() {

    }





    public interface OnModeloChangelistener {

        void setActualizoListadoDeServicios();

        void setActualizoListadoDeServiciosHistoricos();
    }

    public void llamarServicios(){

        //cmdOrden.getTodasLasOrdenesPasajero();
        //cmdOrden.validarUsuario();

    }





    public void llamarServiciosHistorial(){

        comandoOrdenesPasajeroUltimosDiez.getTodasLasOrdenesPasajeroHistorial();
    }



    //bd
    public void procesarNotificacionCambioDeEstado(){

    }


    public boolean existeNit(String nit){

        for (Empresa empresa : clientesEmpresases){
            if (empresa.getNit().equals(nit)){
                return true;
            }

        }
        return false;

    }

    public String getIdEmpresa(String nit){

        for (Empresa empresa : clientesEmpresases){
            if (empresa.getNit().equals(nit)){
                return empresa.id;
            }

        }
        return null;

    }




    public void  pararListeners(){
        for (Map.Entry<Query, ChildEventListener> entry : hijosListener.entrySet()) {
            Query ref = entry.getKey();
            ChildEventListener listener = entry.getValue();
            ref.removeEventListener(listener);

        }

        for (Map.Entry<Query, ValueEventListener> entry : hijosListenerValue.entrySet()) {
            Query ref = entry.getKey();
            ValueEventListener listener = entry.getValue();
            ref.removeEventListener(listener);

        }

        for (Map.Entry<DatabaseReference, ChildEventListener> entry : hijosListenerRef.entrySet()) {
            Query ref = entry.getKey();
            ChildEventListener listener = entry.getValue();
            ref.removeEventListener(listener);

        }

    }




    public interface OnModelListener {

        void terminoPrecarga();

    }


    public int getNumeroServicios(){
        int num = 0;
        for (Servicio serv : servicios){
            if (serv.estado.equals("Finalizado") || serv.estado.equals("Anulado") || serv.estado.equals("Cancelado")){
                continue;
            }
            num++;
        }

        return num;
    }



    public void reiniciarCarga( final OnModelListener listener){

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();


        if (user == null) {
            return;
        }

        uid = user.getUid();

        ComandoValidarUsuario cmd = new ComandoValidarUsuario(new OnOValidarUsuarioChangeListener() {
            @Override
            public void validandoPasajeroOK() {
                ComandoPasajero.getPasajero(uid, new OnPasajeroChangeListener() {
                    @Override
                    public void cargoPasajero(Pasajero pasajer) {
                        pasajero = pasajer;
                        CmdEmpresa.getEmpresa(new OnEmpresaListener() {
                            @Override
                            public void cargoEmpresa() {
                                listener.terminoPrecarga();
                            }
                        });
                    }


                });


            }

            @Override
            public void validandoPasajeroError() {

            }

            @Override
            public void pasajeroPendiente() {

            }
        });
        cmd.validarUsuario();





    }







}
