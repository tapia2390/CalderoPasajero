package transcalderonpasajero.transcalderonpasajero.Clases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Conexion extends SQLiteOpenHelper {

    private static final int VERSION_BASEDATOS = 4;
    // Nombre de nuestro archivo de base de datos
    private static final String NOMBRE_BASEDATOS = "calderon.sqlite";

    public Conexion(Context context) {

        super(context,NOMBRE_BASEDATOS,null,VERSION_BASEDATOS);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //tabla mensaje
        db.execSQL("Create table Mensajes (Id_Mensajes INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL , num_msm integer)");

        db.execSQL("Create table Notification (Id_Notification INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL ," +
                "id_notificacionO TEXT, estado TEXT, mensaje TEXT,  timestamp TEXT)");

        db.execSQL("Create table Ubicacion (Id_Ubicacion INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL , numero_orden TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS Mensajes");
        db.execSQL("Create table Mensajes (Id_Mensajes integer, num_msm integer)");

        db.execSQL("DROP TABLE IF EXISTS Notification");
        db.execSQL("Create table Notification (Id_Notification INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL ," +
                "id_notificacionO TEXT, estado TEXT, mensaje TEXT, timestamp TEXT)");

        db.execSQL("DROP TABLE IF EXISTS Ubicacion");
        db.execSQL("Create table Ubicacion (Id_Ubicacion INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL , numero_orden TEXT)");

    }
}

