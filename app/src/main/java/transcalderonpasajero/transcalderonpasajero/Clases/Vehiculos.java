package transcalderonpasajero.transcalderonpasajero.Clases;

import java.util.ArrayList;

/**
 * Created by tacto on 29/08/17.
 */

public class Vehiculos {

    String  idNombre = "";
    String  nombre = "";
    int posicion = 0;

    public String getIdNombre() {
        return idNombre;
    }

    public void setIdNombre(String idNombre) {
        this.idNombre = idNombre;
    }

    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}