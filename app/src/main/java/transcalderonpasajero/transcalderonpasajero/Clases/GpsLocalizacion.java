package transcalderonpasajero.transcalderonpasajero.Clases;



public class GpsLocalizacion {


    String pais ="";
    String ciudad ="";
    double latInicio = 0.0;
    double lotInicio = 0.0;
    double latFin = 0.0;
    double lotFin = 0.0;

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public double getLatInicio() {
        return latInicio;
    }

    public void setLatInicio(double latInicio) {
        this.latInicio = latInicio;
    }

    public double getLotInicio() {
        return lotInicio;
    }

    public void setLotInicio(double lotInicio) {
        this.lotInicio = lotInicio;
    }

    public double getLatFin() {
        return latFin;
    }

    public void setLatFin(double latFin) {
        this.latFin = latFin;
    }

    public double getLotFin() {
        return lotFin;
    }

    public void setLotFin(double lotFin) {
        this.lotFin = lotFin;
    }
}
