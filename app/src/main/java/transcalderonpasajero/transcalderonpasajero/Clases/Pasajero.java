package transcalderonpasajero.transcalderonpasajero.Clases;

import java.util.ArrayList;

/**
 * Created by tactomotion on 28/09/16.
 */
public class Pasajero {

    public String uid;
    public String nombre;
    private String apellido;
    private String celular;
    private String correo;
    private String direccion;
    private String idCliente;
    public  String idEmpresa = "";
    private String rol;
    private String foto;
    public String estado = "";
    public String categoria = "B";
    public String cedula;

    public ArrayList<Pasajero> amigos = new ArrayList<>();


    public  Pasajero(){

    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public String getNombre( ){
        return this.nombre;
    }

    public void setApellido(String apellido){
        this.apellido = apellido;
    }

    public  String getApellido(){
        return this.apellido;
    }

    public void setCelular(String celular){
        this.celular = celular;
    }

    public  String getCelular(){
        return this.celular;
    }

    public void setCorreo(String correo){
        this.correo = correo;
    }

    public  String getCorreo(){
        return this.correo;
    }

    public void setDireccion(String direccion){
        this.direccion = direccion;
    }

    public  String getDireccion(){
        return this.direccion;
    }


    public void setIdCliente(String idCliente){
        this.idCliente = idCliente;
    }

    public  String getIdCliente(){
        return this.idCliente;
    }


    public void setRol(String rol){
        this.rol = rol;
    }

    public  String getRol(){
        return this.rol;
    }

    public void setFoto(String foto){
        this.foto = foto;
    }

    public  String getFoto(){
        return this.foto;
    }


    public Pasajero getPasajeroAmigoById(String id){

        for (Pasajero pas : amigos){

            if (pas.uid.equals(id)){
                return  pas;
            }
        }
        return  null;
    }
}
