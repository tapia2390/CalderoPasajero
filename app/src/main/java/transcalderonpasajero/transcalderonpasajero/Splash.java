package transcalderonpasajero.transcalderonpasajero;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import transcalderonpasajero.transcalderonpasajero.Clases.Conexion;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Pasajero;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdEmpresa;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdEmpresa.OnEmpresaListener;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdParams;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoCiudades;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoCiudades.OnCiudadesChangeListener;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoPasajero;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoPasajero.OnPasajeroChangeListener;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoValidarUsuario;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoValidarUsuario.OnOValidarUsuarioChangeListener;
import transcalderonpasajero.transcalderonpasajero.home.NewHomeNav;

public class Splash extends Activity implements  OnOValidarUsuarioChangeListener, OnCiudadesChangeListener {


    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseAuth.AuthStateListener mAuthListener;


    Modelo modelo = Modelo.getInstance();
    ComandoValidarUsuario comandoValidarUsuario = new ComandoValidarUsuario(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        if(estaConectado()){

            CmdParams.getParams();
            ComandoCiudades comandoCiudades = new ComandoCiudades(this);
            comandoCiudades.getCiudades();
            comandoCiudades.getVehiculos();



            mAuthListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    if (user != null) {
                        // User is signed in

                        modelo.uid = user.getUid();

                        validarUsuario();

                    } else {
                        Intent i = new Intent(Splash.this, MainActivity.class);
                        startActivity(i);
                        overridePendingTransition(R.anim.zoom_back_in, R.anim.zoom_back_out);
                        finish();


                    }

                }
            };

        }else{
            showAlertSinInternet();
        }


    }


    private void validarUsuario() {
        comandoValidarUsuario.validarUsuario();
    }

    @Override
    public void validandoPasajeroOK() {

        ComandoPasajero.getPasajero(modelo.uid, new OnPasajeroChangeListener() {
            @Override
            public void cargoPasajero(Pasajero pasajero) {
                modelo.context = getApplicationContext();
                modelo.pasajero = pasajero;

                CmdEmpresa.getEmpresa(new OnEmpresaListener() {
                    @Override
                    public void cargoEmpresa() {
                        CmdOrden.getCurrentService();
                        Intent i = new Intent(Splash.this, NewHomeNav.class);
                        startActivity(i);
                        overridePendingTransition(R.anim.zoom_back_in, R.anim.zoom_back_out);
                        finish();

                    }
                });
            }


        });


    }

    @Override
    public void validandoPasajeroError() {

    }

    @Override
    public void pasajeroPendiente() {
        ComandoPasajero.getPasajero(modelo.uid, new OnPasajeroChangeListener() {
            @Override
            public void cargoPasajero(Pasajero pasajero) {
                modelo.context = getApplicationContext();
                modelo.pasajero = pasajero;

                CmdEmpresa.getEmpresa(new OnEmpresaListener() {
                    @Override
                    public void cargoEmpresa() {
                        CmdOrden.getCurrentService();
                        Intent i = new Intent(Splash.this, NewHomeNav.class);
                        startActivity(i);
                        overridePendingTransition(R.anim.zoom_back_in, R.anim.zoom_back_out);
                        finish();

                    }
                });
            }


        });
    }



    //validacion conexion internet
    protected Boolean estaConectado(){
        if(conectadoWifi()){
            return true;
        }else{
            if(conectadoRedMovil()){
                return true;
            }else{
                showAlertSinInternet();
                return false;
            }
        }
    }


    //validacion wifi
    protected Boolean conectadoWifi(){
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }



    protected Boolean conectadoRedMovil(){
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }



    public void showAlertSinInternet(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                Splash.this);

        // set title
        alertDialogBuilder.setTitle("Sin Internet");

        // set dialog message
        alertDialogBuilder
                .setMessage("Sin conexión a Internet")
                .setCancelable(false)
                .setPositiveButton("Reintentar",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                        Intent inte  = new Intent(getBaseContext(),Splash.class);
                        startActivity(inte);
                        finish();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }



    @Override
    public void onStart() {
        super.onStart();
        if(mAuth==null || mAuthListener ==null){
            return;
        }else{
            mAuth.addAuthStateListener(mAuthListener);
        }


    }

    @Override
    public void onStop() {
        super.onStop();

        if(mAuth==null || mAuthListener ==null){
            return;
        }else{
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }


    @Override
    public void cargoCiudades() {

    }

    @Override
    public void cargoVehiculos() {

    }
}
