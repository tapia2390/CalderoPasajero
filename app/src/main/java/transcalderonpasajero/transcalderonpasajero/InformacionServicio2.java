package transcalderonpasajero.transcalderonpasajero;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import transcalderonpasajero.transcalderonpasajero.Adapter.ListaNotificacionesAdapter;
import transcalderonpasajero.transcalderonpasajero.Clases.Conductor;
import transcalderonpasajero.transcalderonpasajero.Clases.Conexion;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoConductor;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoOrdenesPasajeroUltimosDiez;

import android.app.AlertDialog;

public class InformacionServicio2 extends Activity implements ListaNotificacionesAdapter.AdapterCallback , Modelo.OnModeloChangelistener, ComandoOrdenesPasajeroUltimosDiez.OnOrdenesPasajeroHistorialChangeListener {

    String idServicio;
    TextView numero_orden;
    TextView text_ciudad_origen;
    ImageView imagen_estado;
    TextView text_ciudad_llegada;
    TextView barrio_recogida;
    TextView barrio_llegada;
    TextView fecha_y_hora_recogida;
    TextView datos_conductor;
    TextView datos_conductor_telefono;
    Modelo modelo = Modelo.getInstance();
    Servicio servicio;
    ListView lvcomentario;
    Conductor conductor = new Conductor();
    ListaNotificacionesAdapter mAdapterNotifivicacion;
    final Context context = this;
    Conexion con;
    SQLiteDatabase miDB;
    int datoss = 0;
    int idm;
    ImageView gpsubicacion;
    ComandoOrdenesPasajeroUltimosDiez comandoOrdenPasajero;
    Button btn_paradas;
    protected Location ultima_localizacion;
    String idBDUbicacion = "";
    int posicionservicio = 0;
    boolean gpsActivo = false;
    private final int REQUEST_PERMISSION = 1000;
    Servicio ordenp;
    double latitude, longitud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_informacion_servicio);

        Bundle bundle = getIntent().getExtras();
        idServicio = bundle.getString("id");
        posicionservicio = bundle.getInt("posicionservicio");
        servicio = modelo.getOrdenById(idServicio);

        numero_orden = (TextView) findViewById(R.id.numero_orden);
        text_ciudad_origen = (TextView) findViewById(R.id.text_ciudad_origen);
        imagen_estado = (ImageView) findViewById(R.id.imagen_estado);
        text_ciudad_llegada = (TextView) findViewById(R.id.text_ciudad_llegada);
        barrio_recogida = (TextView) findViewById(R.id.barrio_recogida);
        barrio_llegada = (TextView) findViewById(R.id.barrio_llegada);
        fecha_y_hora_recogida = (TextView) findViewById(R.id.fecha_y_hora_recogida);
        datos_conductor = (TextView) findViewById(R.id.datos_conductor);
        datos_conductor_telefono = (TextView) findViewById(R.id.datos_conductor_telefono);
        lvcomentario = (ListView) findViewById(R.id.lvcomentario);
        gpsubicacion = (ImageView) findViewById(R.id.gpsubicacion);
        btn_paradas = (Button) findViewById(R.id.btn_paradas);
        comandoOrdenPasajero = new ComandoOrdenesPasajeroUltimosDiez(this);
        selectUbicacion();
        actualizarPantalla();
        gpsDatos();
        ordenp = modelo.getOrdenById(idServicio);

        String commaSeparated = ordenp.getRuta();
        ArrayList<String> items = new  ArrayList<String>(Arrays.asList(commaSeparated.split("-")));
        btn_paradas.setText("Paradas: "+items.size());
    }

    private void gpsDatos() {
        LocationManager locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED){
            if (estaConectado()) {
                locationStart2();
            } else {
                showAlertSinInternet();
            }
        }
        //Si usted ha sido rechazado
        else{
            requestLocationPermission();
        }
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (estaConectado()) {
            locationStart2();
        } else {
            showAlertSinInternet();
        }



    }


    private void displayListaNotificaciones() {
        //pasamos los datos del adaptador
        mAdapterNotifivicacion = new ListaNotificacionesAdapter(this, this, idServicio);
        lvcomentario.setAdapter(mAdapterNotifivicacion);

    }


    public void actualizarEstadoPantalla() {

        if (servicio.getEstado().equals("Asignado") || servicio.getEstado().equals("No Asignado")) {
            imagen_estado.setImageResource(R.drawable.estado_confirmado_i5);
        } else if (servicio.getEstado().equals("En Camino")) {
            imagen_estado.setImageResource(R.drawable.estado_en_camino_i5);
        } else if (servicio.getEstado().equals("Transportando")) {
            imagen_estado.setImageResource(R.drawable.estado_transportando_i5);
        } else if (servicio.getEstado().equals("Finalizado")) {
            comandoOrdenPasajero.getOrdenesPasajeroHistorial();
            imagen_estado.setImageResource(R.drawable.estado_finalizado_i5);

        } else if (servicio.getEstado().equals("No Asignado")) {
            imagen_estado.setImageResource(R.drawable.estado_sin_confirmar_i5);
        }
        else if (servicio.getEstado().equals("Anulado")) {
            imagen_estado.setVisibility(View.INVISIBLE);

        }
        else {
            imagen_estado.setImageResource(R.drawable.estado_cancelado_i5);
        }
    }


    public void showAlertCAlificacion() {
        //   Toast.makeText(getApplicationContext(),"ingreso",Toast.LENGTH_SHORT).show();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle("¡Estado del Servicio!");

        // set dialog message
        alertDialogBuilder
                .setMessage("Su servicio ha finalizado. Puede calificarlo. ")
                .setCancelable(false)
                .setPositiveButton("Ok, Calificar Servicio", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //todo internet

                        Intent intent = new Intent(getApplicationContext(), InformacionDelServicioDetallada.class);
                        intent.putExtra("id", "" + idServicio);
                        intent.putExtra("fecha", "");
                        startActivity(intent);

                    }
                })
                .setNegativeButton("Ahora no", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {



                        dialog.cancel();
                        dialog.dismiss();
                    }
                });

        alertDialogBuilder.show();

        modelo.llamarServicios();
    }


    public void cargoConductor() {
        Log.v("cargo", "cargo" + "conductor");
        //  Toast.makeText(getApplicationContext()," "+modelo.conductor.getNombre(),Toast.LENGTH_SHORT).show();
        try {
            // Log.v("nombre", "nombre" + modelo.conductor.getNombre());

            //datos_conductor.setText(modelo.conductor.getNombre() + " " + modelo.conductor.getApellido() + " | " + servicio.getMatricula());
            //datos_conductor_telefono.setText(modelo.conductor.getCelular());

        } catch (Exception e) {

        }
    }

    public void llamar(View v) {
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            //callIntent.setData(Uri.parse("tel:" + modelo.conductor.getCelular()));
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            startActivity(callIntent);
        } catch (Exception e) {
            Log.v("no puede llamar", "no puede llamar");

        }

    }







    @Override
    public void setActualizoListadoDeServicios() {
        actualizarPantalla();
    }

    @Override
    public void setActualizoListadoDeServiciosHistoricos() {

    }

    public void actualizarPantalla() {
        modelo.setModeloListener(this);
        servicio = modelo.getOrdenById(idServicio);

        if (servicio == null) {
            return;
        }

        text_ciudad_origen.setText(servicio.getOrigen());
        text_ciudad_llegada.setText(servicio.getDestino());
        numero_orden.setText("SERVICIO " + servicio.getCosecutivoOrden());
        barrio_recogida.setText(servicio.getDireccionOrigen());
        barrio_llegada.setText(servicio.direccionDestino);
        fecha_y_hora_recogida.setText("Recoger: " + modelo.dfsimple.format(servicio.getFechaEnOrigen()) + " " + servicio.horaEnOrigen);
        Log.v("matricula", "matricula" + servicio.getMatricula());

        if (servicio.getMatricula() == "" || servicio.getMatricula().equals("")) {
            datos_conductor.setText("Sin Asignar");
            datos_conductor_telefono.setText("Sin Asignar");
            datos_conductor_telefono.setBackgroundColor(Color.rgb(150, 75, 0));

        } else {
            //ComandoConductor comandoConductor = new ComandoConductor(this);
            //comandoConductor.getDatosConductor(modelo.getOrdenById(idServicio).idConductor);

        }

        displayListaNotificaciones();
        actualizarEstadoPantalla();

        updateDatos(mAdapterNotifivicacion.getCount());


    }

    // bd
    public void abrirCone() {
        con = new Conexion(this);
        miDB = con.getWritableDatabase();
    }

    public void cerrarCone() {
        miDB.close();
    }


    public void updateDatos(int cant) {

        abrirCone();
        ContentValues valores = new ContentValues();
        valores.put("num_msm", cant);
        long result1 = miDB.update("Mensajes", valores, "Id_Mensajes=" + 1, null);

        //Toast.makeText(getApplicationContext(), ".... update"+result1, Toast.LENGTH_LONG).show();
        if (result1 > 0) {
            // Toast.makeText(getApplicationContext(), "Datos ingresados con exito"+result1, Toast.LENGTH_SHORT).show();
            Log.v("update", "Datos ingresados con exito");
        } else {
            //Toast.makeText(getApplicationContext(), "Datos no ingresados"+result1, Toast.LENGTH_SHORT).show();
            Log.v("no update", "Datos No ingresados ");
        }

        cerrarCone();
    }


    //ciclo de vida


    @Override
    protected void onResume() {
        super.onResume();
        modelo.appcerradaAbierta = false;
        System.out.print("detalel onResumen");

    }


    @Override
    protected void onStart() {
        super.onStart();
        System.out.print("detalle onstart");
        modelo.appcerradaAbierta = false;

    }


    @Override
    protected void onStop() {
        super.onStop();
        System.out.print("detalle onstop");
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.print("detalle onPause");
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.print("detalle onDestroy");
    }


    /* mi Servicios */
    public void Start_Service() {
        modelo.appcerradaAbierta = true;
        Intent intent = new Intent(getApplicationContext(), MiService.class);
        intent.putExtra("primerplano", false);
        startService(intent);
    }

    /* mi MiServiceForeground */
    private void MiServiceBoot() {

        Intent intent = new Intent(getApplicationContext(), ServiceBoot.class);
        intent.putExtra("primerplano", false);
        startService(intent);
    }

    public void Stop_Service() {
        modelo.appcerradaAbierta = false;
        stopService(new Intent(getApplicationContext(), MiService.class));
    }

    @Override
    public void cargoUnaOrdenesPasajeroHistorial() {
        showAlertCAlificacion();
    }


    @Override
    public void cargoHisTorialP() {


    }


    //gps
    public void gps(View v) {
        //Verificamos si el GPS esta prendido o no:
        if(Build.VERSION.SDK_INT >= 23){
            checkPermission();
        }
        else{
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            } else {
                if (estaConectado()) {
                    locationStart();
                } else {
                    showAlertSinInternet();
                }
            }
        }


    }

    //gps+

    private void locationStart() {
        final LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Localizacion Local = new Localizacion();
        Local.setMainActivity(this);
        final boolean gpsEnabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            return;
        }
        mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, (LocationListener) Local);
        mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) Local);

        if(gpsEnabled)
        {
            LocationManager locationManager = (LocationManager)
                    getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();

            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            Location location = locationManager.getLastKnownLocation(locationManager
                    .getBestProvider(criteria, false));


            try {
                latitude = Double.parseDouble(""+location.getLatitude());
                longitud = Double.parseDouble(""+location.getLongitude());
            }
            catch (Exception e){
                Log.v("exeption","exeption");
            }


            modelo.latitud =latitude;
            modelo.longitud =longitud;


            if(gpsActivo){
                showGpsBolean();
            }else{



                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                intent.putExtra("id", idServicio);
                intent.putExtra("idUbi", modelo.idGpsUbicacion);

                startActivity(intent);
            }

        }else {

            showAlerGpsDesactivado();

        }

    }


    /*public void setLocation(Location loc) {
        //Obtener la direccion de la calle a partir de la latitud y la longitud
        if (loc.getLatitude() != 0.0 && loc.getLongitude() != 0.0) {
            try {
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                List<Address> list = geocoder.getFromLocation(
                        loc.getLatitude(), loc.getLongitude(), 1);
                if (!list.isEmpty()) {
                    Address DirCalle = list.get(0);

                    //  Toast.makeText(getApplicationContext(),""+"Mi direccion es: \n"
                      //      + DirCalle.getAddressLine(0),Toast.LENGTH_SHORT).show();


                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
*/
    /* Aqui empieza la Clase Localizacion */
    public class Localizacion implements LocationListener {
        InformacionServicio2 mainActivity;

        public InformacionServicio2 getMainActivity() {
            return mainActivity;
        }

        public void setMainActivity(InformacionServicio2 mainActivity) {
            this.mainActivity = mainActivity;
        }

        /**
         * Called when the location has changed.
         * <p>
         * <p> There are no restrictions on the use of the supplied Location object.
         *
         */
        @Override
        public void onLocationChanged(Location loc) {
            // Este metodo se ejecuta cada vez que el GPS recibe nuevas coordenadas
            // debido a la deteccion de un cambio de ubicacion

//metodo que  conbierte latitu y longitud en direccion
            // this.mainActivity.setLocation(loc);

            if (loc != null) {
                //setCurrentLocation(loc);

                modelo.latitud = loc.getLatitude();
                modelo.longitud = loc.getLongitude();


                String Text = "Mi ubicacion actual es: " + "\n Lat = "
                        + loc.getLatitude() + "\n Long = " + loc.getLongitude();

            }
            else{
                Toast.makeText(getApplicationContext(),"No se pudo optener su ubicaciòn, por favor intente mas tarde...",Toast.LENGTH_SHORT).show();
            }

        }

        private void setCurrentLocation(Location loc) {
            String Text = "Mi ubicacion actual es--: " + "\n Lat = "
                    + loc.getLatitude() + "\n Long = --" + loc.getLongitude();

            //Toast.makeText(getApplicationContext(),""+Text,Toast.LENGTH_SHORT).show();


        }

        @Override
        public void onProviderDisabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es desactivado
            //  Toast.makeText(getApplicationContext(),"GPS Desactivado",Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onProviderEnabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es activado

            //Toast.makeText(getApplicationContext(), "GPS Activo", Toast.LENGTH_SHORT).show();


        }



        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            switch (status) {
                case LocationProvider.AVAILABLE:
                    Log.d("debug", "LocationProvider.AVAILABLE");
                    break;
                case LocationProvider.OUT_OF_SERVICE:
                    Log.d("debug", "LocationProvider.OUT_OF_SERVICE");
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE");
                    break;
            }
        }
    }



    //validacion conexion internet
    protected Boolean estaConectado(){
        if(conectadoWifi()){
            Log.v("wifi","Tu Dispositivo tiene Conexion a Wifi.");
            return true;
        }else{
            if(conectadoRedMovil()){
                Log.v("Datos", "Tu Dispositivo tiene Conexion Movil.");
                return true;
            }else{
                showAlertSinInternet();
                // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
    }

    protected Boolean conectadoWifi(){
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    protected Boolean conectadoRedMovil(){
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }




    public void showAlertSinInternet(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("Sin Internet");

        // set dialog message
        alertDialogBuilder
                .setMessage("Sin Conexión a Internet")
                .setCancelable(false)
                .setPositiveButton("Reintentar",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void showAlerGpsDesactivado()
    {

        if(!((Activity) context).isFinishing())
        {
            //show dialog
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            // Setting Alert Dialog Title
            alertDialogBuilder.setTitle("GPS deshabilitado");
            // Icon Of Alert Dialog
            alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
            // Setting Alert Dialog Message
            alertDialogBuilder.setMessage("Active la función de localización para determinar su ubicación ");
            alertDialogBuilder.setCancelable(false);

            alertDialogBuilder.setPositiveButton("Ajustes", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(settingsIntent);
                }
            });

            alertDialogBuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //
                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        }

    }


    public void selectUbicacion() {

        if(servicio.ubicacionPasajeroGPsses.size()>0){
            for(int i = 0; i < servicio.ubicacionPasajeroGPsses.size(); i++){
                if (servicio.ubicacionPasajeroGPsses.get(i).getPasajero().equals(modelo.uid)) {
                    gpsubicacion.setBackgroundResource(R.drawable.btn_ubicacion_compartida_i5);
                    modelo.idGpsUbicacion= servicio.ubicacionPasajeroGPsses.get(i).getPathUbicacion();
                    gpsActivo=true;
                }else{
                    gpsubicacion.setBackgroundResource(R.drawable.btn_compartir_ubicacion_i5);
                    gpsActivo=false;
                }
            }
        }
        else{
            gpsubicacion.setBackgroundResource(R.drawable.btn_compartir_ubicacion_i5);
            gpsActivo=false;
        }
    }



    public void showGpsBolean()
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // Setting Alert Dialog Title
        alertDialogBuilder.setTitle("Su ubicación ya ha sido enviada");
        // Icon Of Alert Dialog
        alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
        // Setting Alert Dialog Message
        alertDialogBuilder.setMessage("¿Desea enviar otra ubicacíon");
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {

                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                intent.putExtra("id", idServicio);
                intent.putExtra("idUbi", modelo.idGpsUbicacion);
                startActivity(intent);

            }
        });

        alertDialogBuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //
            }
        });
        /*alertDialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(),"You clicked on Cancel",Toast.LENGTH_SHORT).show();
            }
        });*/

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }


    //permisos v6
    public void checkPermission() {
        //ya permitida
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED){
            if (estaConectado()) {
                locationStart();
            } else {
                showAlertSinInternet();
            }
        }
        //Si usted ha sido rechazado
        else{
            requestLocationPermission();
        }
    }

    // Pedir permiso
    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION);

        } else {
            //Toast toast = Toast.makeText(this, "No es permitido y no se puede ejecutar la aplicación", Toast.LENGTH_SHORT);
            //toast.show();

            showAlerGpsDesactivado();
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,}, REQUEST_PERMISSION);

        }
    }

    // Los resultados de recepción de
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION) {
            //uso está permitido
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationStart();
                return;

            } else {
                // La correspondencia, cuando todavía se niega
                //  Toast toast = Toast.makeText(this, "No se puede estar haciendo más", Toast.LENGTH_SHORT);
                // toast.show();
                showAlerGpsDesactivado();
            }
        }
    }


    // tener guardado la latud y longitud al cargar la vista
    private void locationStart2() {
        final LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Localizacion Local = new Localizacion();
        Local.setMainActivity(this);
        final boolean gpsEnabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            return;
        }
        mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, (LocationListener) Local);
        mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) Local);

        if(gpsEnabled)
        {
            LocationManager locationManager = (LocationManager)
                    getSystemService(Context.LOCATION_SERVICE);
            Criteria criteria = new Criteria();

            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            Location location = locationManager.getLastKnownLocation(locationManager
                    .getBestProvider(criteria, false));


            try {
                latitude = Double.parseDouble(""+location.getLatitude());
                longitud = Double.parseDouble(""+location.getLongitude());
            }
            catch (Exception e){
                Log.v("exeption","exeption");
            }


            modelo.latitud =latitude;
            modelo.longitud =longitud;

        }

    }


    public void paradas(View v){
        Intent intent = new Intent(getApplicationContext(),Rutas.class);
        intent.putExtra("id", "" + idServicio);
        startActivity(intent);
    }
}