package transcalderonpasajero.transcalderonpasajero;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoOrdenesPasajeroUltimosDiez;

@SuppressWarnings("ALL")
public class Historico extends Fragment implements ComandoOrdenesPasajeroUltimosDiez.OnOrdenesPasajeroHistorialChangeListener {

    private Modelo sing = Modelo.getInstance();
    public EditText fechaHistorial;
    public LinearLayout layer10,fecha;
    public Button btnBuscar;
    ComandoOrdenesPasajeroUltimosDiez comandoHistorial;

    static final int DATE_DIALOG_ID = 1;
    private int mYear;
    private int mMonth;
    private int mDay;
    String datos;
    Date date_1,date_2;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.activity_historico, null);


        return root;

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        fechaHistorial = (EditText) view.findViewById(R.id.fechaHistorial);
        layer10 = view.findViewById(R.id.linear10);
        fecha = view.findViewById(R.id.fecha);
        btnBuscar = view.findViewById(R.id.btnBuscar);


        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);

        sing.appcerradaAbierta = false;

        comandoHistorial = new ComandoOrdenesPasajeroUltimosDiez(this);
        comandoHistorial.getOrdenesPasajeroHistorial();

        layer10.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), HistorialUltimosServicios.class);
                startActivity(i);
            }
        });


        fecha.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
              //  Toast.makeText(getContext(),"mm",Toast.LENGTH_LONG).show();
                final int mes = c.get(Calendar.MONTH);
                final int dia = c.get(Calendar.DAY_OF_MONTH);
                final int anio = c.get(Calendar.YEAR);

                DatePickerDialog recogerFecha = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        int mesActual = month + 1;
                        String diaFormateado = (dayOfMonth < 10)? "0" + String.valueOf(dayOfMonth):String.valueOf(dayOfMonth);
                        String mesFormateado = (mesActual < 10)? "0" + String.valueOf(mesActual):String.valueOf(mesActual);

                        fechaHistorial.setText(mesFormateado +"/"+ year);

                    }
                },anio, mes, dia);

                recogerFecha.show();

            }
        });

        btnBuscar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                buscar();
            }
        });

        fechaHistorial.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int length = fechaHistorial.length();
                String convert = String.valueOf(length);



            }



            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });




    }





    public void buscar(){




        if(fechaHistorial.getText().toString().equals("")){
            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(getActivity());
            dialogo1.setTitle("Fecha");
            dialogo1.setMessage("Selecione una Fecha");
            dialogo1.setCancelable(false);
            dialogo1.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {

                }
            });
            dialogo1.show();

        }
        else{

            Calendar calendar = Calendar.getInstance();
            final int year1 = calendar.get(Calendar.YEAR);
            final int month1 = calendar.get(Calendar.MONTH)+1;
            int  day1 = calendar.get(Calendar.DAY_OF_MONTH);
            final String fecha_actual = day1+"/"+month1+"/"+year1;

            //String dateInString = fecha_actual;
            try {
                date_1 = sing.dfsimple.parse(fecha_actual);
                //System.out.println(date_1);
                //System.out.println(formatter.format(date_1));

            } catch (ParseException e) {
                e.printStackTrace();
            }

            String fechaResivida = "02/"+fechaHistorial.getText().toString();

            try {
                date_2 = sing.dfsimple.parse(fechaResivida);
            } catch (ParseException ex) {
                ex.printStackTrace();
            }

            int rf = date_1.compareTo(date_2);
            if (rf == -1) {
                System.out.println("mayor");

                AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                dialog.setTitle("Fecha Superior"); // set title
                dialog.setMessage("La fecha ingresada es mayor a la  fecha actual"); // set message
                dialog.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                fechaHistorial.setText(month1+"/"+year1);
                            }
                        });
                dialog.create().show();

            }else {

                String fecha =fechaHistorial.getText().toString().replaceAll("\\s","");
                sing.filtrarPorFechaYCliente( "01/"+fecha);
                Intent i = new Intent(getActivity(), HistoricoServiciosDetallada.class);
                i.putExtra("fecha", "" + fecha);
                startActivity(i);
            }
        }
    }

    //fecha
    DatePickerDialog.OnDateSetListener mDateSetListner = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            mYear = year;
            mMonth = monthOfYear;
            mDay = dayOfMonth;
            updateDate();
        }
    };




    //@Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
   /*
    * return new DatePickerDialog(this, mDateSetListner, mYear, mMonth,
    * mDay);
    */
                DatePickerDialog datePickerDialog = this.customDatePicker();
                return datePickerDialog;
        }
        return null;
    }

    protected void updateDate() {
        int localMonth = (mMonth + 1);
        String monthString = localMonth < 10 ? "0" + localMonth : Integer
                .toString(localMonth);
        String localYear = Integer.toString(mYear).substring(0);
        fechaHistorial.setText(new StringBuilder()
                // Month is 0 based so add 1
                .append(monthString).append("/").append(localYear).append(" "));
        getActivity().showDialog(DATE_DIALOG_ID);

    }


    private DatePickerDialog customDatePicker() {
        DatePickerDialog dpd = new DatePickerDialog(getActivity(), mDateSetListner,
                mYear, mMonth+1, mDay);
        try {

            Field[] datePickerDialogFields = dpd.getClass().getDeclaredFields();
            for (Field datePickerDialogField : datePickerDialogFields) {
                if (datePickerDialogField.getName().equals("mDatePicker")) {
                    datePickerDialogField.setAccessible(true);
                    DatePicker datePicker = (DatePicker) datePickerDialogField
                            .get(dpd);
                    Field datePickerFields[] = datePickerDialogField.getType()
                            .getDeclaredFields();
                    for (Field datePickerField : datePickerFields) {
                        if ("mDayPicker".equals(datePickerField.getName())
                                || "mDaySpinner".equals(datePickerField
                                .getName())) {
                            datePickerField.setAccessible(true);
                            Object dayPicker = new Object();
                            dayPicker = datePickerField.get(datePicker);
                            ((View) dayPicker).setVisibility(View.GONE);
                        }
                    }
                }

            }
        } catch (Exception ex) {
        }
        return dpd;
    }
    //fin fecha








    @Override
    public void cargoUnaOrdenesPasajeroHistorial() {

    }

    @Override
    public void cargoHisTorialP() {

    }
}
