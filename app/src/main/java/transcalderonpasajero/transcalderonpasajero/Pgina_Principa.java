package transcalderonpasajero.transcalderonpasajero;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;

import android.app.TabActivity;
import android.content.Intent;
import android.widget.TabHost;
import android.widget.Toast;


import com.google.firebase.auth.FirebaseAuth;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden.OnValidarListener;

public class Pgina_Principa extends TabActivity implements  ActivityCompat.OnRequestPermissionsResultCallback {

    Modelo modelo = Modelo.getInstance();
    String vistaPosicion;
    final Context context = this;

    private static final String TAG ="EmailPassword";
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseAuth.AuthStateListener mAuthListener;
    CmdOrden cmdOrden;

    TabHost.TabSpec tab1;
    TabHost.TabSpec tab2;
    TabHost.TabSpec tab3;
    TabHost tabHost;

    String tvLatitud, tvLongitud, tvAltura, tvPrecision;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_pgina__principa);

        cmdOrden = new CmdOrden();

        CmdOrden.validarUsuario(new OnValidarListener() {
            @Override
            public void valido() {

                if(modelo.validaUsuario.equals("Si exise")){
                    Log.v("modelo registor exitoso","registor exitoso");
                    tapvista();

                }
                else if(modelo.validaUsuario.equals("")){
                    Log.v("sin loguearce","sin loguearce");
                }
                else{
                    Log.v("modelo No eres pasajero","No eres pasajero");
                    Toast.makeText(getApplicationContext(),"Usted no es un pasajero",Toast.LENGTH_SHORT).show();
                    mAuth.signOut();
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    i.putExtra("vistaPosicion","dos");
                    startActivity(i);
                    overridePendingTransition(R.anim.zoom_back_in, R.anim.zoom_back_out);
                    finish();
                }

            }
        });


        modelo.llamarServicios();

        Resources res = getResources();

        // create the TabHost that will contain the Tabs
        tabHost = (TabHost)findViewById(android.R.id.tabhost);


        tab1 = tabHost.newTabSpec("primer Tab");
        tab2 = tabHost.newTabSpec("segundo Tab");
        tab3 = tabHost.newTabSpec("tercero tab");
        modelo.appcerradaAbierta = false;
        //tapvista();

        getLocationPermission();

    }



    public void tapvista(){
        // Set the Tab name and Activity
        // that will be opened when particular Tab will be selected
        tab1.setIndicator("",null);
        tab1.setContent(new Intent(this, Perfil.class));

        tab2.setIndicator("", null);
        tab2.setContent(new Intent(this, Servicios.class));
        //overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);

        tab3.setIndicator("", null);
        tab3.setContent(new Intent(this, Historico.class));

        /** Add the tabs  to the TabHost to display. */
        tabHost.addTab(tab1);
        tabHost.addTab(tab2);
        tabHost.addTab(tab3);

        Bundle bundle = getIntent().getExtras();
        vistaPosicion = bundle.getString("vistaPosicion");

        if(vistaPosicion.equals("uno")){
            tabHost.setCurrentTab(0);
            tabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.btn_perfil_blanco_i5);
            tabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.btn_servicios_verde_i5);
            tabHost.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.btn_historico_verde_i5);

        }
        else if(vistaPosicion.equals("tres")){
            tabHost.setCurrentTab(2);
            tabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.btn_perfil_verde_i5);
            tabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.btn_servicios_verde_i5);
            tabHost.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.btn_historico_blanco_i5);

        }
        else {

            tabHost.setCurrentTab(1);
            tabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.btn_perfil_verde_i5);
            tabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.btn_servicios_blanco_i5);
            tabHost.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.btn_historico_verde_i5);

        }

        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                if (tabId.equals("primer Tab")) {
                    //Toast.makeText(getApplicationContext(),"tab1",Toast.LENGTH_SHORT).show();
                    tabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.btn_perfil_blanco_i5);
                    tabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.btn_servicios_verde_i5);
                    tabHost.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.btn_historico_verde_i5);
                }
                else if (tabId.equals("segundo Tab")) {
                    //Toast.makeText(getApplicationContext(),"tab2",Toast.LENGTH_SHORT).show();
                    tabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.btn_perfil_verde_i5);
                    tabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.btn_servicios_blanco_i5);
                    tabHost.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.btn_historico_verde_i5);
                }
                else{
                    //Toast.makeText(getApplicationContext(),"tab3",Toast.LENGTH_SHORT).show();
                    tabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.btn_perfil_verde_i5);
                    tabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.btn_servicios_verde_i5);
                    tabHost.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.btn_historico_blanco_i5);
                }

            }
        });

    }



    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == event.KEYCODE_BACK) {
            Log.v("cerrar","cerrar");
        }
        return false;
    }





    @Override
    protected void onStart() {
        super.onStart();
        modelo.appcerradaAbierta = false;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.wtf("Ciclo de vida", "onDestroy");
        Start_Service();
    }


    @Override
    protected void onStop() {
        super.onStop();
        Log.wtf("Ciclo de vida", "onStop");
        Start_Service();
    }

    /* mi Servicios */
    public void Start_Service(){
        modelo.appcerradaAbierta  = true;
        Intent intent = new Intent(getApplicationContext(), MiService.class);
        intent.putExtra("primerplano", false);
        startService(intent);
    }

    /* mi MiServiceForeground */
    private void MiServiceBoot() {

        Intent intent = new Intent(getApplicationContext(), ServiceBoot.class);
        intent.putExtra("primerplano", false);
        startService(intent);
    }

    public void Stop_Service(){
        modelo.appcerradaAbierta  = false;
        stopService(new Intent(getApplicationContext(), MiService.class));
    }


    public void showAlerGpsDesactivado()
    {

        if(!((Activity) context).isFinishing())
        {
            //show dialog
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            // Setting Alert Dialog Title
            alertDialogBuilder.setTitle("GPS deshabilitado");
            // Icon Of Alert Dialog
            alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
            // Setting Alert Dialog Message
            alertDialogBuilder.setMessage("Active la función de localización para determinar su ubicación ");
            alertDialogBuilder.setCancelable(false);

            alertDialogBuilder.setPositiveButton("Ajustes", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(settingsIntent);
                }
            });

            alertDialogBuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //
                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        }

    }


    /**
     * Prompts the user for permission to use the device location.
     */
    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            //mLocationPermissionGranted = true;
            //startLocationUpdates();
        } else {


            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    1);



        }


    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        //mLocationPermissionGranted = false;
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //mLocationPermissionGranted = true;
                    //startLocationUpdates();
                }
            }
        }
        //updateLocationUI();
    }



}
