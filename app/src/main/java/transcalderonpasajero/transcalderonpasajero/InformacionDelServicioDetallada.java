package transcalderonpasajero.transcalderonpasajero;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RatingBar.OnRatingBarChangeListener;

import transcalderonpasajero.transcalderonpasajero.Clases.Calificacion;
import transcalderonpasajero.transcalderonpasajero.Clases.Conductor;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoCalificaion;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoConductor;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoConductor.OnCmdConductorListener;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoOrdenesPasajeroUltimosDiez;

public class InformacionDelServicioDetallada extends Activity implements ComandoOrdenesPasajeroUltimosDiez.OnOrdenesPasajeroHistorialChangeListener,  ComandoCalificaion.OnCalificaionChangeListener {

    String idServicio;
    TextView numero_orden;
    TextView text_ciudad_origen;
    ImageView imagen_estado;
    TextView text_ciudad_llegada;
    TextView barrio_recogida;
    TextView barrio_llegada;
    TextView fecha_y_hora_recogida;
    TextView datos_conductor;
    TextView datos_conductor_telefono;
    Modelo modelo = Modelo.getInstance();
    Servicio servicio;
    Conductor conductor = new Conductor();
    String fechaHistorco;
    RatingBar ratingBar;
    EditText observacion;
    ComandoCalificaion comandiNotificacion;
    String value = "0";
    Float valcalificaion;
    Button button3;
    final Context context = this;
    ComandoOrdenesPasajeroUltimosDiez comandoH = new ComandoOrdenesPasajeroUltimosDiez(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_informacion_del_servicio_detallada);

        Bundle bundle = getIntent().getExtras();
        idServicio = bundle.getString("id");
        fechaHistorco = bundle.getString("fecha");


        comandiNotificacion = new ComandoCalificaion(this);


        numero_orden = (TextView) findViewById(R.id.numero_orden);
        text_ciudad_origen = (TextView) findViewById(R.id.text_ciudad_origen);
        imagen_estado = (ImageView) findViewById(R.id.imagen_estado);
        text_ciudad_llegada = (TextView) findViewById(R.id.text_ciudad_llegada);
        barrio_recogida = (TextView) findViewById(R.id.barrio_recogida);
        barrio_llegada = (TextView) findViewById(R.id.barrio_llegada);
        fecha_y_hora_recogida = (TextView) findViewById(R.id.fecha_y_hora_recogida);
        datos_conductor = (TextView) findViewById(R.id.datos_conductor);
        datos_conductor_telefono = (TextView) findViewById(R.id.datos_conductor_telefono);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        observacion = (EditText) findViewById(R.id.observacion);
        button3 = (Button) findViewById(R.id.button3);

        servicio = modelo.getOrdenHistorial(idServicio);
        if(servicio !=null){
            text_ciudad_origen.setText(servicio.getOrigen());
            text_ciudad_llegada.setText(servicio.getDestino());
            numero_orden.setText("SERVICIO " + servicio.getCosecutivoOrden());
            barrio_recogida.setText(servicio.getDireccionOrigen());
            barrio_llegada.setText(servicio.direccionDestino);

            fecha_y_hora_recogida.setText("Recoger: " + servicio.getFechaEnOrigen() + " " + servicio.horaEnDestino);
        }


        ratingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {

                value = "" + rating;

            }
        });

        comandoH.getTodasLasOrdenesPasajeroHistorial();
        ComandoConductor.getConductorTercero(servicio, servicio.getIdConductor(), new OnCmdConductorListener() {
            @Override
            public void cargoTercero() {
                datos_conductor.setText(servicio.conductor.nombre + " " + servicio.conductor.apellido + " | " + servicio.getMatricula());
                datos_conductor_telefono.setText(servicio.conductor.celular);

            }
        });


    }

    public void atras(View v) {
        if (fechaHistorco.equals("")) {
            Intent i = new Intent(getApplicationContext(), HistorialUltimosServicios.class);
            startActivity(i);
            //finish();
        }else {
            Intent i = new Intent(getApplicationContext(), HistoricoServiciosDetallada.class);
            i.putExtra("fecha", "" + fechaHistorco);
            startActivity(i);
            //finish();
        }
    }



    public void llamar(View v) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:"+servicio.conductor.celular));

    }

    @Override
    public void cargoUnaOrdenesPasajeroHistorial() {

        Log.v("cargo","cargo"+"historial");
        // Toast.makeText(getApplicationContext(),"ok "+modelo.getOrdenHistorial(idServicio).notificaciones.size(),Toast.LENGTH_SHORT).show();
        int notificaciones  = servicio.contarMisCalificaciones();
        if(notificaciones >0){
            button3.setVisibility(View.GONE);
            button3.setVisibility(View.INVISIBLE);
            observacion.setEnabled(false);
            ratingBar.setEnabled(false);

            if(servicio.contarMisCalificaciones()>0){
                Calificacion calificacion = servicio.getMiCalificacion();
                ratingBar.setRating(Float.parseFloat(""+calificacion.getValor()));
                observacion.setText(""+calificacion.getObervacion());
            }
        }

    }

    @Override
    public void cargoHisTorialP() {

    }

    @Override
    public void cargoCalificaion() {

        button3.setVisibility(View.GONE);
        button3.setVisibility(View.INVISIBLE);
        observacion.setEnabled(false);
        ratingBar.setEnabled(false);

    }

    public  void enviar(View v){
        valcalificaion = Float.parseFloat(value);
        if (observacion.getText().toString().equals("") ) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Confirmación de datos")
                    .setMessage("Calificación: "+valcalificaion+" \n" +"Observaciones : ¿Está seguro(a) de no enviar ninguna Observación? Su opinión es importante parsa nosotros.")
                    .setCancelable(false)
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {



                            comandiNotificacion.enviarCalificaion(idServicio,  "Sin comentarios", valcalificaion);
                            Toast.makeText(getApplicationContext(), "Se envio la calificacion ", Toast.LENGTH_LONG).show();

                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });


            builder.create().show();
        }
        else{
            valcalificaion = Float.parseFloat(value);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Confirmación de datos")
                    .setMessage("Calificación: "+valcalificaion+" \n" +"Observaciones : "+ observacion.getText().toString())
                    .setCancelable(false)
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {


                            comandiNotificacion.enviarCalificaion(idServicio,  observacion.getText().toString(), valcalificaion);
                            Toast.makeText(getApplicationContext(), "Se envio la calificacion ", Toast.LENGTH_LONG).show();

                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });


            builder.create().show();


        }
    }

    /*public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == event.KEYCODE_BACK) {
            Log.v("cerrar","cerrar");
        }
        return false;
    }*/


    //ciclo de vida


    @Override
    protected void onStart() {
        super.onStart();
        modelo.appcerradaAbierta = false;
    }



    @Override
    protected void onStop() {
        super.onStop();
        Log.wtf("Ciclo de vida", "onStop");
        Start_Service();
    }

    /* mi Servicios */
    public void Start_Service(){
        modelo.appcerradaAbierta  = true;
        Intent intent = new Intent(getApplicationContext(), MiService.class);
        intent.putExtra("primerplano", false);
        startService(intent);
    }

    /* mi MiServiceForeground */
    private void MiServiceBoot() {

        Intent intent = new Intent(getApplicationContext(), ServiceBoot.class);
        intent.putExtra("primerplano", false);
        startService(intent);
    }

    public void Stop_Service(){
        modelo.appcerradaAbierta  = false;
        stopService(new Intent(getApplicationContext(), MiService.class));
    }

}
