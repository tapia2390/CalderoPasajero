package transcalderonpasajero.transcalderonpasajero;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import transcalderonpasajero.transcalderonpasajero.Adapter.HistorialAdapter;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoOrdenesPasajeroUltimosDiez;

public class HistorialUltimosServicios extends Activity implements HistorialAdapter.AdapterCallback, ComandoOrdenesPasajeroUltimosDiez.OnOrdenesPasajeroHistorialChangeListener {

    private HistorialAdapter mAdapter;
    ListView lv;
    public ProgressBar progressBar;
    Modelo modelo = Modelo.getInstance();

    TextView sindatos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_historial_ultimos_servicios);

        lv = (ListView)findViewById(R.id.lv);
        progressBar = (ProgressBar)findViewById(R.id.progressBar3);
        sindatos= (TextView) findViewById(R.id.sindatos);

        ComandoOrdenesPasajeroUltimosDiez comandoOrdenPasajero = new ComandoOrdenesPasajeroUltimosDiez(this);
        comandoOrdenPasajero.getOrdenesPasajeroHistorial();
        displayHistorial();


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getApplicationContext(),""+parent[position]+"-"+position, Toast.LENGTH_SHORT);
                //se captura la posicion
                Servicio orden = modelo.getHistorial().get(position);

                Intent intent = new Intent(getApplicationContext(), InformacionDelServicioDetallada.class);
                intent.putExtra("id", "" +orden.getId());// se envia el id de la orden segun la posicion
                intent.putExtra("fecha", "");
                startActivity(intent);
                finish();
            }
        });

        timerload();
    }

    private void displayHistorial(){
        mAdapter = new HistorialAdapter(this,this);
        lv.setAdapter(mAdapter);

    }



    @Override
    public void cargoUnaOrdenesPasajeroHistorial() {
        //modelo.filtrarUltimosDiezHistorial();
        mAdapter.notifyDataSetChanged();
        ocultartexto();
        //Log.v("tamano","Tamano"+modelo.getHistorial().size()+modelo.conductor.getNombre());

    }

    @Override
    public void cargoHisTorialP() {

    }




    public void timerload(){

        Thread  thread = new Thread(){
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(2000);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if(modelo.getHistorial().size() < 1){
                                    progressBar.setVisibility(View.INVISIBLE);
                                    sindatos.setVisibility(View.VISIBLE);
                                    return;
                                }else{
                                    progressBar.setVisibility(View.INVISIBLE);

                                }

                            }
                        });

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            };
        };
        thread.start();
    }

    public void ocultartexto(){

        Thread  thread = new Thread(){
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(500);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if(modelo.getHistorial().size() < 1){
                                    sindatos.setVisibility(View.VISIBLE);
                                    return;
                                }


                            }
                        });

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            };
        };
        thread.start();
    }



    //ciclo de vida


    @Override
    protected void onStart() {
        super.onStart();
        modelo.appcerradaAbierta = false;
    }



    @Override
    protected void onStop() {
        super.onStop();
        Log.wtf("Ciclo de vida", "onStop");
        Start_Service();
    }

    /* mi Servicios */
    public void Start_Service(){
        modelo.appcerradaAbierta  = true;
        Intent intent = new Intent(getApplicationContext(), MiService.class);
        intent.putExtra("primerplano", false);
        startService(intent);
    }

    /* mi MiServiceForeground */
    private void MiServiceBoot() {

        Intent intent = new Intent(getApplicationContext(), ServiceBoot.class);
        intent.putExtra("primerplano", false);
        startService(intent);
    }

    public void Stop_Service(){
        modelo.appcerradaAbierta  = false;
        stopService(new Intent(getApplicationContext(), MiService.class));
    }

}
