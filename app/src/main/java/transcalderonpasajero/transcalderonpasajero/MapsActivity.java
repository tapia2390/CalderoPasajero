package transcalderonpasajero.transcalderonpasajero;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import transcalderonpasajero.transcalderonpasajero.Clases.Conexion;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoCompartirUbicacion;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,ComandoCompartirUbicacion.OnCompartirUbicacionChangeListener {

    private GoogleMap mMap;


    String idServicio;
    String idBDUbicacion;
    Button button6;
    private ProgressDialog progressDialog;

    ComandoCompartirUbicacion comandoCompartirUbicacion;
    Modelo modelo = Modelo.getInstance();

    //bd
    final Context context = this;
    Conexion con;
    SQLiteDatabase miDB;
    Servicio servicio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.


        Bundle bundle = getIntent().getExtras();

        idServicio = bundle.getString("id");
        idBDUbicacion = bundle.getString("idUbi");

        comandoCompartirUbicacion  = new ComandoCompartirUbicacion(this);
       // servicio = modelo.getOrden(idServicio);

        button6 = (Button) findViewById(R.id.button6);

        progressDialog = new ProgressDialog(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
       /* LatLng sydney = new LatLng(latitud, longitud);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Mi ubicación"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
*/
        LatLng ctg = new LatLng(modelo.latitud, modelo.longitud);// colombia
        CameraPosition possiCameraPosition = new CameraPosition.Builder().target(ctg).zoom(15).bearing(0).tilt(0).build();
        CameraUpdate cam3 =
                CameraUpdateFactory.newCameraPosition(possiCameraPosition);
        mMap.animateCamera(cam3);
        mMap.addMarker(new MarkerOptions().position(ctg).title("Mi ubicación"));

        float verde = BitmapDescriptorFactory.HUE_GREEN;
        marcadorColor(modelo.latitud, modelo.longitud,"Pais Colombia", verde);
    }

    private void marcadorColor(double lat, double lng, String  pais, float color){
        mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title(pais).icon(BitmapDescriptorFactory.defaultMarker(color)));
    }

    public void atras(View v){
        atras2();
    }

    public void atras2(){
        progressDialog.dismiss();
        Intent intent = new Intent(getApplicationContext(), InformacionServicio2.class);
        intent.putExtra("id",idServicio);
        startActivity(intent);
    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == event.KEYCODE_BACK) {
            Log.v("cerrar","cerrar");
            atras2();
        }
        return true;
    }

    public  void compatirubicacion(View v){

        button6.setClickable(false);
        progressDialog.setMessage("Enviando  información, por favor espere...");
        progressDialog.show();

        if(servicio.ubicacionPasajeroGPsses.size()>0) {

            for (int i = 0; i < servicio.ubicacionPasajeroGPsses.size(); i++) {
                if (servicio.ubicacionPasajeroGPsses.get(i).getPasajero().equals(modelo.uid)) {
                    comandoCompartirUbicacion.actualizarUbicacion(modelo.latitud, modelo.longitud, idServicio, modelo.idGpsUbicacion);
                } else {
                    comandoCompartirUbicacion.enviarUbicacion(modelo.latitud, modelo.longitud, idServicio, modelo.uid);
                }
            }
        }else{
            comandoCompartirUbicacion.enviarUbicacion(modelo.latitud, modelo.longitud, idServicio, modelo.uid);
        }

    }

    @Override
    public void cargoUbicacion() {
       atras2();
    }

    @Override
    public void actualizoUbicacion() {
        atras2();
    }


    // bd
    public void abrirCone() {
        con = new Conexion(this);
        miDB = con.getWritableDatabase();
    }

    public void cerrarCone() {
        miDB.close();
    }



    public void insertUbi(){

        abrirCone();
        long result;
        ContentValues valor = new ContentValues();
        valor.put("numero_orden", ""+ servicio.getCosecutivoOrden());

        result = miDB.insert("UbicacionPasajero", null, valor);
        if (result > 0) {
            // Toast.makeText(getApplicationContext(), "exito", Toast.LENGTH_SHORT).show();
            Log.v("orden", "orden"+"ok");
        }else{
            //Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
            Log.v("grave", "grave"+"grave");
        }
        cerrarCone();
    }


    public void updateUbi() {

        abrirCone();
        ContentValues valores = new ContentValues();
        valores.put("numero_orden",""+ servicio.getCosecutivoOrden());
        long result1 = miDB.update("UbicacionPasajero", valores, "Id_Ubicacion=" + 1, null);

        //Toast.makeText(getApplicationContext(), ".... update"+result1, Toast.LENGTH_LONG).show();
        if (result1 > 0) {
            // Toast.makeText(getApplicationContext(), "Datos ingresados con exito"+result1, Toast.LENGTH_SHORT).show();
            Log.v("update", "Datos ingresados con exito");
        } else {
            //Toast.makeText(getApplicationContext(), "Datos no ingresados"+result1, Toast.LENGTH_SHORT).show();
            Log.v("no update", "Datos No ingresados ");
        }

        cerrarCone();
    }
}
