package transcalderonpasajero.transcalderonpasajero;;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;

import java.text.SimpleDateFormat;
import java.util.Date;

import transcalderonpasajero.transcalderonpasajero.Clases.Conexion;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden;
import transcalderonpasajero.transcalderonpasajero.home.NewHomeNav;


public class MiService extends Service  {


    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseAuth.AuthStateListener mAuthListener;
    Modelo modelo = Modelo.getInstance();
    private static final String TAG = "EjemploServicio";
    CmdOrden comandoP  = new CmdOrden();
    Conexion con;
    SQLiteDatabase miDB;
    int datoss=0;
    int idm;
    Context context;

    @Override
    public void onCreate(){
        super.onCreate();
        Log.d(TAG, "El servicio se ha creado");

    }


    @Override
    public void onStart(final Intent intent, final int startId)
    {
        super.onStart(intent, startId);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        //Toast.makeText(this, "Service iniciado MiService", Toast.LENGTH_SHORT).show();
        // mostrar_alertasUp();
        Log.v("iniciado MiService", " iniciado MiService");

        modelo.appcerradaAbierta  = true;



        return Service.START_STICKY;
    }




    @Override
    public void onDestroy(){
        //Toast.makeText(this, "Service finalizado MiService", Toast.LENGTH_SHORT).show();

    }



    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }



    public void notificacionOrden(Servicio orden){
        String mensaje ="";
        String time  ="";

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        if(orden.notificaciones.size() != 0)
        {
            mensaje = orden.notificaciones.get(0).getTexto();
            time = ""+orden.notificaciones.get(0).getTimestamp();
        }

        String   consecutivo = orden.getCosecutivoOrden();
        String  destino = orden.getDestino();
        String tiker = ""+orden.getFechaEnDestino();
        String estado = ""+orden.getEstado();
        String   idOr = orden.getId();
        //Date   fecha = orden.getFechaEnOrigen();
        //String fecha2 = df.format(fecha);
        String idConductor = orden.idConductor;
        // notifficacion(tiker, 1, consecutivo, destino, idOr, fecha2);


        try{
            selectDatos(idOr,estado,mensaje,time);

        }catch (Exception e){
            Log.v("exeptions","exeptions"+e);
        }


    }

    public void notifficacion(int ids, String estado, String idOr,String mensaje){

        //notifficacion(idBD,estado, idOrden,"Cambio de estado");

        int idNotificacion = ids;//0
        //Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
        Log.v("android", "ENTROOOOOOOOO1  Notificacion");

        Date date = new Date();
        // CharSequence ticker ="Fecha Destino "+tiker1;

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Intent i = new Intent(getApplicationContext(), NewHomeNav.class);
        // i.putExtra("id",idOr);


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, i, 0);
        NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);


        //Log.e("android", "ENTROOOOOOOOO2");
        Notification noti = new NotificationCompat.Builder(this)
                .setContentIntent(pendingIntent)
                //.setTicker(""+ticker)
                .setContentTitle(""+mensaje)
                .setContentText(""+estado)
                .setSmallIcon(R.drawable.icononotificacion)
                // .addAction(R.mipmap.ic_launcher, ticker, pendingIntent)
                .setVibrate(new long[] {100, 250, 100, 500})
                .setSound(soundUri)
                .setAutoCancel(true)
                .build();
        nm.notify(idNotificacion, noti);

        return;
    }


    public void abrirCone() {
        con = new Conexion(this);
        miDB = con.getWritableDatabase();
    }

    public void cerrarCone() {
        miDB.close();
    }



    public void selectDatos(String idOrden, String estado, String mensaje, String timestamp) {

        String idBD1, estadoBD, mensajeBD, timestampBD;
        int idBD = 0;
        abrirCone();
        String sql1 = "SELECT  *  FROM Notification " +
                "WHERE id_notificacionO =" + "'" + idOrden + "' ORDER BY Id_Notification ASC";
        Cursor mycur = miDB.rawQuery(sql1, null);

        if (mycur.moveToFirst()) {

            do {
                idBD = mycur.getInt(0);
                idBD1 = mycur.getString(1);
                estadoBD = mycur.getString(2);
                mensajeBD = mycur.getString(3);
                timestampBD = mycur.getString(4);

                Log.v("cantms", "cantms " + idm);

            }
            while (mycur.moveToNext());

            if (idBD > 0) {
                updateOrden(idOrden, estado, mensaje, timestamp, idBD, idBD1, estadoBD, mensajeBD, timestampBD);
                // Toast.makeText(getApplicationContext(),"esta"+idOrden,Toast.LENGTH_LONG).show();
                Log.v("esta", "esta");
            } else {
                insertOrden(idOrden, estado, mensaje, timestamp);
            }

        } else {
            if (idBD > 0) {

                Log.v("esta", "esta");
            } else {
                insertOrden(idOrden, estado, mensaje, timestamp);
            }
            cerrarCone();
        }
    }

    public void insertOrden(String idOrden, String estado, String mensaje, String timestamp){

        abrirCone();
        long result;
        ContentValues valor = new ContentValues();
        valor.put("id_notificacionO", ""+idOrden);
        valor.put("estado",estado);
        valor.put("mensaje",mensaje);
        valor.put("timestamp",timestamp);
        result = miDB.insert("Notification", null, valor);
        if (result > 0) {
            // Toast.makeText(getApplicationContext(), "exito", Toast.LENGTH_SHORT).show();
            Log.v("orden", "orden"+"ok");
/*
            if(estado.equals("NoAsignado")){
                notifficacion(1,"CalderonApp \n Tiene un nuevo servico", idOrden,"CalderonApp");
                return;
            }else{
                notifficacion(1,"Su servicio cambió de Estado, se encuentra "+estado, idOrden,"CalderonApp");
                return;
            }
            */



        }else{
            //Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
            Log.v("grave", "grave"+"grave");
        }
        cerrarCone();
    }

    public void updateOrden(String idOrden, String estado, String mensaje, String timestamp, int idBD, String idOrdenBD, String estadoBD, String mensajeBD, String timestampBD){

        /*if(!estado.equals(estadoBD)){
            if(estado.equals("NoAsignado")){
                notifficacion(idBD,"CalderonApp  \n Tiene un nuevo servico", idOrden,"Su servicio cambió de Estado, se encuentra "+estado);
            }else{
                notifficacion(idBD,"Su servicio cambió de Estado, se encuentra "+estado, idOrden,"CalderonApp");
            }
        }

        if(!timestamp.equals(timestampBD)){
            notifficacion(idBD,"Tiene un mensaje de su servicio: "+mensaje, idOrden,"CalderonApp");
        }
*/
        abrirCone();
        long result;
        ContentValues valor = new ContentValues();
        valor.put("estado",estado);
        valor.put("mensaje",mensaje);
        valor.put("timestamp",timestamp);
        result = miDB.update("Notification", valor, "id_notificacionO= '" + idOrden+"'", null);
        if (result > 0) {
            // Toast.makeText(getApplicationContext(), "U exito", Toast.LENGTH_SHORT).show();
            Log.v("orden", "orden"+"ok");


        }else{
            //Toast.makeText(getApplicationContext(), " Uerror", Toast.LENGTH_SHORT).show();
            Log.v("grave", "grave"+" U grave");
        }
        cerrarCone();
    }

}