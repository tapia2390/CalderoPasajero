package transcalderonpasajero.transcalderonpasajero;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;

public class Rutas extends Activity {
    ListView lista_rutas;
    String idServicio;
    Servicio servicio;
    Modelo modelo  = Modelo.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_rutas);


        Bundle bundle = getIntent().getExtras();
        idServicio = bundle.getString("id");
        //servicio = modelo.getOrden(idServicio);
        lista_rutas = (ListView)findViewById(R.id.lista_rutas);


        String commaSeparated = servicio.getRuta();
        ArrayList<String> items = new  ArrayList<String>(Arrays.asList(commaSeparated.split("-")));
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        lista_rutas.setAdapter(adaptador);

    }

    public void atras(View v){
        atras2();
    }

    public void atras2(){
        Intent i  = new Intent(getApplicationContext(), InformacionServicio2.class);
        i.putExtra("id", "" + idServicio);
        startActivity(i);
        finish();
    }
}
