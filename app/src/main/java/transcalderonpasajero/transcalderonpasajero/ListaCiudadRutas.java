package transcalderonpasajero.transcalderonpasajero;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.SectionIndexer;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import transcalderonpasajero.transcalderonpasajero.Clases.Ciudad;
import transcalderonpasajero.transcalderonpasajero.Clases.IndexableListView;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden.OnOrdenesListener;
import transcalderonpasajero.transcalderonpasajero.servicio.VehiculosActivity;
import transcalderonpasajero.transcalderonpasajero.servicio.listados.ListaServicios;

public class ListaCiudadRutas extends Activity {


    private static final List<Ciudad> countryList = new ArrayList<Ciudad>();
    private IndexableListView listView;
    private EditText search;
    TextView nombres;
    private EfficientAdapter adapter;
    Modelo modelo = Modelo.getInstance();;
    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_lista_ciudad_rutas);

        listaCiudades();


    }


    private void listaCiudades() {


        if (estaConectado()) {

            countryList.clear();
            modelo.data.clear();


            for (int i = 0; i < modelo.ciudades.size(); i++) {

                countryList.add(modelo.ciudades.get(i));
            }

            for (char c = 'A'; c <= 'Z'; c++) {
                Ciudad letra = new Ciudad();
                letra.nombre = "" + c;
                countryList.add(letra);
            }

            listView = (IndexableListView) findViewById(R.id.listView_main_menu);
            nombres = findViewById(R.id.nombres);
            setNombre();

            //Collections.sort(countryList);
            adapter = new EfficientAdapter(this);
            listView.setAdapter(adapter);
            listView.setFastScrollEnabled(true);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapter, View view, int pos, long id) {
                    String countryName = modelo.data.get(pos).nombre;


                    for (char c = 'A'; c <= 'Z'; c++) {
                        if (countryName.equals(c)) {
                            return;
                        } else {
                            continue;
                        }
                    }

                    if (countryName.equalsIgnoreCase("A") || countryName.equalsIgnoreCase("B") || countryName.equalsIgnoreCase("C") || countryName.equalsIgnoreCase("D") ||
                            countryName.equalsIgnoreCase("E") || countryName.equalsIgnoreCase("F") || countryName.equalsIgnoreCase("G") || countryName.equalsIgnoreCase("H") ||
                            countryName.equalsIgnoreCase("I") || countryName.equalsIgnoreCase("J") || countryName.equalsIgnoreCase("K") || countryName.equalsIgnoreCase("L") ||
                            countryName.equalsIgnoreCase("M") || countryName.equalsIgnoreCase("N") || countryName.equalsIgnoreCase("O") || countryName.equalsIgnoreCase("P") ||
                            countryName.equalsIgnoreCase("Q") || countryName.equalsIgnoreCase("R") || countryName.equalsIgnoreCase("S") || countryName.equalsIgnoreCase("T") ||
                            countryName.equalsIgnoreCase("U") || countryName.equalsIgnoreCase("V") || countryName.equalsIgnoreCase("W") || countryName.equalsIgnoreCase("X") ||
                            countryName.equalsIgnoreCase("Y") || countryName.equalsIgnoreCase("Z")) {

                    } else {

                        modelo.currentService.addEliminarTrayecto(countryName);
                        CmdOrden.saveCurrentService();
                        setNombre();
                    }

                }
            });


            search = (EditText) findViewById(R.id.editText_main_search);
            search.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    // Toast.makeText(Medicamentos.this,"posición: "+start+"id: "+before, Toast.LENGTH_LONG).show();
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {
                    //  Toast.makeText(Medicamentos.this,"posición: "+start+"id: "+count+"3: "+after, Toast.LENGTH_LONG).show();

                }

                @Override
                public void afterTextChanged(Editable s) {

                    adapter.getFilter().filter(s);

                }
            });

        } else {
            showAlertSinInternet();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        listaCiudades();

        CmdOrden.getOrdenesPasajero(new OnOrdenesListener() {
            @Override
            public void nueva() {

            }


            @Override
            public void modificada(String idServicio , boolean cambioEstado) {

                if (cambioEstado){

                    Servicio servicio  = modelo.getOrdenById(idServicio);
                    if (servicio == null){
                        return;
                    }

                    android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                            ListaCiudadRutas.this);
                    alertDialogBuilder.setTitle("Estado de servicio");
                    alertDialogBuilder
                            .setMessage("El servicio " + servicio.cosecutivoOrden + " cambió de estado, se encuentra " + servicio.getEstadoLeible())
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    return;
                                }
                            });

                    // create alert dialog
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                }

            }


            @Override
            public void eliminada() {

            }
        });



    }

    @Override
    protected void onPause() {
        super.onPause();
        modelo.pararListeners();

    }


    public void atras() {
        modelo.data.clear();
        finish();
    }



    public  void setNombre(){

        String paradas = modelo.currentService.getRuta();

        if (paradas.equals("")){
            nombres.setVisibility(View.GONE);
        }else{
            nombres.setVisibility(View.VISIBLE);
            nombres.setText(paradas);
        }
    }




    /******************************** EfficientAdapter ************************************/

    public class EfficientAdapter extends BaseAdapter implements SectionIndexer, Filterable {

        private Filter filter;
        private String mSections = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private LayoutInflater mInflater;
        @SuppressWarnings("unused")
        private String TAG = EfficientAdapter.class.getSimpleName();
        @SuppressWarnings("unused")
        private Context context;

        Modelo modelo = Modelo.getInstance();


        public EfficientAdapter(Context context) {
            mInflater = LayoutInflater.from(context);
            this.context = context;
            this.modelo.data.addAll(countryList);

        }


        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {

            ViewHolder holder;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.main_list, parent, false);
                holder = new ViewHolder();
                holder.textLine = (TextView) convertView.findViewById(R.id.textView_main_item);
                holder.textDep = (TextView) convertView.findViewById(R.id.textView_main_dep);
                holder.textSeperater = (TextView) convertView.findViewById(R.id.textView_main_seperater);
                convertView.setTag(holder);
            }
            holder = (ViewHolder) convertView.getTag();


            Collections.sort(modelo.data, new Comparator<Ciudad>() {
                        @Override
                        public int compare(Ciudad c1, Ciudad c2) {
                            return c1.nombre.compareTo(c2.nombre);
                        }
                    }
            );

            Ciudad ciu = (Ciudad) getItem(position);
            if (ciu.nombre.length() <= 1) {
                holder.textLine.setVisibility(View.GONE);
                holder.textDep.setVisibility(View.GONE);
                holder.textSeperater.setVisibility(View.VISIBLE);
                holder.textSeperater.setText(ciu.nombre.substring(0,1));


            } else {
                holder.textLine.setVisibility(View.VISIBLE);
                holder.textDep.setVisibility(View.VISIBLE);
                holder.textSeperater.setVisibility(View.GONE);
                holder.textLine.setText(ciu.nombre);
                holder.textDep.setText(ciu.departamento);


            }

            return convertView;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return modelo.data.size();
        }

        @Override
        public Object getItem(int position) {


            return modelo.data.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        class ViewHolder {
            TextView textLine;
            TextView textDep;
            TextView textSeperater;

        }

        @Override
        public Filter getFilter() {
            if (filter == null)
                filter = new MangaNameFilter();
            return filter;
        }

        /************** sectionIndexer Overriding Functions **************/

        @Override
        public int getPositionForSection(int section) {
            // If there is no item for current section, previous section will be selected
            for (int i = section; i >= 0; i--) {
                for (int j = 0; j < getCount(); j++) {
                    if (i == 0) {
                        // For numeric section
                        for (int k = 0; k <= 9; k++) {
                            if (match(String.valueOf(((Ciudad)getItem(j)).nombre.charAt(0)), String.valueOf(k)))
                                return j;
                        }
                    } else {
                        if (match(String.valueOf(((Ciudad)getItem(j)).nombre.charAt(0)), String.valueOf(mSections.charAt(i))))
                            return j;
                    }
                }
            }
            return 0;
        }

        @Override
        public int getSectionForPosition(int position) {
            return position;
        }

        @Override
        public Object[] getSections() {
            String[] sections = new String[mSections.length()];
            for (int i = 0; i < mSections.length(); i++)
                sections[i] = String.valueOf(mSections.charAt(i));
            return sections;
        }

        /************** MangaNameFilter Class **************/
        private class MangaNameFilter extends Filter {

            final List<Ciudad> list = new ArrayList<Ciudad>();

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                constraint = constraint.toString().toLowerCase();
                FilterResults result = new FilterResults();

                if (constraint != null && constraint.toString().length() > 0) {

                    ArrayList<Ciudad> allItems = null;
                    synchronized (this) {
                        allItems = new ArrayList<>(countryList);
                    }

                    list.clear();

                    for (int i = 0; i < allItems.size(); i++) {
                        String item = allItems.get(i).nombre.toLowerCase();
                        if (item.startsWith(constraint + "")) {
                            list.add(allItems.get(i));
                        }
                    }

                    result.count = list.size();
                    result.values = list;

                } else {
                    synchronized (this) {
                        list.clear();
                        for (int i = 0; i < countryList.size(); i++) {
                            list.add(countryList.get(i));
                        }

                        result.values = list;
                        result.count = list.size();
                    }
                }
                return result;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                modelo.data = (ArrayList<Ciudad>) results.values;
                notifyDataSetChanged();
            }
        }

    } //EfficientAdapter class ends

    /************** Function for sectionIndexer **************/

    public boolean match(String value, String keyword) {
        if (value == null || keyword == null)
            return false;
        if (keyword.length() > value.length())
            return false;

        int i = 0, j = 0;
        do {
            if (keyword.charAt(j) == value.charAt(i)) {
                i++;
                j++;
            } else if (j > 0)
                break;
            else
                i++;
        } while (i < value.length() && j < keyword.length());

        return (j == keyword.length()) ? true : false;
    }


//validacion a internet


    //validacion conexion internet
    protected Boolean estaConectado() {
        if (conectadoWifi()) {
            Log.v("wifi", "Tu Dispositivo tiene Conexion a Wifi.");
            return true;
        } else {
            if (conectadoRedMovil()) {
                Log.v("Datos", "Tu Dispositivo tiene Conexion Movil.");
                return true;
            } else {
                showAlertSinInternet();
                // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
    }

    protected Boolean conectadoWifi() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    protected Boolean conectadoRedMovil() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }


    public void showAlertSinInternet() {

        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("Sin Internet");

        // set dialog message
        alertDialogBuilder
                .setMessage("Sin Conexión a Internet")
                .setCancelable(false)
                .setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Intent inte = new Intent(getBaseContext(), Splash.class);
                        startActivity(inte);
                    }
                });

        // create alert dialog
        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }
}