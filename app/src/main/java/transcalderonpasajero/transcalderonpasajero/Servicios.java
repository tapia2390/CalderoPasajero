package transcalderonpasajero.transcalderonpasajero;

import android.Manifest.permission;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.FrameMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.support.v7.widget.SwitchCompat;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Date;
import java.util.Locale;

import android.location.Address;

import java.io.IOException;
import java.util.List;

import transcalderonpasajero.transcalderonpasajero.Clases.Ciudad;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Pasajero;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.Clases.Ubicacion;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdEmpresa;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdEmpresa.OnEmpresaListener;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden.OnOrdenesListener;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoCiudades;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoPasajero;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoPasajero.OnCambioEstado;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoPasajero.OnPasajeroChangeListener;
import transcalderonpasajero.transcalderonpasajero.home.NewHomeNav;
import transcalderonpasajero.transcalderonpasajero.servicio.AyudaServicios;
import transcalderonpasajero.transcalderonpasajero.servicio.ConfirmarServicio;
import transcalderonpasajero.transcalderonpasajero.servicio.Duracion;
import transcalderonpasajero.transcalderonpasajero.servicio.OtrasOpciones;
import transcalderonpasajero.transcalderonpasajero.servicio.ProgramarServicio;
import transcalderonpasajero.transcalderonpasajero.servicio.listados.ListaServicios;
import transcalderonpasajero.transcalderonpasajero.sistema.Utility;



public class Servicios extends Fragment implements OnMapReadyCallback, ComandoCiudades.OnCiudadesChangeListener {

    RelativeLayout relativeinicio, relativellegada;
    TextView txt_ciudad_Inicio, txt_ciudad_llegada , duracion, numeroNoti, tarifa;
    EditText dirOrigen;
    ImageView gps1, gps2, btnReloj;
    EditText dirdestino;
    SwitchCompat swich_servicio_domcilio;
    Button pideYa, programar, btnOpcionesPeq;

    Modelo modelo = Modelo.getInstance();
    //final Context context = this;
    private GoogleMap mMap;

    GoogleMap googleMap2;
    RelativeLayout rel_dirdestino;
    LinearLayout rel_destino_abierto, permisos, linMasOpciones;
    FrameLayout layerRojo;

    ComandoCiudades comandoCiudades;

    LocationRequest mLocationRequest;
    private boolean mLocationPermissionGranted = false;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 2;
    Marker mDestino, mOrigen = null;

    public boolean salvarAlCheck =  true;
    public boolean mapaYa = false;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.activity_servicios, null);
        return root;


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map2);
        mapFragment.getMapAsync(this);


        dirOrigen = (EditText) view.findViewById(R.id.direcion);
        dirdestino = (EditText) view.findViewById(R.id.dirdestino);
        swich_servicio_domcilio = (SwitchCompat) view.findViewById(R.id.swich_servicio_domcilio);
        rel_dirdestino = view.findViewById(R.id.rel_dirdestino);
        rel_destino_abierto = view.findViewById(R.id.rel_destino_abierto);
        txt_ciudad_Inicio = (TextView) view.findViewById(R.id.txt_ciudad_Inicio);
        txt_ciudad_llegada = (TextView) view.findViewById(R.id.txt_ciudad_llegada);
        duracion = view.findViewById(R.id.duracion);
        btnReloj = view.findViewById(R.id.btnreloj);
        permisos = view.findViewById(R.id.permisos);
        gps1 = view.findViewById(R.id.gps1);
        gps2 = view.findViewById(R.id.gps2);
        linMasOpciones = view.findViewById(R.id.linMasOpciones);
        btnOpcionesPeq = view.findViewById(R.id.btnOpcionesPeq);
        numeroNoti = view.findViewById(R.id.numeroNoti);
        layerRojo = view.findViewById(R.id.layerrojo);
        tarifa = view.findViewById(R.id.tarifa);

        relativeinicio = (RelativeLayout) view.findViewById(R.id.relativeinicio);
        relativellegada = (RelativeLayout) view.findViewById(R.id.relativellegada);

        pideYa = view.findViewById(R.id.pideYa);
        programar = view.findViewById(R.id.programar);



        relativeinicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), ListaCiudadOrigenDestino.class);
                i.putExtra("CIUDAD", "ORIGEN");
                startActivity(i);
            }
        });


        relativellegada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (txt_ciudad_llegada.getText().toString().equals("Abierto")){
                    return;
                }

                Intent i = new Intent(getActivity(), ListaCiudadOrigenDestino.class);
                i.putExtra("CIUDAD", "DESTINO");
                startActivity(i);
            }
        });


        swich_servicio_domcilio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //Toast.makeText(getApplicationContext(),"true",Toast.LENGTH_SHORT).show();
                    rel_dirdestino.setVisibility(View.GONE);
                    rel_destino_abierto.setVisibility(View.VISIBLE);
                    modelo.currentService.abierto = true;
                    txt_ciudad_llegada.setText("ABIERTO");
                    modelo.currentService.direccionDestino = "";
                    modelo.currentService.destino = "ABIERTO";
                    if (mDestino != null){
                        mDestino.remove();
                    }



                } else {
                    //Toast.makeText(getApplicationContext(),"false",Toast.LENGTH_SHORT).show();
                    txt_ciudad_llegada.setText(txt_ciudad_Inicio.getText().toString());
                    rel_dirdestino.setVisibility(View.VISIBLE);
                    rel_destino_abierto.setVisibility(View.GONE);
                    modelo.currentService.abierto = false;
                    modelo.currentService.origen = txt_ciudad_Inicio.getText().toString();
                    modelo.currentService.destino = txt_ciudad_llegada.getText().toString();

                }

                if (salvarAlCheck){
                    CmdOrden.saveCurrentService();
                }

                salvarAlCheck = true;
                pintarTarifa();

            }
        });



        ComandoPasajero.escucharEstado(new OnCambioEstado() {
            @Override
            public void aprobado() {
                btnReloj.setImageResource(R.drawable.btn_servicios_blanco_60_i5);

                ComandoPasajero.getPasajero(modelo.uid, new OnPasajeroChangeListener() {
                    @Override
                    public void cargoPasajero(Pasajero pasajero) {

                        modelo.pasajero = pasajero;

                        CmdEmpresa.getEmpresa(new OnEmpresaListener() {
                            @Override
                            public void cargoEmpresa() {

                            }
                        });
                    }


                });

                if (modelo.pasajero.estado.equals("aprobado") && modelo.empresa.puedePedirYa()){
                    pideYa.setVisibility(View.VISIBLE);
                }else {
                    pideYa.setVisibility(View.GONE);
                }


                if (modelo.pasajero.estado.equals("aprobado")){
                    programar.setVisibility(View.VISIBLE);
                }else {
                    programar.setVisibility(View.GONE);
                }


            }

            @Override
            public void pendiente() {
                btnReloj.setImageResource(R.drawable.reloj_arena);
                if (modelo.pasajero.estado.equals("aprobado") && modelo.empresa.puedePedirYa()){
                    pideYa.setVisibility(View.VISIBLE);
                }else {
                    pideYa.setVisibility(View.GONE);
                }


                if (modelo.pasajero.estado.equals("aprobado")){
                    programar.setVisibility(View.VISIBLE);
                }else {
                    programar.setVisibility(View.GONE);
                }

            }
        });

        dirOrigen.setImeActionLabel("Listo", KeyEvent.KEYCODE_ENTER);
        dirOrigen.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                putMarcaOrigen();
                modelo.currentService.origen = txt_ciudad_Inicio.getText().toString();
                modelo.currentService.direccionOrigen = dirOrigen.getText().toString();
                CmdOrden.saveCurrentService();
                pintarTarifa();
                return false;
            }
        });

        dirdestino.setImeActionLabel("Listo", KeyEvent.KEYCODE_ENTER);
        dirdestino.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                if (txt_ciudad_llegada.getText().equals("")){
                    txt_ciudad_llegada.setText(txt_ciudad_Inicio.getText().toString());
                }
                putMarcaDestino();
                modelo.currentService.origen = txt_ciudad_Inicio.getText().toString();
                CmdOrden.saveCurrentService();
                pintarTarifa();
                return false;
            }
        });

        pideYa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (modelo.currentService.faltaConfigurarAbierto()) {
                    android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                            getActivity());
                    alertDialogBuilder.setTitle("Falta Información");
                    alertDialogBuilder
                            .setMessage("Debes especificar la duración")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    return;
                                }
                            });

                    // create alert dialog
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                    return;
                }

                if (!modelo.currentService.abierto){

                    if (!modelo.currentService.hasDestinoDefinido()){
                        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                                getActivity());
                        alertDialogBuilder.setTitle("Falta Información");
                        alertDialogBuilder
                                .setMessage("Ingresa la dirección del destino")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        return;
                                    }
                                });

                        // create alert dialog
                        android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                        return;
                    }

                }


                modelo.currentService.origen = txt_ciudad_Inicio.getText().toString();
                modelo.currentService.direccionOrigen = dirOrigen.getText().toString();
                modelo.currentService.fechaEnOrigen = Utility.convertDateToString(new Date());
                modelo.currentService.horaEnOrigen  = Utility.getHora();
                CmdOrden.saveCurrentService();

                Intent i = new Intent(getActivity(), ConfirmarServicio.class);
                i.putExtra("IDSERVICIO","CURRENT");
                startActivity(i);
            }
        });



        programar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (modelo.currentService.faltaConfigurarAbierto()) {
                    android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                            getActivity());
                    alertDialogBuilder.setTitle("Falta Información");
                    alertDialogBuilder
                            .setMessage("Debes especificar la duración")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    return;
                                }
                            });

                    // create alert dialog
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                    return;
                }

                if (!modelo.currentService.abierto){

                    if (!modelo.currentService.hasDestinoDefinido()){
                        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                                getActivity());
                        alertDialogBuilder.setTitle("Falta Información");
                        alertDialogBuilder
                                .setMessage("Ingresa la dirección del destino")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        return;
                                    }
                                });

                        // create alert dialog
                        android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                        return;
                    }


                }

                modelo.currentService.origen = txt_ciudad_Inicio.getText().toString();
                modelo.currentService.direccionOrigen = dirOrigen.getText().toString();
                CmdOrden.saveCurrentService();
                Intent i = new Intent(getActivity(), ProgramarServicio.class);
                startActivity(i);
            }
        });


        if (modelo.pasajero.estado.equals("aprobado") && modelo.empresa.puedePedirYa()){
            pideYa.setVisibility(View.VISIBLE);
        }else {
            pideYa.setVisibility(View.GONE);
        }


        if (modelo.pasajero.estado.equals("aprobado")){
            programar.setVisibility(View.VISIBLE);
        }else {
            programar.setVisibility(View.GONE);
        }





        btnReloj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (modelo.pasajero.estado.equals("aprobado")) {
                    Intent i = new Intent(getActivity(), ListaServicios.class);
                    startActivity(i);
                    return;
                }

                android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                        getActivity());
                alertDialogBuilder.setTitle("Registro en validación");
                alertDialogBuilder
                        .setMessage("Sus datos están siendo validados por el responsable de la empresa. Si tiene dudas, se puede comunicar con ellos para más información")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                return;
                            }
                        });

                // create alert dialog
                android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
                return;



            }
        });


        gps1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                putMarcaOrigen();
            }
        });


        gps2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                putMarcaDestino();
            }
        });

        rel_destino_abierto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), Duracion.class);
                startActivity(i);

            }
        });

        linMasOpciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), OtrasOpciones.class);
                startActivity(i);
            }
        });

        btnOpcionesPeq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), OtrasOpciones.class);
                startActivity(i);
            }
        });


    }


    @Override
    public void onPause() {
        super.onPause();
        modelo.pararListeners();
    }




    @Override
    public void onResume() {
        super.onResume();


        if (mDestino != null && mapaYa) {
            mDestino.remove();
        }

        if (modelo.currentService.direccionDestino == null){
            dirdestino.setText("");
        }

        if (modelo.currentService.origen == null){
            salvarAlCheck = false;
            dirdestino.setText("");
            swich_servicio_domcilio.setChecked(false);
            putMarcaOrigen();

        }



        if (modelo.currentService.origen != null){
            txt_ciudad_Inicio.setText(modelo.currentService.origen);
        }

        if (modelo.currentService.destino != null){
            txt_ciudad_llegada.setText(modelo.currentService.destino);
        }
        else {
            if (modelo.currentService.origen != null){
                txt_ciudad_llegada.setText(modelo.currentService.origen);
            }else {
                txt_ciudad_llegada.setText(txt_ciudad_Inicio.getText());
            }

        }


        if (modelo.lastCiudad.equals("ORIGEN")){
            putMarcaOrigen();
        }
        else{
            putMarcaDestino();
        }

        if (modelo.currentService.abierto){
            txt_ciudad_llegada.setText("ABIERTO");
            duracion.setText(modelo.currentService.getDuracionCadena());
        }
        else {
            salvarAlCheck = false;
            swich_servicio_domcilio.setChecked(false);

        }





        if (numeroNoti != null) {
            if (modelo.getNumeroServicios()> 0){
                layerRojo.setVisibility(View.VISIBLE);
            }else {
                layerRojo.setVisibility(View.GONE);
            }

            numeroNoti.setText("" +  modelo.getNumeroServicios());
        }



        pintarTarifa();

        CmdOrden.getOrdenesPasajero(new OnOrdenesListener() {
            @Override
            public void nueva() {
                if (modelo.getNumeroServicios()> 0){
                    layerRojo.setVisibility(View.VISIBLE);
                }else {
                    layerRojo.setVisibility(View.GONE);
                }
                numeroNoti.setText("" + modelo.getNumeroServicios());
            }


            @Override
            public void modificada(final String idServicio , boolean cambioEstado) {

                if (modelo.getNumeroServicios()> 0){
                    layerRojo.setVisibility(View.VISIBLE);
                }else {
                    layerRojo.setVisibility(View.GONE);
                }
                numeroNoti.setText("" + modelo.getNumeroServicios());

                if (cambioEstado){

                    Servicio servicio  = modelo.getOrdenById(idServicio);
                    if (servicio == null){
                        return;
                    }

                    if (servicio.estado.equals("Finalizado")){
                        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                                getActivity());
                        alertDialogBuilder.setTitle("Servicio Finalizado");
                        alertDialogBuilder
                                .setMessage("El servicio " + servicio.cosecutivoOrden + " ha terminado, por favor dale una calificación para poder mejorar el servicio")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent i = new Intent(getActivity(), InformacionDelServicioDetallada.class);
                                        i.putExtra("id",idServicio);
                                        startActivity(i);
                                        return;
                                    }
                                });

                        // create alert dialog
                        android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();

                        return;

                    }

                    android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                            getActivity());
                    alertDialogBuilder.setTitle("Estado de servicio");
                    alertDialogBuilder
                            .setMessage("El servicio " + servicio.cosecutivoOrden + " cambió de estado, se encuentra " + servicio.getEstadoLeible())
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    return;
                                }
                            });

                    // create alert dialog
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                }


            }


            @Override
            public void eliminada() {
                //numeroNoti.setText(modelo.getNumeroServicios());


            }
        });


    }


    public void pintarTarifa(){

        String valor = modelo.currentService.getValorAproximado();
        if (valor.equals("")){
            tarifa.setVisibility(View.GONE);
        }
        else {
            tarifa.setVisibility(View.VISIBLE);
            tarifa.setText(valor);
        }

    }



    public void pintarCurrentService(){

        txt_ciudad_Inicio.setText(modelo.currentService.origen);
        if (modelo.currentService.destino == null){
            txt_ciudad_llegada.setText(txt_ciudad_Inicio.getText().toString());
        }else {
            txt_ciudad_llegada.setText(modelo.currentService.destino);
        }

        dirOrigen.setText(modelo.currentService.direccionOrigen);
        dirdestino.setText(modelo.currentService.direccionDestino);
        putMarcaDestino();
        putMarcaOrigen();

        pintarTarifa();

        duracion.setText(modelo.currentService.getDuracionCadena());


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        getLocationPermission();
        this.googleMap2 = googleMap;
        LatLng ctg = new LatLng(4.597014, 74.0728759);  // La candelaria
        CameraPosition possiCameraPosition = new CameraPosition.Builder().target(ctg).zoom(16).bearing(0).tilt(0).build();
        CameraUpdate cam3 =
                CameraUpdateFactory.newCameraPosition(possiCameraPosition);
        googleMap.moveCamera(cam3);
        mapa();
    }



    public void mapa() {

        if (googleMap2 == null) {
            return;
        }

        mMap = googleMap2;

        mapaYa = true;


        if (!checkLocationOn()) {

            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                    getActivity());

            // set title
            alertDialogBuilder.setTitle("¡Lo sentimos!");

            // set dialog message
            alertDialogBuilder
                    .setMessage("Active su Ubicación (gps) en su dispositivo para poder continuar")
                    .setNegativeButton("Cancelar", new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            return;
                        }
                    })
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(myIntent);
                            return;

                        }
                    });

            // create alert dialog
            android.app.AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();

            return;

        } else {

            if (mLocationPermissionGranted) {

                permisos.setVisibility(View.VISIBLE);

                if (ActivityCompat.checkSelfPermission(getContext(), permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            }

            permisos.setVisibility(View.GONE);

           /* if (mOrigen != null){
                mOrigen.remove();
            }

            if (modelo.latitud == 0.0){
                return;
            }


            LatLng ctg;
            if (modelo.currentService.ubiOrigen == null){
                ctg = new LatLng(modelo.latitud, modelo.longitud);
            }else {
                ctg = new LatLng(modelo.currentService.ubiOrigen.latitud, modelo.currentService.ubiOrigen.longitud);
            }

            CameraPosition possiCameraPosition = new CameraPosition.Builder().target(ctg).zoom(16).bearing(0).tilt(0).build();
            CameraUpdate cam3 = CameraUpdateFactory.newCameraPosition(possiCameraPosition);
            mMap.moveCamera(cam3);
            mOrigen = mMap.addMarker(new MarkerOptions().position(ctg).title("Origen"));
            mOrigen.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.img_pin));
            Ubicacion ubi = new Ubicacion();
            ubi.longitud = modelo.longitud;
            ubi.latitud = modelo.latitud;
            modelo.currentService.ubiOrigen = ubi;*/
            //CmdOrden.saveCurrentService();


            //float verde = BitmapDescriptorFactory.HUE_GREEN;
            //marcadorColor(modelo.latitud, modelo.longitud, "Pais Colombia", verde);

        }

    }

    private void putMarcaOrigen(){


        if (!mapaYa) {
            return;
        }

        if (mOrigen != null){
            mOrigen.remove();
        }
        String ciu = "";

        if (modelo.currentService != null){
            ciu = modelo.currentService.origen;
        }
        else{
            ciu = txt_ciudad_Inicio.getText().toString();
        }


        Address dir = getLocationFromAddress(dirOrigen.getText().toString(), ciu);
        if (dir == null){
            return;
        }
        LatLng ctg = new LatLng(dir.getLatitude(),dir.getLongitude());
        CameraPosition possiCameraPosition = new CameraPosition.Builder().target(ctg).zoom(16).bearing(0).tilt(0).build();
        CameraUpdate cam3 =
                CameraUpdateFactory.newCameraPosition(possiCameraPosition);
        mMap.animateCamera(cam3);
        mOrigen = mMap.addMarker(new MarkerOptions().position(ctg).title("Origen"));
        mOrigen.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.img_pin));


        Ubicacion ubiOrigen = new Ubicacion();
        ubiOrigen.latitud = dir.getLatitude();
        ubiOrigen.longitud = dir.getLongitude();
        modelo.currentService.ubiOrigen = ubiOrigen;



    }

    private void putMarcaDestino(){


        if (!mapaYa) {
            return;
        }

        if (mDestino != null){
            mDestino.remove();
        }
        String ciu = "";

        if (modelo.currentService.destino != null){
            ciu = modelo.currentService.destino;
        }
        else{
            ciu = txt_ciudad_llegada.getText().toString();
        }

        Address dir = getLocationFromAddress(dirdestino.getText().toString(), ciu);
        if (dir == null){
            return;
        }
        LatLng ctg = new LatLng(dir.getLatitude(),dir.getLongitude());
        CameraPosition possiCameraPosition = new CameraPosition.Builder().target(ctg).zoom(16).bearing(0).tilt(0).build();
        CameraUpdate cam3 =
                CameraUpdateFactory.newCameraPosition(possiCameraPosition);

        if (mMap == null){
            return;
        }

        mMap.animateCamera(cam3);
        mDestino = mMap.addMarker(new MarkerOptions().position(ctg).title("Destino"));
        mDestino.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.img_pin_destino));


        modelo.currentService.destino = ciu;
        modelo.currentService.direccionDestino = dirdestino.getText().toString();
        modelo.currentService.direccionOrigen = dirOrigen.getText().toString();

        Ubicacion ubiDestino = new Ubicacion();
        ubiDestino.latitud = dir.getLatitude();
        ubiDestino.longitud = dir.getLongitude();
        modelo.currentService.ubiDestino = ubiDestino;

    }



    private void marcadorColor(double lat, double lng, String pais, float color) {
        //mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title(pais).icon(BitmapDescriptorFactory.defaultMarker(color)));
    }




    public boolean checkLocationOn() {


        final Context context = getContext();
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {

        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            // notify user
            return false;

        }

        return true;

    }



    /**
     * Prompts the user for permission to use the device location.
     */
    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */

        if (ContextCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            startLocationUpdates();
        } else {

            requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

        }

    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                    startLocationUpdates();
                }
         }
        }
        //updateLocationUI();
    }


    @Override
    public void cargoCiudades() {
        modelo.ciudades.size();
    }

    @Override
    public void cargoVehiculos() {
        modelo.listavehiculos.size();
    }


    // Trigger new location updates at interval
    protected void startLocationUpdates() {

        // Create the location request to start receiving updates
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(100);
        mLocationRequest.setFastestInterval(50);
        mLocationRequest.setNumUpdates(1);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(getContext());
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(getContext(), permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.getFusedLocationProviderClient(getContext()).requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        // do work here
                        onLocationChanged(locationResult.getLastLocation());
                    }
                },
                Looper.myLooper());
    }




    public void onLocationChanged(Location loc) {



        // You can now create a LatLng Object for use with maps
        modelo.latitud = loc.getLatitude();
        modelo.longitud = loc.getLongitude();


        LatLng ctg;
        if (modelo.currentService.origenEstaFull()){
            ctg = new LatLng(modelo.currentService.ubiOrigen.latitud, modelo.currentService.ubiOrigen.longitud);
            CameraPosition possiCameraPosition = new CameraPosition.Builder().target(ctg).zoom(16).bearing(0).tilt(0).build();
            CameraUpdate cam3 = CameraUpdateFactory.newCameraPosition(possiCameraPosition);
            mMap.moveCamera(cam3);
            mOrigen = mMap.addMarker(new MarkerOptions().position(ctg).title("Origen"));
            mOrigen.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.img_pin));
            dirOrigen.setText(modelo.currentService.direccionOrigen);
            txt_ciudad_Inicio.setText(modelo.currentService.origen);
            txt_ciudad_llegada.setText(modelo.currentService.origen);
        }
        else {
            Ubicacion ubi = new Ubicacion();
            ubi.longitud = modelo.longitud;
            ubi.latitud = modelo.latitud;
            modelo.currentService.ubiOrigen = ubi;

            ctg = new LatLng(modelo.latitud, modelo.longitud);

            CameraPosition possiCameraPosition = new CameraPosition.Builder().target(ctg).zoom(16).bearing(0).tilt(0).build();
            CameraUpdate cam3 = CameraUpdateFactory.newCameraPosition(possiCameraPosition);
            mMap.moveCamera(cam3);
            mOrigen = mMap.addMarker(new MarkerOptions().position(ctg).title("Origen"));
            mOrigen.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.img_pin));

            try {
                Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
                List<Address> list = geocoder.getFromLocation(
                        loc.getLatitude(), loc.getLongitude(), 1);
                if (!list.isEmpty()) {
                    Address dir = list.get(0);
                    dirOrigen.setText(adressToString(dir));
                    txt_ciudad_Inicio.setText(adressToCiudad(dir));
                    txt_ciudad_llegada.setText(adressToCiudad(dir));
                    modelo.currentService.origen = adressToCiudad(dir);
                    if (modelo.currentService.origen.equals("")){
                        modelo.currentService.origen = null;
                    }

                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }


        if (modelo.currentService.destinoEstaFull() && modelo.currentService.abierto == false){
            ctg = new LatLng(modelo.currentService.ubiDestino.latitud, modelo.currentService.ubiDestino.longitud);
            CameraPosition possiCameraPosition = new CameraPosition.Builder().target(ctg).zoom(16).bearing(0).tilt(0).build();
            CameraUpdate cam3 = CameraUpdateFactory.newCameraPosition(possiCameraPosition);
            mMap.moveCamera(cam3);
            mDestino = mMap.addMarker(new MarkerOptions().position(ctg).title("Destino"));
            mDestino.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.img_pin_destino));
            dirdestino.setText(modelo.currentService.direccionDestino);
            txt_ciudad_llegada.setText(modelo.currentService.destino);
        }


        if  (modelo.currentService.abierto == true){
            salvarAlCheck = false;
            swich_servicio_domcilio.setChecked(true);
            duracion.setText(modelo.currentService.getDuracionCadena());
            txt_ciudad_llegada.setText("ABIERTO");

        }


    }


    public String adressToString(Address ladir){

        final String ciudad = ladir.getLocality();
        final String uno = ladir.getThoroughfare();
        final String dos = ladir.getSubThoroughfare();
        final String pais = ladir.getCountryName();
        final String barrio = ladir.getFeatureName();

        if (dos != null){
            return uno + " " + dos;
        }else if (uno != null){
            return  uno;
        }else {
            if (barrio != null) {
                return barrio;
            }
        }

        return "Desconocida";
    }


    public String adressToCiudad(Address ladir){

        return  ladir.getLocality();

    }

    public Address getLocationFromAddress(String strAddress, String ciudad) {

        Geocoder coder = new Geocoder(getContext());
        List<Address> address;

        try {
            address = coder.getFromLocationName(strAddress + " ," + ciudad  , 1);
            if (address == null) {
                return null;
            }
            return address.get(0);

        } catch (Exception e) {
            return null;
        }
    }



}

