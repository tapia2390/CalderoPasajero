package transcalderonpasajero.transcalderonpasajero;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Pasajero;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdEmpresa;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdEmpresa.OnEmpresaListener;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoPasajero;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoPasajero.OnPasajeroChangeListener;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoValidarUsuario;
import transcalderonpasajero.transcalderonpasajero.home.NewHomeNav;

public class MainActivity extends Activity  implements  ComandoValidarUsuario.OnOValidarUsuarioChangeListener{

    private static final String TAG ="EmailPassword";
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseAuth.AuthStateListener mAuthListener;
    private EditText email,password;
    private ProgressBar progressBar;
    private ProgressDialog progressDialog;
    Button button;
    Modelo modelo = Modelo.getInstance();
    ComandoValidarUsuario comandoValidarUsuario;
    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);



        email =(EditText)findViewById(R.id.correo);
        password =(EditText)findViewById(R.id.contrasena);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        button = (Button) findViewById(R.id.button);

        progressDialog = new ProgressDialog(this);
        comandoValidarUsuario = new ComandoValidarUsuario(this);
        // autenticacion de loguin
        if(estaConectado()){


            mAuthListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    if (user != null) {
                        // User is signed in

                        modelo.uid = user.getUid();
                        Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());

                        validarUsuario();

                    }

                }
            };

        }else{
            return;
            //Toast.makeText(getApplicationContext(),"No Tiene conexión a internet", Toast.LENGTH_LONG).show();
        }
    }


    private void validarUsuario() {
        comandoValidarUsuario.validarUsuario();
    }

    @Override
    public void validandoPasajeroOK() {

        ComandoPasajero.getPasajero(modelo.uid, new OnPasajeroChangeListener() {
            @Override
            public void cargoPasajero(Pasajero pasajero) {
                modelo.context = getApplicationContext();
                modelo.pasajero = pasajero;

                CmdEmpresa.getEmpresa(new OnEmpresaListener() {
                    @Override
                    public void cargoEmpresa() {
                        CmdOrden.getCurrentService();
                        Intent i = new Intent(MainActivity.this, NewHomeNav.class);
                        startActivity(i);
                        overridePendingTransition(R.anim.zoom_back_in, R.anim.zoom_back_out);
                        finish();

                    }
                });
            }


        });



    }

    @Override
    public void validandoPasajeroError() {
        progressDialog.dismiss();
        progressBar.setVisibility(View.GONE);
        button.setEnabled(true);
        //Toast.makeText(getApplicationContext(),"Usuario no registrado o esta en proceso de validación.",Toast.LENGTH_SHORT).show();
        showAlertDialog();
    }

    @Override
    public void pasajeroPendiente() {
        ComandoPasajero.getPasajero(modelo.uid, new OnPasajeroChangeListener() {
            @Override
            public void cargoPasajero(Pasajero pasajero) {
                modelo.context = getApplicationContext();
                modelo.pasajero = pasajero;

                CmdEmpresa.getEmpresa(new OnEmpresaListener() {
                    @Override
                    public void cargoEmpresa() {
                        CmdOrden.getCurrentService();
                        Intent i = new Intent(MainActivity.this, NewHomeNav.class);
                        startActivity(i);
                        overridePendingTransition(R.anim.zoom_back_in, R.anim.zoom_back_out);
                        finish();

                    }
                });
            }


        });
    }


    @Override
    public void onStart() {
        super.onStart();
       // mAuth.addAuthStateListener(mAuthListener);

        if(mAuth==null || mAuthListener ==null){
            return;
        }else{
            mAuth.addAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if(mAuth==null || mAuthListener ==null){
            return;
        }else{
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    public void login(View v){
        if (email.getText().toString().equals("") || password.getText().equals("")){
            AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this);
            dialogo1.setTitle("Campos Vacios");
            dialogo1.setMessage("Todos los camopos son obligatorios");
            dialogo1.setCancelable(false);
            dialogo1.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {

                }
            });
            dialogo1.show();
        }

        else{
            logueo();
        }
    }


    public void logueo(){
       if (estaConectado()){
           progressBar.setVisibility(View.VISIBLE);
           progressDialog.setMessage("Validando la información, por favor espere...");
           progressDialog.setCancelable(false);
           progressDialog.show();
           button.setEnabled(false);
           mAuth.signInWithEmailAndPassword(email.getText().toString(),password.getText().toString())
                   .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                       @Override
                       public void onComplete(@NonNull Task<AuthResult> task) {
                           //Log.d(TAG, "signInWithEmail:onComplete:"+task.isSuccessful());
                           FirebaseAuthException ex = (FirebaseAuthException) task.getException();
                           if (ex==null){
                               return;
                           }

                           String error = ex.getErrorCode();

                           if (error.equals("ERROR_INVALID_EMAIL")){
                               Log.d(TAG, "signInWithEmail:onComplete:"+"correo nada que ver");
                               Toast.makeText(getApplication(),"...."+"correo nada que ver", Toast.LENGTH_SHORT).show();
                               progressBar.setVisibility(View.GONE);
                               button.setEnabled(true);
                               progressDialog.dismiss();

                           }
                           if (error.equals("ERROR_USER_NOT_FOUND")){
                               Log.d(TAG, "signInWithEmail:onComplete:" + "USUARIO NUEVO");
                               Toast.makeText(getApplication(),"...."+"USUARIO NUEVO", Toast.LENGTH_SHORT).show();
                               progressBar.setVisibility(View.GONE);
                               button.setEnabled(true);
                               progressDialog.dismiss();
                           }
                           if (error.equals("ERROR_WRONG_PASSWORD")){
                               Log.d(TAG, "signInWithEmail:onComplete:" + "CONTRASEÑA INCORRECTA");
                               Toast.makeText(getApplication(),"...."+"CONTRASEÑA INCORRECTA", Toast.LENGTH_SHORT).show();
                               progressBar.setVisibility(View.GONE);
                               button.setEnabled(true);
                               progressDialog.dismiss();
                           }
                           if (!task.isSuccessful()){
                               Log.d(TAG, "signInWithEmail:onComplete:" + task.getException());
                               Toast.makeText(getApplication(),"...."+"FALLO EN LA AUTENTICACION", Toast.LENGTH_SHORT).show();
                               progressBar.setVisibility(View.GONE);
                               button.setEnabled(true);
                               progressDialog.dismiss();
                           }else{
                               Intent i = new Intent(MainActivity.this, NewHomeNav.class);
                               startActivity(i);
                               finish();
                               //startActivity(new Intent(MainActivity.this, Pgina_Principa.class));

                           }
                       }
                   });
       }else{
           Toast.makeText(getApplicationContext(),"No Tiene conexión a internet", Toast.LENGTH_LONG).show();
       }
    }



    protected Boolean estaConectado(){
        if(conectadoWifi()){
            Log.v("wifi","Tu Dispositivo tiene Conexion a Wifi.");
            return true;
        }else{
            if(conectadoRedMovil()){
                Log.v("Datos", "Tu Dispositivo tiene Conexion Movil.");
                return true;
            }else{
                showAlertDialog(MainActivity.this, "Conexion a Internet",
                        "Tu Dispositivo no tiene Conexion a Internet.", false);
                return false;
            }
        }
    }

    protected Boolean conectadoWifi(){
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    protected Boolean conectadoRedMovil(){
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        alertDialog.show();
    }

    public void olvidopassword(View v) {
        Intent i = new Intent(MainActivity.this, OlvidoSuPassword.class);
        startActivity(i);
        finish();
    }

    public void help (View v) {
        Intent i = new Intent(MainActivity.this, Help.class);
        startActivity(i);
        finish();
    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == event.KEYCODE_BACK) {
            Log.v("cerrar","cerrar");
        }
        return false;
    }

    public void registrarse(View v){

        if(estaConectado()){
            Intent i = new Intent(getApplicationContext(), RegistarUsuarios.class);
            startActivity(i);
            finish();

        }else{
            showAlertDialog(MainActivity.this, "Conexion a Internet",
                    "Tu Dispositivo no tiene Conexion a Internet.", false);
        }
    }




    //esto pendiente
    public void showAlertDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("¡Pasajero inactivo!");
        alertDialog.setMessage("Sus datos aún no han sido validaddos. Le notificaremos cuando su usuario " +
                "haya sido aprobado o rechazado según sea el caso.");
        alertDialog.setButton("OK, entendido", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        alertDialog.show();
    }


}
