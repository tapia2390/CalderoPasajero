package transcalderonpasajero.transcalderonpasajero.Adapter;

/**
 * Created by tactomotion on 19/10/16.
 */
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.R;

/**
 * Created by tactomotion on 4/09/16.
 */
public class HistorialAdapterDetallada extends BaseAdapter {

    //Atributos del adaptador
    private final Context mContext;
    AdapterCallback mListener = sDummyCallbacks;
    private List<Servicio> ordenes ;
    private Modelo sing = Modelo.getInstance();
    String fechaHistorco;


    public HistorialAdapterDetallada(Context mContext, AdapterCallback mListener,String fechaHistorco){

        this.mContext = mContext;
        this.mListener = mListener;
        this.fechaHistorco = fechaHistorco;
        this.ordenes  = sing.filtrohistorialmes;

    }

    @Override
    public int getCount() {
        return ordenes.size();
    }

    @Override
    public Object getItem(int position) {
        return ordenes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Declare Variables
        final RelativeLayout fondogris;
        final TextView textView17;
        final ImageView imagen_estado2;
        final TextView text_ciudad_origen;
        final TextView text_ciudad_llegada;
        final TextView texto_emprea;
        final TextView texto_fecha;



        final Servicio orden =  (Servicio) getItem(position);

        // Inflate la vista de la Orden
        LinearLayout itemLayout =  (LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.historialservicios, parent, false);

        // Toma los elementos a utilizar
        imagen_estado2 = (ImageView) itemLayout.findViewById(R.id.imagen_estado);
        text_ciudad_origen = (TextView) itemLayout.findViewById(R.id.text_ciudad_origen);
        text_ciudad_llegada = (TextView) itemLayout.findViewById(R.id.text_ciudad_llegada);
        texto_fecha = (TextView) itemLayout.findViewById(R.id.texto_fecha);
        texto_emprea = (TextView) itemLayout.findViewById(R.id.texto_emprea);
        textView17 = (TextView) itemLayout.findViewById(R.id.textView17);
        fondogris = (RelativeLayout) itemLayout.findViewById(R.id.fondogris);


        if (orden.getEstado().equals("Almacenado")){
            imagen_estado2.setImageResource(R.drawable.estado_finalizado_i5);
        }
        else{
            imagen_estado2.setImageResource(R.drawable.estado_finalizado_i5);
        }

        text_ciudad_origen.setText(orden.getOrigen());
        text_ciudad_llegada.setText(orden.getDestino());

        texto_fecha.setText(""+orden.getFechaEnDestino());
        //pasamos el dato fecha al metodo  formatearFecha y lo convertimos a formato fecha
        if (orden.conductor !=null){
            texto_emprea.setText(""+orden.conductor.nombre+" "+orden.conductor.apellido+" | "+orden.getMatricula());
        }

        Log.v("consola","consola"+ordenes.get(position).calificaciones.size());


        if(orden.contarMisCalificaciones() > 0 ){
            textView17.setText("");
            fondogris.setBackgroundResource(R.color.color_blanco);

        }
        else{
            textView17.setText("Pendiente por calificar");
            fondogris.setBackgroundResource(R.color.color_gris_linea);

        }


        return itemLayout;

    }



    /* Se define la interfaz del callback para poder informar a la actividad de alguna accion o dato.
   */
    public interface AdapterCallback
    {

    }

    /**
     * Para evitar nullpointerExceptions
     */
    private static AdapterCallback sDummyCallbacks = new AdapterCallback()
    {


    };

}
