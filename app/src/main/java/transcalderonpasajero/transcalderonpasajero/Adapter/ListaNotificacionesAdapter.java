package transcalderonpasajero.transcalderonpasajero.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import transcalderonpasajero.transcalderonpasajero.Clases.Mensajes;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.R;
import java.util.List;


/**
 * Created by tactomotion on 11/10/16.
 */
public class ListaNotificacionesAdapter extends BaseAdapter {

    //Atributos del adaptador
    private final Context mContext;
    AdapterCallback mListener = sDummyCallbacks;
    private List<Mensajes> notificaciones;
    private Modelo sing = Modelo.getInstance();


    //recivimos los datos de la clase informacionServicio
    public ListaNotificacionesAdapter(Context mContext, AdapterCallback mListener, String  idServicio){

        this.mContext = mContext;
        this.mListener = mListener;
        //asignamos los valores y se llama la clase principal modelo y llamamos el metodo get ordenes
        // que contiene las ordenes y le pasamos como parametro el idservicio para que nos muetre la
        //informacion del pasajero segun el idServicio
        //this.notificaciones = sing.getOrden(idServicio).notificaciones;
    }

    @Override
    public int getCount() {
        return notificaciones.size();
    }

    @Override
    public Object getItem(int position) {
        return notificaciones.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Declare Variables

        final TextView texto;
        final TextView hora;


        final Mensajes  mensaje =  (Mensajes) getItem(position);

        // Inflate la vista de la Orden
        LinearLayout itemLayout =  (LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.notificaciones, parent, false);

        // Toma los elementos a utilizar
        texto = (TextView) itemLayout.findViewById(R.id.texto);
        hora = (TextView) itemLayout.findViewById(R.id.hora);
        hora.setText(mensaje.getHora());
        texto.setText(mensaje.getTexto());

        sing.notificacacion = notificaciones.size();

        return itemLayout;

    }



    /* Se define la interfaz del callback para poder informar a la actividad de alguna accion o dato.
   */
    public interface AdapterCallback
    {

    }

    /**
     * Para evitar nullpointerExceptions
     */
    private static AdapterCallback sDummyCallbacks = new AdapterCallback()
    {


    };
}
