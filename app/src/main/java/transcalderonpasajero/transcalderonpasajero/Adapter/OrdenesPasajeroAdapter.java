package transcalderonpasajero.transcalderonpasajero.Adapter;

/**
 * Created by tactomotion on 30/09/16.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.Days;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import transcalderonpasajero.transcalderonpasajero.Clases.Conexion;
import transcalderonpasajero.transcalderonpasajero.Clases.Mensajes;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.R;

public class OrdenesPasajeroAdapter extends BaseAdapter {

    //Atributos del adaptador
    final Context mContext;
    AdapterCallback mListener = sDummyCallbacks;
    private List<Servicio> ordenes;
    private Modelo sing = Modelo.getInstance();
    Date date_1,date_2;
    private List<Mensajes> notificaciones;
    int contador =0;
    MediaPlayer mediaplayer;

    Conexion con;
    SQLiteDatabase miDB;
    int datoss=0;
    int idm;

    public OrdenesPasajeroAdapter(Context mContext, AdapterCallback mListener){

        this.mContext = mContext;
        this.mListener = mListener;
        this.ordenes = sing.getOrdenes();

    }

    @Override
    public int getCount() {
        return ordenes.size();
    }

    @Override
    public Object getItem(int position) {
        return ordenes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Declare Variables
        final ImageView imagen_estado2;
        final TextView text_ciudad_origen;
        final TextView text_ciudad_llegada;
        final TextView texto_fecha;
        final TextView texto_hora;
        final TextView texto_dias;
        final TextView texto_direccion;
        final TextView text_estado;
        final TextView cantida_msm;



        final Servicio orden =  (Servicio) getItem(position);


        // Inflate la vista de la Orden
        LinearLayout itemLayout =  (LinearLayout) LayoutInflater.from(mContext).inflate(R.layout.list_row, parent, false);

        // Toma los elementos a utilizar
        imagen_estado2 = (ImageView) itemLayout.findViewById(R.id.imagen_estado);
        text_ciudad_origen = (TextView) itemLayout.findViewById(R.id.text_ciudad_origen);
        text_ciudad_llegada = (TextView) itemLayout.findViewById(R.id.text_ciudad_llegada);
        texto_fecha = (TextView) itemLayout.findViewById(R.id.texto_fecha);
        texto_hora = (TextView) itemLayout.findViewById(R.id.texto_hora);
        texto_dias = (TextView) itemLayout.findViewById(R.id.texto_dias);
        texto_direccion = (TextView) itemLayout.findViewById(R.id.texto_direccion);
        text_estado = (TextView) itemLayout.findViewById(R.id.text_estado);
        cantida_msm = (TextView) itemLayout.findViewById(R.id.cantida_msm);


        if (orden.getEstado().equals("Asignado")){
            imagen_estado2.setImageResource(R.drawable.estado_confirmado_i5);
            text_estado.setText("Confirmado");
        }
        else if (orden.getEstado().equals("En Camino")){
            imagen_estado2.setImageResource(R.drawable.estado_en_camino_i5);
            text_estado.setText("En Camino");
        }
        else if (orden.getEstado().equals("Transportando")){
            imagen_estado2.setImageResource(R.drawable.estado_transportando_i5);
            text_estado.setText("Transportando");
        }
        else if (orden.getEstado().equals("Finalizado")){
            imagen_estado2.setImageResource(R.drawable.estado_finalizado_i5);
            text_estado.setText("Finalizado");

            Log.v("idOrden","idOrden"+orden.getId());

        }
        else if (orden.getEstado().equals("NoAsignado")){
            imagen_estado2.setImageResource(R.drawable.estado_sin_confirmar_i5);
            text_estado.setText("No Asignado");
        }
        else if (orden.getEstado().equals("Anulado")) {
            imagen_estado2.setVisibility(View.INVISIBLE);
            text_estado.setText("Anulado");
        }
        else{
            imagen_estado2.setImageResource(R.drawable.estado_cancelado_i5);
            text_estado.setText("Cancelado");
        }

        text_ciudad_origen.setText(orden.getOrigen());

        text_ciudad_llegada.setText(orden.getDestino());

        texto_fecha.setText(sing.dfsimple.format(orden.getFechaEnOrigen()));
        texto_hora.setText(orden.horaEnOrigen);
        //texto_dias.setText(orden.getTiempoRestante()+"");
        //texto_direccion.setText(orden.getDireccionDestino());


        //notificaciones = sing.getOrden(orden.getId()).notificaciones;

        Log.v("mensajes","mensajes"+notificaciones.size()+ "contador modelo"+ sing.notificacacion);

        if( notificaciones.size() <= sing.notificacacion ){
            cantida_msm.setText("");
            cantida_msm.setVisibility(View.INVISIBLE);

            Log.v("notifi","notifi"+ sing.notificacacion);
        }
        else{
            int res  = notificaciones.size()-sing.notificacacion;
            cantida_msm.setText(""+res);
            cantida_msm.setVisibility(View.VISIBLE);
            Log.v("mensajes","mensajes"+notificaciones.size()+ "contador modelo"+res);
        }

        //

        //notificaciones = sing.getOrden(orden.getId()).notificaciones;

        Log.v("mensajes","mensajes"+notificaciones.size()+ "contador modelo"+ sing.notificacacion);

        if( notificaciones.size() <= sing.notificacacion ){
            cantida_msm.setText("");
            cantida_msm.setVisibility(View.INVISIBLE);

            Log.v("notifi","notifi"+ sing.notificacacion);
        }
        else{
            int res  = notificaciones.size()-sing.notificacacion;
            cantida_msm.setText(""+res);
            cantida_msm.setVisibility(View.VISIBLE);
            Log.v("mensajes","mensajes"+notificaciones.size()+ "contador modelo"+res);

          //  Vibrator v = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
            //v.vibrate(1500);
            // mediaplayer = MediaPlayer.create(mContext, R.raw.notifi);
            //mediaplayer.start();

        }




        //pasamos el dato fecha al metodo  formatearFecha y lo convertimos a formato fecha

        /*
        if(sing.formatearFecha(orden.getFechaEnOrigen()).equals("Hoy") || sing.formatearFecha(orden.getFechaEnOrigen()).equals("Mañana") || sing.formatearFecha(orden.getFechaEnOrigen()).equals("Pasado Mañana")){

            texto_dias.setText(sing.formatearFecha(orden.getFechaEnOrigen()));
        }
        else {
//capturamos la fecha actual
            Calendar calendar = Calendar.getInstance();
            final int year1 = calendar.get(Calendar.YEAR);
            final int month1 = calendar.get(Calendar.MONTH)+1;
            int  day1 = calendar.get(Calendar.DAY_OF_MONTH);
            final String fecha_actual = day1+"/"+month1+"/"+year1;


            try {
                //convertimos a tipo fecha
                date_1 = sing.dfsimple.parse(fecha_actual);
                date_2 = orden.getFechaEnOrigen();

                //convertimos de date a DAtetime
                DateTime fechanow = new DateTime(date_1);
                DateTime fechaOrden = new DateTime(date_2);

                //capturamos la diferencia de dias
                Days.daysBetween(fechanow, fechaOrden).getDays();
                //System.out.println(date_1);
                //System.out.println(formatter.format(date_1));
                //validamos si el numero de dias es negativo

                System.out.println(date_2 + "..." + date_1 + "-----" + fecha_actual);
                Log.v("fechas", "dechas" + sing.dfsimple.format(date_2) + "..." + sing.dfsimple.format(date_1) + "-----" + fecha_actual);

                int f = fechanow.compareTo(fechaOrden);
                int f2 = fechaOrden.compareTo(fechanow);

                System.out.println("fechas" + "..." + "-----" + f + "_____....__" + f2);

                if (f2 < 0) {
                    texto_dias.setText(sing.dfsimple.format(date_2));
                } else if (Days.daysBetween(fechanow, fechaOrden).getDays() < 0) {
                    texto_dias.setText(sing.formatearFecha(orden.getFechaEnOrigen()));
                } else {
                    texto_dias.setText(Days.daysBetween(fechanow, fechaOrden).getDays() + " Dias");

            }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        */


        sing.catidadOrdenes =ordenes.size();

       String mensaje ="";
        String timestap="";

        if(orden.notificaciones.size() != 0){
            mensaje  = orden.notificaciones.get(orden.notificaciones.size()-1).getTexto();
            timestap = ""+orden.notificaciones.get(orden.notificaciones.size()-1).getTimestamp();
        }
           try{
               selectDatos(orden.getId(),orden.getEstado(), mensaje, timestap);

           }catch (Exception e){
               Log.v("exeptions","exeptions"+e);
           }

        return itemLayout;

    }




    public void abrirCone() {
        con = new Conexion(mContext);
        miDB = con.getWritableDatabase();
    }

    public void cerrarCone() {
        miDB.close();
    }



    public void selectDatos(String idOrden, String estado, String mensaje , String timestamp){


        abrirCone();
        String sql1 ="SELECT  count(id_notificacionO)  FROM Notification " +
                "WHERE id_notificacionO ="+"'"+idOrden+"'";
        Cursor mycur = miDB.rawQuery(sql1 , null);

        if (mycur.moveToFirst()) {

            do {
                idm =mycur.getInt(0);

                Log.v("cantms", "cantms "+idm);

            }
            while (mycur.moveToNext());

            if (idm>0){
                updateOrden(idOrden,estado,mensaje,timestamp);
                //Toast.makeText(mContext,"esta"+idOrden,Toast.LENGTH_LONG).show();
                Log.v("esta","esta");
            }else{
               // Toast.makeText(mContext,"no esta"+idOrden,Toast.LENGTH_LONG).show();
                insertOrden(idOrden,estado,mensaje,timestamp); //timestamp:
            }

        }
        cerrarCone();
    }

    public void insertOrden(String idOrden, String estado, String mensaje, String timestamp){
        abrirCone();
        long result;
        ContentValues valor = new ContentValues();
        valor.put("id_notificacionO", ""+idOrden);
        valor.put("estado",estado);
        valor.put("mensaje",mensaje);
        valor.put("timestamp",timestamp);
        result = miDB.insert("Notification", null, valor);
        if (result > 0) {
          //  Toast.makeText(mContext, "exito orden", Toast.LENGTH_SHORT).show();
            Log.v("orden", "orden"+"ok");
        }else{
           // Toast.makeText(mContext, "error", Toast.LENGTH_SHORT).show();
            Log.v("grave", "grave"+"grave");
        }
        cerrarCone();
    }


    public void updateOrden(String idOrden, String estado, String mensaje, String timestamp){
        abrirCone();
        long result;
        ContentValues valor = new ContentValues();
        valor.put("estado",estado);
        valor.put("mensaje",mensaje);
        valor.put("timestamp",timestamp);
        result = miDB.update("Notification", valor, "id_notificacionO= '" + idOrden+"'", null);
        if (result > 0) {
           // Toast.makeText(mContext, "U exito", Toast.LENGTH_SHORT).show();
            Log.v("orden", "orden"+"ok");
        }else{
          //  Toast.makeText(mContext, " Uerror", Toast.LENGTH_SHORT).show();
            Log.v("grave", "grave"+" U grave");
        }
        cerrarCone();
    }



    /* Se define la interfaz del callback para poder informar a la actividad de alguna accion o dato.
   */
    public interface AdapterCallback
    {

    }

    /**
     * Para evitar nullpointerExceptions
     */
    private static AdapterCallback sDummyCallbacks = new AdapterCallback()
    {


    };

}
