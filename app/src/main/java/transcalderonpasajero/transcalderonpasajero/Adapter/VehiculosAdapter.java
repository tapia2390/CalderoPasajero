package transcalderonpasajero.transcalderonpasajero.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.List;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Vehiculos;
import transcalderonpasajero.transcalderonpasajero.R;

/**
 * Created by tacto on 31/08/17.
 */

public class VehiculosAdapter extends BaseAdapter {

    private Modelo modelo = Modelo.getInstance();
    //Atributos del adaptador
    Context mContext;
    AdapterCallback mListener = sDummyCallbacks;
    private List<Vehiculos> vehiculos;


    public VehiculosAdapter(Context mContext, AdapterCallback mListener) {

        this.mContext = mContext;
        this.mListener = mListener;

        /*
        //ordenadon un array por la posicion del objrto
        Collections.sort(modelo.vehiculos, new Comparator<Vehiculos>() {
            @Override
            public int compare(Categorias o1, Categorias o2) {
                return new Long(o1.getPosicion()).compareTo(new Long(o2.getPosicion()));

            }
        });*/
        this.vehiculos = modelo.listavehiculos;

    }

    public int getCount() {
        return vehiculos.size();
    }

    public Object getItem(int position) {
        return vehiculos.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {

        View retval = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewitem, null);
        final ImageView accesorios_circulo = (ImageView) retval.findViewById(R.id.accesorios_circulo);
        final ImageView idcategoria = (ImageView) retval.findViewById(R.id.idcategoria);
        TextView text_categoria = (TextView) retval.findViewById(R.id.text_categoria);


        final String nombre1;
        nombre1 = vehiculos.get(position).getIdNombre().replaceAll("\\s", "").toLowerCase();
        ;


        //descargar imagenes firebase

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference().child("vehiculos/" + nombre1 + ".png");


        storageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                String imageURL = uri.toString();
                Glide.with(mContext).load(imageURL).into(accesorios_circulo);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });

        /*
        //get download file url
        storageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Log.i("Main", "File uri: " + uri.toString());
            }
        });


        //download the file
        try {
            final File localFile = File.createTempFile("images", "jpg");
            storageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                    if (modelo.empresa.exclusivoMujer == false) {
                        if (nombre1.equals("vehiculo0")) {
                            accesorios_circulo.setVisibility(View.GONE);
                        }
                    }
                    else {
                        accesorios_circulo.setImageBitmap(bitmap);
                    }


                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    int resID2 = mContext.getResources().getIdentifier(nombre1, "drawable", mContext.getPackageName()); // This will return an integer value stating the id for the image you want.

                    if (resID2 == 0) {
                        Toast.makeText(mContext, "Error al cargar la imagen", Toast.LENGTH_SHORT).show();
                    } else {

                        if (modelo.empresa.exclusivoMujer == false) {
                            if (nombre1.equals("vehiculo0")) {
                                accesorios_circulo.setVisibility(View.GONE);
                            }
                        }
                        else {
                            accesorios_circulo.setBackgroundResource(resID2);
                        }

                    }


                }
            });
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Main", "IOE Exception");
        }
*/

        text_categoria.setText(vehiculos.get(position).getNombre());


        if (modelo.selectPosition != -1) {
            if (modelo.selectPositionAnterior == position) {
                if (modelo.selectImge == true) {
                    //your drawable code
                    if (modelo.selectPosition == position) {
                        idcategoria.setVisibility(View.VISIBLE);
                    }
                    //your other stuff : changing color etc

                } else {
                    idcategoria.setVisibility(View.INVISIBLE);
                }
            }

        }


        return retval;
    }


    //create this method in Adapter class
    public void setSelected(int pos, boolean estado) {

        modelo.selectPosition = pos;//change selected item position
        modelo.selectImge = estado;
        notifyDataSetChanged();  //refresh views


    }


    /* Se define la interfaz del callback para poder informar a la actividad de alguna accion o dato.
 */
    public interface AdapterCallback {

    }

    /**
     * Para evitar nullpointerExceptions
     */
    private static AdapterCallback sDummyCallbacks = new AdapterCallback() {


    };

}