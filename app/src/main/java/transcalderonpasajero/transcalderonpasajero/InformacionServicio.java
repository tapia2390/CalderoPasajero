package transcalderonpasajero.transcalderonpasajero;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import transcalderonpasajero.transcalderonpasajero.Adapter.ListaNotificacionesAdapter;
import transcalderonpasajero.transcalderonpasajero.Clases.Conductor;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoConductor;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden;

public class InformacionServicio extends Activity implements ListaNotificacionesAdapter.AdapterCallback, Modelo.OnModeloChangelistener {

    String idServicio;
    TextView numero_orden;
    TextView text_ciudad_origen;
    ImageView imagen_estado;
    TextView text_ciudad_llegada;
    TextView barrio_recogida;
    TextView barrio_llegada;
    TextView fecha_y_hora_recogida;
    TextView datos_conductor;
    TextView datos_conductor_telefono;
    Modelo modelo  = Modelo.getInstance();
    Servicio servicio;
    ListView lvcomentario;
    Conductor conductor = new Conductor();
    ListaNotificacionesAdapter mAdapterNotifivicacion;
    final Context context = this;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_informacion_servicio);

        Bundle bundle = getIntent().getExtras();
        idServicio = bundle.getString("id");

        servicio = modelo.getOrdenById(idServicio);


        numero_orden = (TextView)findViewById(R.id.numero_orden);
        text_ciudad_origen = (TextView)findViewById(R.id.text_ciudad_origen);
        imagen_estado = (ImageView)findViewById(R.id.imagen_estado);
        text_ciudad_llegada = (TextView)findViewById(R.id.text_ciudad_llegada);
        barrio_recogida = (TextView)findViewById(R.id.barrio_recogida);
        barrio_llegada = (TextView)findViewById(R.id.barrio_llegada);
        fecha_y_hora_recogida = (TextView)findViewById(R.id.fecha_y_hora_recogida);
        datos_conductor = (TextView)findViewById(R.id.datos_conductor);
        datos_conductor_telefono = (TextView)findViewById(R.id.datos_conductor_telefono);
        lvcomentario = (ListView) findViewById(R.id.lvcomentario);
        actualizarPantalla();


    }


    private void displayListaNotificaciones(){
        //pasamos los datos del adaptador
        mAdapterNotifivicacion = new ListaNotificacionesAdapter(this,this,idServicio);
        lvcomentario.setAdapter(mAdapterNotifivicacion);

    }


    public void actualizarEstadoPantalla(){

        if (servicio.getEstado().equals("Asignado") || servicio.getEstado().equals("No Asignado")){
            imagen_estado.setImageResource(R.drawable.estado_confirmado_i5);
        }
        else if (servicio.getEstado().equals("En Camino")){
            imagen_estado.setImageResource(R.drawable.estado_en_camino_i5);
        }
        else if (servicio.getEstado().equals("Transportando")){
            imagen_estado.setImageResource(R.drawable.estado_transportando_i5);
        }
        else if (servicio.getEstado().equals("Finalizado")){
            imagen_estado.setImageResource(R.drawable.estado_finalizado_i5);

            showAlertCAlificacion();
        }
        else if (servicio.getEstado().equals("No Asignado")){
            imagen_estado.setImageResource(R.drawable.estado_sin_confirmar_i5);
        }
        else{
            imagen_estado.setImageResource(R.drawable.estado_sin_confirmar_i5);
        }
    }



    public void showAlertCAlificacion() {
        //   Toast.makeText(getApplicationContext(),"ingreso",Toast.LENGTH_SHORT).show();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("¡Estado del Servicio!");

        // set dialog message
        alertDialogBuilder
                .setMessage("Tu servicio ha finalizado puedes calificarlo ")
                .setCancelable(false)
                .setPositiveButton("Ok, Calificar Servicio", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //todo internet

                        Intent intent = new Intent(getApplicationContext(),InformacionDelServicioDetallada.class);
                        intent.putExtra("id",""+idServicio);
                        intent.putExtra("fecha","");
                        startActivity(intent);

                    }
                })
                .setNegativeButton("Ahora no", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                        dialog.cancel();
                    }
                });

        alertDialogBuilder.show();

        modelo.llamarServicios();
    }



    public void llamar(View v) {
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            //callIntent.setData(Uri.parse("tel:"+modelo.conductor.getCelular()));
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            startActivity(callIntent);
        }catch (Exception e){
            Log.v("no puede llamar","no puede llamar");

        }

    }





    @Override
    public void setActualizoListadoDeServicios() {
        actualizarPantalla();
    }

    @Override
    public void setActualizoListadoDeServiciosHistoricos() {

    }

    public void actualizarPantalla(){
        modelo.setModeloListener(this);
        servicio = modelo.getOrdenById(idServicio);

        text_ciudad_origen.setText(servicio.getOrigen());
        text_ciudad_llegada.setText(servicio.getDestino());
        numero_orden.setText("SERVICIO "+ servicio.getCosecutivoOrden());
        barrio_recogida.setText(servicio.getDireccionOrigen());
        barrio_llegada.setText(servicio.direccionDestino);
        fecha_y_hora_recogida.setText("Recoger: "+modelo.dfsimple.format(servicio.getFechaEnOrigen())+" "+ servicio.horaEnOrigen);
        Log.v("matricula","matricula"+ servicio.getMatricula());

        if (servicio.getMatricula() =="" || servicio.getMatricula().equals("")){
            datos_conductor.setText("Sin Asignar");
            datos_conductor_telefono.setText("Sin Asignar");
            datos_conductor_telefono.setBackgroundColor(Color.rgb(150,75,0));

        }else{
            //ComandoConductor  comandoConductor  = new ComandoConductor(this);
            //comandoConductor.getDatosConductor(modelo.getOrdenById(idServicio).idConductor);

        }

        displayListaNotificaciones();
        actualizarEstadoPantalla();


    }

    //guardar en una tabla o txt el  valor de la cantida de mensajes leidos el valor de notificacacion se borra al cerrar la app


    //ciclo de vida


    @Override
    protected void onStart() {
        super.onStart();
        modelo.appcerradaAbierta = false;
        super.onStart();
    }



    @Override
    protected void onStop() {
        super.onStop();
        Log.wtf("Ciclo de vida", "onStop");
        Start_Service();
    }

    /* mi Servicios */
    public void Start_Service(){
        modelo.appcerradaAbierta  = true;
        Intent intent = new Intent(getApplicationContext(), MiService.class);
        intent.putExtra("primerplano", false);
        startService(intent);
    }

    /* mi MiServiceForeground */
    private void MiServiceBoot() {

        Intent intent = new Intent(getApplicationContext(), ServiceBoot.class);
        intent.putExtra("primerplano", false);
        startService(intent);
    }

    public void Stop_Service(){
        modelo.appcerradaAbierta  = false;
        stopService(new Intent(getApplicationContext(), MiService.class));

        super.onStop();

    }





    //validacion conexion internet
    protected Boolean estaConectado(){
        if(conectadoWifi()){
            Log.v("wifi","Tu Dispositivo tiene Conexion a Wifi.");
            return true;
        }else{
            if(conectadoRedMovil()){
                Log.v("Datos", "Tu Dispositivo tiene Conexion Movil.");
                return true;
            }else{
                showAlertSinInternet();
                // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
    }

    protected Boolean conectadoWifi(){
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    protected Boolean conectadoRedMovil(){
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }




    public void showAlertSinInternet(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle("Sin Internet");

        // set dialog message
        alertDialogBuilder
                .setMessage("Sin Conexión a Internet")
                .setCancelable(false)
                .setPositiveButton("Reintentar",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                        Intent inte  = new Intent(getBaseContext(),Splash.class);
                        startActivity(inte);
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

}