package transcalderonpasajero.transcalderonpasajero;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;

public class NuevoServicio extends Activity {

    Modelo modelo = Modelo.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_nuevo_servicio);
    }




    //ciclo de vida


    @Override
    protected void onStart() {
        super.onStart();
        modelo.appcerradaAbierta = false;
    }




    @Override
    protected void onStop() {
        super.onStop();
        Log.wtf("Ciclo de vida", "onStop");
        Start_Service();
    }

    /* mi Servicios */
    public void Start_Service(){

        modelo.appcerradaAbierta  = true;
        Intent intent = new Intent(getApplicationContext(), MiService.class);
        intent.putExtra("primerplano", false);
        startService(intent);
    }

    /* mi MiServiceForeground */
    private void MiServiceBoot() {

        Intent intent = new Intent(getApplicationContext(), ServiceBoot.class);
        intent.putExtra("primerplano", false);
        startService(intent);
    }

    public void Stop_Service(){
        modelo.appcerradaAbierta  = false;
        stopService(new Intent(getApplicationContext(), MiService.class));
    }

}
