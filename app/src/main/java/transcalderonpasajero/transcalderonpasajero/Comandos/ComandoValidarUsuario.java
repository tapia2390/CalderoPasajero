package transcalderonpasajero.transcalderonpasajero.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;

/**
 * Created by tacto on 11/02/17.
 */

public class ComandoValidarUsuario {

    Modelo modelo;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

    //interface del listener de la actividad interesada
    private OnOValidarUsuarioChangeListener mListener;

    public ComandoValidarUsuario(OnOValidarUsuarioChangeListener mListener){

        this.mListener = mListener;

    }




    public void validarUsuario(){
        modelo = Modelo.getInstance();
        DatabaseReference ref = database.getReference("usuarios/"+modelo.uid);//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {

                boolean  data2 = snap.exists();

                if(data2 == true){

                    if(snap.child("rol").getValue().toString().equals("Pasajero") ){

                        if(snap.child("estado").getValue() != null){
                            String  estado = snap.child("estado").getValue().toString();
                            if(estado.equals("pendiente")){
                                mListener.pasajeroPendiente();

                            }else {
                                mListener.validandoPasajeroOK();
                            }
                        }else{
                            // si es un pasajero creado desde la empresa
                            mListener.validandoPasajeroOK();
                        }
                    }
                  else{
                        mListener.validandoPasajeroError();
                    }

                }else{
                    mListener.validandoPasajeroError();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    /**
     * Interfaz para avisar de eventos a los interesados
     */
    public interface OnOValidarUsuarioChangeListener {

        void validandoPasajeroOK();
        void validandoPasajeroError();
        void pasajeroPendiente();

    }

    /**
     * Para evitar nullpointerExeptions
     */
    private static OnOValidarUsuarioChangeListener sDummyCallbacks = new OnOValidarUsuarioChangeListener()
    {

        @Override
        public void validandoPasajeroOK()
        {}

        @Override
        public void validandoPasajeroError()
        {}

        @Override
        public void pasajeroPendiente() {

        }


    };
}
