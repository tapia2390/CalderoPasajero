package transcalderonpasajero.transcalderonpasajero.Comandos;

import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import transcalderonpasajero.transcalderonpasajero.Clases.Empresa;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Pasajero;
import transcalderonpasajero.transcalderonpasajero.Clases.Vehiculos;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoPasajero.OnPasajeroChangeListener;

/**
 * Created by tacto on 7/10/17.
 */

public class CmdEmpresa {



    public static void listadoClienteEmpresa(){
        final Modelo modelo = Modelo.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("clientes/");//ruta path
        Query query = ref.orderByChild("estado").equalTo("aprobado");

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                boolean data2 = snap.exists();

                if (data2 == false) {

                } else {
                    modelo.clientesEmpresases.clear();

                    for (DataSnapshot lista : snap.getChildren()){
                        Empresa empresa = new Empresa();
                        empresa.setId(lista.getKey());
                        empresa.setActivo(true);
                        empresa.setCelular(lista.child("celular").getValue().toString());
                        empresa.setConsecutivoOrden(lista.child("consecutivoOrden").getValue().toString());
                        empresa.setCorreo(lista.child("correo").getValue().toString());
                        empresa.setDireccion(lista.child("direccion").getValue().toString());
                        empresa.setInicialesConsecutivo(lista.child("inicialesConsecutivo").getValue().toString());
                        empresa.setNit(lista.child("nit").getValue().toString());
                        empresa.setNombreContacto(lista.child("nombreContacto").getValue().toString());
                        empresa.setRazonSocial(lista.child("razonSocial").getValue().toString());


                        //nuevo arbol snapVehiculos

                        if(snap.child("tipoVehiculo").exists()){
                            DataSnapshot snapVehiculos;
                            snapVehiculos = (DataSnapshot) snap.child("tipoVehiculo");//ubicacion
                            empresa.tiposVehiculo.clear();
                            for (DataSnapshot gps : snapVehiculos.getChildren()) {
                                Vehiculos newVehiculos = new Vehiculos();
                                newVehiculos.setIdNombre(gps.getKey());
                                empresa.tiposVehiculo.add(0, newVehiculos);

                            }
                        }



                        modelo.clientesEmpresases.add(empresa);
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public interface OnEmpresaListener {

        void cargoEmpresa();

    }

    public static void getEmpresa(final OnEmpresaListener listener){

        final Modelo modelo = Modelo.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("clientes/"+ modelo.pasajero.idEmpresa);


        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snap) {
                final Empresa empresa = new Empresa();
                empresa.setId(snap.getKey());
                empresa.setActivo(true);
                empresa.setCorreo(snap.child("correo").getValue().toString());
                empresa.setDireccion(snap.child("direccion").getValue().toString());
                empresa.setInicialesConsecutivo(snap.child("inicialesConsecutivo").getValue().toString());
                empresa.setNit(snap.child("nit").getValue().toString());
                empresa.setNombreContacto(snap.child("nombreContacto").getValue().toString());
                empresa.setRazonSocial(snap.child("razonSocial").getValue().toString());
                if (snap.child("exclusivoMujer").exists()){
                    empresa.exclusivoMujer = (boolean)snap.child("exclusivoMujer").getValue();
                }

                if (snap.child("horasRespuesta").exists()){
                    empresa.horasRespuesta = ((Long) snap.child("horasRespuesta").getValue()).intValue();
                }

                if (snap.child("permitirAutoregistro").exists()){
                    empresa.permitirAutoRegistro =  (boolean)snap.child("permitirAutoregistro").getValue();

                }



                if (snap.hasChild("usuarios")){

                    for (DataSnapshot usuario : snap.child("usuarios").getChildren()){

                        ComandoPasajero.getPasajero(usuario.getKey(), new OnPasajeroChangeListener() {
                            @Override
                            public void cargoPasajero(Pasajero pasajero) {
                                empresa.empresariales.add(pasajero);
                            }


                        });
                    }
                }

                modelo.empresa = empresa;

                listener.cargoEmpresa();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }



}