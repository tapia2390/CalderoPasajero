package transcalderonpasajero.transcalderonpasajero.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;

/**
 * Created by tacto on 6/07/17.
 */

public class ComandoCompartirUbicacion {


    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();


    //interface del listener de la actividad interesada
    private ComandoCompartirUbicacion.OnCompartirUbicacionChangeListener mListener;

    /**
     * Interfaz para avisar de eventos a los interesados
     */
    public interface OnCompartirUbicacionChangeListener {

        void cargoUbicacion();
        void actualizoUbicacion();

    }

    public ComandoCompartirUbicacion(ComandoCompartirUbicacion.OnCompartirUbicacionChangeListener mListener){

        this.mListener = mListener;

    }



    public  void enviarUbicacion(final double latitud, final double  longitud, final String idServicio,final String uid){

        DatabaseReference key = referencia.push();

        final DatabaseReference ref = database.getReference("ordenes/pendientes/"+idServicio+"/ubicacion/"+key.getKey()+"/");//ruta path

        ref.addListenerForSingleValueEvent(new ValueEventListener() {//addListenerForSingleValueEvent no queda escuchando peticiones
            @Override
            public void onDataChange(DataSnapshot snap) {

                String id ="";
                if(modelo.uid.equals("")){
                    id =uid;
                }else{
                    id =modelo.uid;
                }
                Map<String, Object> enviarUbicacion = new HashMap<String, Object>();
                enviarUbicacion.put("lat", latitud);
                enviarUbicacion.put("lon", longitud);
                enviarUbicacion.put("pasajero", ""+ id);
                enviarUbicacion.put("timestamp", ServerValue.TIMESTAMP);

                ref.setValue(enviarUbicacion);
                mListener.cargoUbicacion();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void  actualizarUbicacion(final double latitud, final double  longitud, String idServicio,String  keyUbicacion){

        DatabaseReference ref = database.getReference("ordenes/pendientes/"+idServicio+"/ubicacion/"+keyUbicacion+"/lat");//ruta path
        ref.setValue(latitud);
        DatabaseReference ref2 = database.getReference("ordenes/pendientes/"+idServicio+"/ubicacion/"+keyUbicacion+"/lon");//ruta path
        ref2.setValue(longitud);
        mListener.actualizoUbicacion();


    }


    /**
     * Para evitar nullpointerExeptions
     */
    private static ComandoCompartirUbicacion.OnCompartirUbicacionChangeListener sDummyCallbacks = new ComandoCompartirUbicacion.OnCompartirUbicacionChangeListener()
    {
        @Override
        public void cargoUbicacion()
        {}


        @Override
        public void actualizoUbicacion()
        {}


    };

}
