package transcalderonpasajero.transcalderonpasajero.Comandos;

import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;


import java.util.ArrayList;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;


/**
 * Created by andres on 11/14/17.
 */

public class CmdNoti {


    Modelo model = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();

    private ArrayList<OnCmdNotiListener> listeners = new ArrayList<>();

    private static final CmdNoti ourInstance = new CmdNoti();

    public static CmdNoti getInstance() {
        return ourInstance;

    }


    public interface OnCmdNotiListener {

        void cargoNotificacion(String idServicio, String estado);

    }


    public void registrarListener (OnCmdNotiListener listener) {
        listeners.add(listener);
    }


    public void llegoNotificacion(String idServicio, String estado){
        for (OnCmdNotiListener listener:listeners){
            Servicio servicio = model.getOrdenById(idServicio);
            if (servicio.ultimoEstado.equals(estado)){
                return;
            }
            servicio.ultimoEstado = servicio.estado;
            listener.cargoNotificacion(idServicio, estado);

        }

    }

    private CmdNoti() {


    }


    public void escucharCambios(final String idServico){

        DatabaseReference ref = database.getReference("ordenes/pendientes/" + idServico + "/estado");

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snap) {

                    llegoNotificacion(idServico, snap.getValue().toString());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError snap) {

            }
        });


    }


    public static void actualizarTokenDevice(String tokenDevice){

        Modelo model = Modelo.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        if ((tokenDevice == null) || tokenDevice.equals("") ){
            return;
        }

        DatabaseReference ref = database.getReference("usuarios/"+model.uid + "/tokenDevice");
        ref.setValue(tokenDevice);

    }

    public void clear(){
        listeners.clear();
    }





}
