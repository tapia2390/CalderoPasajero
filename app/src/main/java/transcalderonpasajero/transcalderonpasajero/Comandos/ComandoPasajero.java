package transcalderonpasajero.transcalderonpasajero.Comandos;

/**
 * Created by tactomotion on 28/09/16.
 */
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import transcalderonpasajero.transcalderonpasajero.Clases.Empresa;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Pasajero;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.sistema.Utility;

public class ComandoPasajero {


    private FirebaseAuth mAuth = FirebaseAuth.getInstance();




    public  ComandoPasajero(){

    }


    public interface OnPasajeroChangeListener {

        void cargoPasajero(Pasajero pasajero);

    }

    public static void getPasajero(final String id, final OnPasajeroChangeListener mListener){
        final Modelo modelo = Modelo.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();


        DatabaseReference ref = database.getReference("usuarios/" + id);//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {//addListenerForSingleValueEvent no queda escuchando peticiones
            @Override
            public void onDataChange(DataSnapshot snap) {
                Pasajero pasajero = new Pasajero();

                pasajero.setNombre(snap.child("nombre").getValue().toString());
                pasajero.setApellido(snap.child("apellido").getValue().toString());
                pasajero.setCelular(snap.child("celular").getValue().toString());
                pasajero.setCorreo(snap.child("correo").getValue().toString());
                pasajero.estado =  snap.child("estado").getValue().toString();


                if  (snap.hasChild("categoria")) {
                    pasajero.categoria = snap.child("categoria").getValue().toString();
                }

                pasajero.idEmpresa = snap.child("idCliente").getValue().toString(); // Si esta bien el clinet es la empresa
                pasajero.setIdCliente(id);
                pasajero.uid = id;

                if(snap.child("foto").getValue() == null || snap.child("foto").getValue().toString().equals("")){
                    pasajero.setFoto("");
                }else{
                    pasajero.setFoto(snap.child("foto").getValue().toString());
                }

                if (snap.hasChild("misPasajeros")  ){

                    for (DataSnapshot amigo : snap.child("misPasajeros").getChildren()){
                        Pasajero pasa = new Pasajero();
                        pasa.uid = amigo.getKey();
                        pasa.setNombre(amigo.child("nombre").getValue().toString());
                        pasa.setApellido(amigo.child("apellido").getValue().toString());
                        pasa.setCelular(amigo.child("celular").getValue().toString());
                        pasa.estado = amigo.child("estado").getValue().toString();
                        pasajero.amigos.add(pasa);
                    }
                }
                mListener.cargoPasajero(pasajero);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public interface OnCambioEstado {

        void aprobado();
        void pendiente();

    }


    public static void escucharEstado(final OnCambioEstado listener){
        final Modelo modelo = Modelo.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("usuarios/" + modelo.uid + "/estado");

        if (modelo.uid.equals("")){
            return;
        }

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                modelo.pasajero.estado = snap.getValue().toString();
                if (snap.getValue().toString().equals("aprobado")){
                        listener.aprobado();
                }else {
                    listener.pendiente();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    public static void crearAmigo(Pasajero pasajero){

        final Modelo modelo = Modelo.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("usuarios/"+ modelo.uid + "/misPasajeros/" + pasajero.uid);


        Map<String, Object> dicc = new HashMap<String, Object>();
        dicc.put("nombre",  pasajero.nombre);
        dicc.put("apellido", pasajero.getApellido());
        dicc.put("estado", "activo");
        dicc.put("celular", pasajero.getCelular());

        ref.setValue(dicc);



    }

    public static String getNewFirebaseId(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        return database.getReference("usuarios/").push().getKey();

    }








}
