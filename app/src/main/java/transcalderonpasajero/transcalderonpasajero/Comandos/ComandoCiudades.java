package transcalderonpasajero.transcalderonpasajero.Comandos;

/**
 * Created by tactomotion on 28/09/16.
 */

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import transcalderonpasajero.transcalderonpasajero.Clases.Ciudad;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Vehiculos;


public class ComandoCiudades {


    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();


    //interface del listener de la actividad interesada
    private OnCiudadesChangeListener mListener;

    public ComandoCiudades(OnCiudadesChangeListener mListener) {

        this.mListener = mListener;

    }


    public void getCiudades() {


        DatabaseReference ref = database.getReference("listados/colombia/");//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {//addListenerForSingleValueEvent no queda escuchando peticiones
            @Override
            public void onDataChange(DataSnapshot snap) {


                for (DataSnapshot depar : snap.getChildren()) {
                    String departamento = depar.child("departamento").getValue().toString();
                    DataSnapshot snapCiudades = depar.child("ciudades");

                    for (DataSnapshot ciu : snapCiudades.getChildren()) {
                        Ciudad ciudad = new Ciudad();
                        ciudad.nombre = ciu.getKey();
                        ciudad.departamento = departamento;
                        modelo.ciudades.add(ciudad);
                    }

                }
                mListener.cargoCiudades();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void getVehiculos() {


        DatabaseReference ref = database.getReference("listados/tiposVehiculo/");//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {//addListenerForSingleValueEvent no queda escuchando peticiones
            @Override
            public void onDataChange(DataSnapshot snap) {


                String snapi = snap.getKey().toString();

                for (DataSnapshot veho2 : snap.getChildren()) {
                    String snapi2 =snap.getChildren().toString();
                    Vehiculos vehiculos = new Vehiculos();
                    vehiculos.setIdNombre(veho2.getKey());
                    vehiculos.setNombre(veho2.getValue().toString());
                    modelo.listavehiculos.add(vehiculos);

                }




                mListener.cargoVehiculos();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    /**
     * Interfaz para avisar de eventos a los interesados
     */
    public interface OnCiudadesChangeListener {

        void cargoCiudades();

        void cargoVehiculos();

    }

    /**
     * Para evitar nullpointerExeptions
     */
    private static OnCiudadesChangeListener sDummyCallbacks = new OnCiudadesChangeListener() {
        @Override
        public void cargoCiudades() {
        }

        @Override
        public void cargoVehiculos() {
        }


    };



}
