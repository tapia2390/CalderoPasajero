package transcalderonpasajero.transcalderonpasajero.Comandos;

/**
 * Created by tactomotion on 30/09/16.
 */

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;

import transcalderonpasajero.transcalderonpasajero.Clases.Calificacion;
import transcalderonpasajero.transcalderonpasajero.Clases.Conductor;
import transcalderonpasajero.transcalderonpasajero.Clases.Mensajes;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.Clases.UbicacionPasajero;


public class ComandoOrdenesPasajeroUltimosDiez {
    Modelo modelo;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");



    //interface del listener de la actividad interesada
    private OnOrdenesPasajeroHistorialChangeListener mListener;

    public ComandoOrdenesPasajeroUltimosDiez(OnOrdenesPasajeroHistorialChangeListener mListener){

        this.mListener = mListener;

    }

    public ComandoOrdenesPasajeroUltimosDiez(){
        modelo.getHistorial();

    }


    public void getTodasLasOrdenesPasajeroHistorial(){
        modelo = Modelo.getInstance();
        modelo.getHistorial().clear();
        DatabaseReference ref = database.getReference("ordenes/historial/");//ruta path
        Query query = ref.orderByChild("pasajeros/"+modelo.uid).equalTo("empresarial");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {

                for (DataSnapshot ordenHistorial : snap.getChildren()){

                    Servicio nuevaOrden = new Servicio();
                    nuevaOrden.setOrigen(ordenHistorial.child("origen").getValue().toString());
                    if(ordenHistorial.child("estado").getValue().toString().equals("Finalizado")){
                        modelo.idUltimoServicioFinalizado =ordenHistorial.getKey();
                    }
                    nuevaOrden.setEstado(ordenHistorial.child("estado").getValue().toString());
                    nuevaOrden.setDestino(ordenHistorial.child("destino").getValue().toString());
                    nuevaOrden.setHora(ordenHistorial.child("horaEnOrigen").getValue().toString());
                    nuevaOrden.setDireccionDestino(ordenHistorial.child("direccionDestino").getValue().toString());
                    nuevaOrden.setFechaEnOrigen(ordenHistorial.child("fechaEnOrigen").getValue().toString());

                    // nuevaOrden.setTiempoRestante(ordenHistorial.child("fechaGeneracion").getValue().toString());

                    if(snap.child("asignadoPor").getValue() != null){
                        nuevaOrden.setAsignadoPor(snap.child("asignadoPor").getValue().toString());
                    }

                    if(snap.child("conductor").getValue() != null){
                         nuevaOrden.setIdConductor(snap.child("conductor").getValue().toString());
                    }

                    if(ordenHistorial.hasChild("cosecutivoOrden")){
                        nuevaOrden.setCosecutivoOrden(ordenHistorial.child("cosecutivoOrden").getValue().toString());
                    }else{
                        if(ordenHistorial.hasChild("consecutivoOrden")){
                            nuevaOrden.setCosecutivoOrden(ordenHistorial.child("consecutivoOrden").getValue().toString());
                        }else{
                            nuevaOrden.setCosecutivoOrden("");
                        }
                    }
                    nuevaOrden.setDireccionOrigen(ordenHistorial.child("direccionOrigen").getValue().toString());

                    if(ordenHistorial.hasChild("fechaEnDestino")){
                        nuevaOrden.setFechaEnDestino(ordenHistorial.child("fechaEnDestino").getValue().toString());
                    }

                    if(ordenHistorial.hasChild("horaEnDestino")){
                        nuevaOrden.setHoraEnDestino(ordenHistorial.child("horaEnDestino").getValue().toString());
                    }

                    nuevaOrden.setHoraGeneracion(ordenHistorial.child("horaGeneracion").getValue().toString());
                    nuevaOrden.setIdCliente(ordenHistorial.child("idCliente").getValue().toString());

                    if(snap.child("matricula").getValue() != null){
                        nuevaOrden.setMatricula(snap.child("matricula").getValue().toString());
                    }
                    nuevaOrden.ruta = ordenHistorial.child("ruta").getValue().toString();
                    nuevaOrden.setSolicitadoPor(ordenHistorial.child("solicitadoPor").getValue().toString());
                    if(snap.child("tarifa").getValue() != null){
                        nuevaOrden.setTarifa(snap.child("tarifa").getValue().toString());
                    }

                    Long l  = (Long)ordenHistorial.child("timestamp").getValue();
                    nuevaOrden.setTimeStamp(l);
                    // Long longTimestamp = Long.valueOf(nuevaOrden.setTimeStamp(snap.child("timestamp").getValue().toString()));
                    // nuevaOrden.setTrayectos(snap.child("trayectos").getValue().toString());
                    nuevaOrden.setId(ordenHistorial.getKey());

                    //nuevo arbol


                    //nuevo arbol mensajes
                    DataSnapshot snapNotificaones;
                    snapNotificaones = (DataSnapshot) ordenHistorial.child("mensajes");//mensajes
                    for (DataSnapshot notificaciones : snapNotificaones.getChildren()){
                        Mensajes newMensaje = new Mensajes();
                        newMensaje.setId(notificaciones.getKey());
                        newMensaje.setHora(notificaciones.child("hora").getValue().toString());
                        newMensaje.setTexto(notificaciones.child("texto").getValue().toString());
                        nuevaOrden.notificaciones.add(0,newMensaje);//  TODO: validacion de evitar duplicados

                    }

                    //nuevo arbol calificacion
                    DataSnapshot snapCalificaciones;
                    snapCalificaciones = (DataSnapshot) snap.child("calificacion");//calificaciones
                    for (DataSnapshot calificacion : snapCalificaciones.getChildren()){
                        Calificacion newCalificacion = new Calificacion();
                        newCalificacion.setId(calificacion.getKey());
                        newCalificacion.setObervacion(calificacion.child("observacion").getValue().toString());
                        newCalificacion.setValor(calificacion.child("valor").getValue().toString());
                        nuevaOrden.calificaciones.add(newCalificacion);//  TODO: validacion de evitar duplicados
                    }




                        modelo.adicionarNuevaOrdenHistorial(nuevaOrden);
                         getDatosConductorHistorial(nuevaOrden.getIdConductor(),nuevaOrden.getId());



                }

                mListener.cargoUnaOrdenesPasajeroHistorial();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getOrdenesPasajeroHistorial(){

        modelo = Modelo.getInstance();
        modelo.getOrdenes().clear();
        DatabaseReference ref = database.getReference("ordenes/historial/");//ruta path
        //se crea un query filtrado por el id del Pasajero
        Query query = ref.orderByChild("pasajeros/"+modelo.uid).equalTo("empresarial");
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snap, String s) {

                Servicio nuevaOrden = new Servicio();
                nuevaOrden.setOrigen(snap.child("origen").getValue().toString());
                nuevaOrden.setEstado(snap.child("estado").getValue().toString());
                nuevaOrden.setDestino(snap.child("destino").getValue().toString());
                 nuevaOrden.setHora(snap.child("horaEnOrigen").getValue().toString());
                 nuevaOrden.setDireccionDestino(snap.child("direccionDestino").getValue().toString());
                nuevaOrden.setFechaEnOrigen(snap.child("fechaEnOrigen").getValue().toString());

                //nuevaOrden.setTiempoRestante(snap.child("fechaGeneracion").getValue().toString());


                if(snap.child("asignadoPor").getValue() != null){
                    nuevaOrden.setAsignadoPor(snap.child("asignadoPor").getValue().toString());
                }


                if(snap.child("conductor").getValue() != null){
                     nuevaOrden.setIdConductor(snap.child("conductor").getValue().toString());
                }

                if(snap.hasChild("cosecutivoOrden")){
                    nuevaOrden.setCosecutivoOrden(snap.child("cosecutivoOrden").getValue().toString());
                }else{
                    if(snap.hasChild("consecutivoOrden")){
                        nuevaOrden.setCosecutivoOrden(snap.child("consecutivoOrden").getValue().toString());
                    }else{
                        nuevaOrden.setCosecutivoOrden("");
                    }
                }
                nuevaOrden.setDireccionOrigen(snap.child("direccionOrigen").getValue().toString());

                if(snap.hasChild("fechaEnDestino")){
                    nuevaOrden.setFechaEnDestino(snap.child("fechaEnDestino").getValue().toString());
                }

                if(snap.hasChild("horaEnDestino")){
                    nuevaOrden.setHoraEnDestino(snap.child("horaEnDestino").getValue().toString());
                }


                nuevaOrden.setHoraGeneracion(snap.child("horaGeneracion").getValue().toString());
                nuevaOrden.setIdCliente(snap.child("idCliente").getValue().toString());

                if(snap.child("matricula").getValue() != null){
                    nuevaOrden.setMatricula(snap.child("matricula").getValue().toString());
                }

                nuevaOrden.ruta = snap.child("ruta").getValue().toString();
                nuevaOrden.setSolicitadoPor(snap.child("solicitadoPor").getValue().toString());

                if(snap.child("tarifa").getValue() != null){
                    nuevaOrden.setTarifa(snap.child("tarifa").getValue().toString());
                }

                Long l  = (Long)snap.child("timestamp").getValue();
                nuevaOrden.setTimeStamp(l);
                // Long longTimestamp = Long.valueOf(nuevaOrden.setTimeStamp(snap.child("timestamp").getValue().toString()));
                // nuevaOrden.setTrayectos(snap.child("trayectos").getValue().toString());
                nuevaOrden.setId(snap.getKey());

                //nuevo arbol

                //nuevo arbol mensajes
                DataSnapshot snapNotificaones;
                snapNotificaones = (DataSnapshot) snap.child("mensajes");//mensajes

                for (DataSnapshot notificaciones : snapNotificaones.getChildren()){
                    Mensajes newMensaje = new Mensajes();
                    newMensaje.setId(notificaciones.getKey());
                    newMensaje.setHora(notificaciones.child("hora").getValue().toString());
                    newMensaje.setTexto(notificaciones.child("texto").getValue().toString());
                    nuevaOrden.notificaciones.add(0,newMensaje);//  TODO: validacion de evitar duplicados

                }


                //nuevo arbol calificacion
                DataSnapshot snapCalificaciones;
                snapCalificaciones = (DataSnapshot) snap.child("calificacion");//calificaciones
                for (DataSnapshot calificacion : snapCalificaciones.getChildren()){
                    Calificacion newCalificacion = new Calificacion();

                    newCalificacion.setId(calificacion.getKey());
                    newCalificacion.setObervacion(calificacion.child("observacion").getValue().toString());
                    newCalificacion.setValor(calificacion.child("valor").getValue().toString());
                    newCalificacion.setPasajero(calificacion.child("pasajero").getValue().toString());
                    Long timestamp  = (Long)snap.child("timestamp").getValue();
                    newCalificacion.setTimestamp(timestamp);


                    nuevaOrden.calificaciones.add(newCalificacion);//  TODO: validacion de evitar duplicados
                }



                if(snap.child("estado").getValue().toString().equals("Finalizado")){
                    modelo.adicionarNuevaOrdenHistorial(nuevaOrden);
                    getDatosConductorHistorial(nuevaOrden.getIdConductor(),nuevaOrden.getId());
                }
                mListener.cargoUnaOrdenesPasajeroHistorial();

            }

            @Override
            public void onChildChanged(DataSnapshot snap, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot snap) {

            }

            @Override
            public void onChildMoved(DataSnapshot snap, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    public void getDatosConductorHistorial(final String idConductor, final String idOrden){
        modelo = Modelo.getInstance();

        DatabaseReference ref = database.getReference("empresa/conductoresTerceros/"+idConductor);//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {

                if(snap.exists()){
                    boolean data2 = snap.exists();

                    if (data2 == false) {
                        getDatosConductorHistorialNormal(idConductor,idOrden);
                    } else {
                        Conductor conductor = new Conductor();

                        conductor.nombre =  snap.child("nombre").getValue().toString();
                        conductor.apellido = snap.child("apellido").getValue().toString();
                        conductor.celular = snap.child("celular").getValue().toString();

                        modelo.getOrdenHistorialNuevo(idOrden).conductor = conductor;
                    }
                }


                mListener.cargoUnaOrdenesPasajeroHistorial();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    public void getDatosConductorHistorialNormal(String idConductor, final String idOrden){
        modelo = Modelo.getInstance();

        DatabaseReference ref = database.getReference("empresa/conductores/"+idConductor);//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                Conductor conductor = new Conductor();

                conductor.nombre = snap.child("nombre").getValue().toString();
                conductor.apellido = snap.child("apellido").getValue().toString();
                conductor.celular = snap.child("celular").getValue().toString();

                modelo.getOrdenHistorialNuevo(idOrden).conductor = conductor;


                mListener.cargoUnaOrdenesPasajeroHistorial();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }



    /**
     * Interfaz para avisar de eventos a los interesados
     */
    public interface OnOrdenesPasajeroHistorialChangeListener {

        void cargoUnaOrdenesPasajeroHistorial();
        void cargoHisTorialP();

    }

    /**
     * Para evitar nullpointerExeptions
     */
    private static OnOrdenesPasajeroHistorialChangeListener sDummyCallbacks = new OnOrdenesPasajeroHistorialChangeListener()
    {
        @Override
        public void cargoUnaOrdenesPasajeroHistorial()
        {}

        @Override
        public void cargoHisTorialP()
        {}



    };

}

