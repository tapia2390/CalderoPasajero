package transcalderonpasajero.transcalderonpasajero.Comandos;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Params;


/**
 * Created by andres on 3/24/18.
 */

public class CmdParams {




    public static  void getParams(){

        final Modelo modelo = Modelo.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();


        DatabaseReference ref = database.getReference("params");//ruta path
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                modelo.params =  snap.getValue(Params.class);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });




    }



}
