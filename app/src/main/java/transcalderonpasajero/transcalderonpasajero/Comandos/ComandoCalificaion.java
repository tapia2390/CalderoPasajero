package transcalderonpasajero.transcalderonpasajero.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import transcalderonpasajero.transcalderonpasajero.Clases.Conductor;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;

/**
 * Created by tactomotion on 20/10/16.
 */
public class ComandoCalificaion {

    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();


    //interface del listener de la actividad interesada
    private OnCalificaionChangeListener mListener;

    /**
     * Interfaz para avisar de eventos a los interesados
     */
    public interface OnCalificaionChangeListener {

        void cargoCalificaion();

    }

    public ComandoCalificaion(OnCalificaionChangeListener mListener){

        this.mListener = mListener;

    }

    public  ComandoCalificaion(){

    }



    public  void enviarCalificaion(String idServicio, final String motivoMensaje, final Float calificaion){

        DatabaseReference key = referencia.push();

        final DatabaseReference ref = database.getReference("ordenes/historial/"+idServicio+"/calificacion/"+key.getKey()+"/");//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {//addListenerForSingleValueEvent no queda escuchando peticiones
            @Override
            public void onDataChange(DataSnapshot snap) {

                //ref.setValue(motivoMensaje);
                //mListener.cargoCancelacion();

                Date date = new Date();
                //Caso 1: obtener la hora y salida por pantalla con formato:
                DateFormat hourFormat = new SimpleDateFormat("hh:mm a");
                System.out.println("Hora: "+hourFormat.format(date));
//Caso 2: obtener la fecha y salida por pantalla con formato:
                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                System.out.println("Fecha: "+dateFormat.format(date));
//Caso 3: obtenerhora y fecha y salida por pantalla con formato:
                DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
                System.out.println("Hora y fecha: "+hourdateFormat.format(date));

                Map<String, Object> enviarMensaje = new HashMap<String, Object>();
                enviarMensaje.put("fecha", dateFormat.format(date));
                enviarMensaje.put("observacion", ""+motivoMensaje);
                enviarMensaje.put("pasajero", ""+modelo.uid);
                enviarMensaje.put("timestamp", ServerValue.TIMESTAMP);
                enviarMensaje.put("valor", calificaion);
                //ref.updateChildren(updateConductor);
                ref.setValue(enviarMensaje);
mListener.cargoCalificaion();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    /**
     * Para evitar nullpointerExeptions
     */
    private static OnCalificaionChangeListener sDummyCallbacks = new OnCalificaionChangeListener()
    {
        @Override
        public void cargoCalificaion()
        {}



    };
}
