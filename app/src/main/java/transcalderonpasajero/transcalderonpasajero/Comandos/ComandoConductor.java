package transcalderonpasajero.transcalderonpasajero.Comandos;

import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import transcalderonpasajero.transcalderonpasajero.Clases.Conductor;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
/**
 * Created by tactomotion on 7/10/16.
 */
public class ComandoConductor {


    private FirebaseAuth mAuth = FirebaseAuth.getInstance();


    public static void getConductor(final Servicio servicio, final String idConductor){
        Modelo modelo = Modelo.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("empresa/conductores/"+idConductor+"/");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snap) {
                if (snap.getValue() == null){
                    getConductorTercero(servicio, idConductor, null);

                }else {
                    Conductor cond = leerConductor(snap);
                    servicio.conductor = cond;
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    public interface OnCmdConductorListener {

        void cargoTercero();

    }


    public static void getConductorTercero(final Servicio servicio, final String idConductor, final OnCmdConductorListener listener){
        Modelo modelo = Modelo.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("empresa/conductoresTerceros/"+idConductor+"/");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snap) {
                if (snap.getValue() == null){

                }else {
                    Conductor cond = leerConductor(snap);
                    servicio.conductor = cond;
                    if (listener != null ){
                        listener.cargoTercero();
                    }

                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }



    public static Conductor leerConductor(DataSnapshot snap){

        Conductor conductor = new Conductor();
        conductor.nombre = snap.child("nombre").getValue().toString();
        conductor.apellido = snap.child("apellido").getValue().toString();
        conductor.celular = snap.child("celular").getValue().toString();
        conductor.correo = snap.child("correo").getValue().toString();
        return  conductor;


    }




}
