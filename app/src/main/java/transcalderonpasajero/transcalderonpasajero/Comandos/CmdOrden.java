package transcalderonpasajero.transcalderonpasajero.Comandos;

/**
 * Created by tactomotion on 30/09/16.
 */

import android.support.annotation.NonNull;
import android.util.Log;

import com.firebase.client.ServerValue;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.Transaction.Handler;
import com.google.firebase.database.Transaction.Result;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import transcalderonpasajero.transcalderonpasajero.Clases.Empresa;
import transcalderonpasajero.transcalderonpasajero.Clases.Mensajes;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Pasajero;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.Clases.Ubicacion;
import transcalderonpasajero.transcalderonpasajero.Clases.UbicacionPasajero;
import transcalderonpasajero.transcalderonpasajero.sistema.Utility;


public class CmdOrden {

    Modelo modelo;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");



    /**
     * Interfaz para avisar de eventos a los interesados
     */
    public interface OnOrdenesListener {

        void nueva();
        void modificada(String idServicio, boolean cambioEstado);
        void eliminada();

    }



    public static void getOrdenesPasajero(final OnOrdenesListener listener){
        final Modelo modelo = Modelo.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("ordenes/pendientes/");
        Query query = ref.orderByChild("pasajeros/"+modelo.uid).equalTo("empresarial");
        ChildEventListener mlistner = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snap, String s) {
                Servicio nuevo =  leerOrden(snap);
                modelo.addServicio(nuevo);
                listener.nueva();


            }

            @Override
            public void onChildChanged(DataSnapshot snap, String s) {
                Servicio nuevo =  leerOrden(snap);
                boolean modServicio = modelo.addServicio(nuevo);
                listener.modificada(nuevo.id, modServicio);
            }


            @Override
            public void onChildRemoved(DataSnapshot snap) {
                Servicio nuevo =  leerOrden(snap);
                boolean modServicio = modelo.addServicio(nuevo);
                listener.modificada(nuevo.id, modServicio);

            }

            @Override
            public void onChildMoved(DataSnapshot snap, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        };

        query.addChildEventListener(mlistner);
        modelo.hijosListener.put(query, mlistner);

    }


    public static void getOrdenesHistorialPasajero(final OnOrdenesListener listener){
        final Modelo modelo = Modelo.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("ordenes/historial/");
        Query query = ref.orderByChild("pasajeros/"+modelo.uid).equalTo("empresarial");
        ChildEventListener mlistner = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snap, String s) {
                Servicio nuevo =  leerOrden(snap);
                modelo.addServicioViejitas(nuevo);
                listener.nueva();
            }

            @Override
            public void onChildChanged(DataSnapshot snap, String s) {
                Servicio nuevo =  leerOrden(snap);
                boolean modServicio = modelo.addServicioViejitas(nuevo);
                listener.modificada(nuevo.id, modServicio);
            }


            @Override
            public void onChildRemoved(DataSnapshot snap) {
                listener.eliminada();

            }

            @Override
            public void onChildMoved(DataSnapshot snap, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        query.addChildEventListener(mlistner);
        modelo.hijosListener.put(query, mlistner);

    }

    public interface OnCurrentService {

        void cargoCurrent();

    }

    public static void saveCurrentService() {

        final Modelo modelo = Modelo.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("usuarios/" + modelo.uid + "/currentService");

        Servicio ser = modelo.currentService;
        Map<String, Object> dicc = new HashMap<String, Object>();
        if (ser.destino == null || ser.destino.equals("")) {
            dicc.put("destino", ser.origen);
        } else {
            dicc.put("destino", ser.destino);
        }
        dicc.put("diasServicio", ser.diasServicio);
        dicc.put("direccionDestino", ser.direccionDestino);
        dicc.put("direccionOrigen", ser.direccionOrigen);
        dicc.put("fechaEnOrigen", ser.fechaEnOrigen);
        dicc.put("horaEnOrigen", ser.horaEnOrigen);
        dicc.put("horasServicio", ser.horasServicio);
        dicc.put("observaciones", ser.observaciones);
        dicc.put("origen", ser.origen);


        Map<String, Object> ubiOrigen = new HashMap<String, Object>();
        if (ser.ubiOrigen != null) {
            ubiOrigen.put("lat", ser.ubiOrigen.latitud);
            ubiOrigen.put("lon", ser.ubiOrigen.longitud);
            dicc.put("ubicacionOrigen", ubiOrigen);
        }

        Map<String, Object> ubiDestino = new HashMap<String, Object>();
        if (!ser.abierto && ser.ubiDestino != null) {
            ubiDestino.put("lat", ser.ubiDestino.latitud);
            ubiDestino.put("lon", ser.ubiDestino.longitud);
            dicc.put("ubicacionDestino", ubiDestino);
        }


        dicc.put("ruta", ser.getRuta());
        dicc.put("tipoVehiculo", ser.tipoVehiculo);

        Map<String, Object> diccPasajeros = new HashMap<String, Object>();
        diccPasajeros.put(modelo.uid, "empresarial");


        for (Pasajero pasajero : modelo.currentService.pasajerosEmp) {
            diccPasajeros.put(pasajero.uid, "empresarial");
        }

        for (Pasajero pasajero : modelo.currentService.pasajerosPart) {
            diccPasajeros.put(pasajero.uid, "particular");
        }

        dicc.put("pasajeros", diccPasajeros);

        try {
        ref.setValue(dicc);
    } catch (DatabaseException e){

            Log.i("ERROR al salvar", e.getMessage());
            Log.i("ERROR al salvar", dicc.toString());
        }



    }


    public static void getCurrentService(){

        final Modelo modelo = Modelo.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("usuarios/" + modelo.uid + "/currentService");

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snap) {
                Servicio serv = leerOrden(snap);
                if (serv == null){
                    modelo.currentService = new Servicio();
                    modelo.currentService.modoResureccion = false;
                }else{
                    serv.modoResureccion = false;
                    modelo.currentService = serv;

                }

            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    public static void deleteCurrenteService(){
        final Modelo modelo = Modelo.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("usuarios/" + modelo.uid + "/currentService");
        ref.removeValue();

    }



    public static Servicio leerOrden(DataSnapshot snap){

        Modelo modelo = Modelo.getInstance();
        Servicio nuevaOrden = new Servicio();

        if (snap.getValue() == null ){
            return null;
        }

        nuevaOrden.id = snap.getKey();

        if (snap.hasChild("origen")) {
            nuevaOrden.setOrigen(snap.child("origen").getValue().toString());
        }

        if (snap.hasChild("estado")) {
            nuevaOrden.setEstado(snap.child("estado").getValue().toString());
        }
        if (snap.hasChild("destino")) {
            nuevaOrden.setDestino(snap.child("destino").getValue().toString());
            if (nuevaOrden.destino.equals("ABIERTO")){
                nuevaOrden.abierto = true;
            }
        }
        if (snap.hasChild("horaEnOrigen")) {
            nuevaOrden.horaEnOrigen =  snap.child("horaEnOrigen").getValue().toString();

        }
        if (snap.hasChild("direccionDestino")) {
            nuevaOrden.direccionDestino = snap.child("direccionDestino").getValue().toString();
        }


        if (snap.hasChild("fechaEnOrigen")) {
            nuevaOrden.setFechaEnOrigen(snap.child("fechaEnOrigen").getValue().toString());
        }


        if (snap.hasChild("fechaGeneracion")) {
            nuevaOrden.fechaGeneracion = snap.child("fechaGeneracion").getValue().toString();
        }

        if(snap.hasChild("tipoVehiculo")) {
            nuevaOrden.tipoVehiculo = snap.child("tipoVehiculo").getValue().toString();
        }

        if(snap.hasChild("conductor")){
            nuevaOrden.idConductor = snap.child("conductor").getValue().toString();
            ComandoConductor.getConductor(nuevaOrden, nuevaOrden.idConductor);
        }


        if(snap.hasChild("cosecutivoOrden")){
            nuevaOrden.setCosecutivoOrden(snap.child("cosecutivoOrden").getValue().toString());
        }else{
            if(snap.hasChild("consecutivoOrden")){
                nuevaOrden.setCosecutivoOrden(snap.child("consecutivoOrden").getValue().toString());
            }else{
                nuevaOrden.setCosecutivoOrden("");
            }
        }

        if(snap.hasChild("direccionOrigen")) {
            nuevaOrden.setDireccionOrigen(snap.child("direccionOrigen").getValue().toString());
        }

        if(snap.hasChild("horaGeneracion")){
            nuevaOrden.setHoraGeneracion(snap.child("horaGeneracion").getValue().toString());
        }
        if(snap.hasChild("idCliente")) {
            nuevaOrden.setIdCliente(snap.child("idCliente").getValue().toString());
        }

        if(snap.hasChild("matricula")){
            nuevaOrden.setMatricula(snap.child("matricula").getValue().toString());

        }else{
            nuevaOrden.setMatricula("");
        }

        if(snap.hasChild("ruta")){
            String[] parts  = snap.child("ruta").getValue().toString().split("-");

            for (String par : parts){
                nuevaOrden.trayectos.add(par);
            }

            nuevaOrden.ruta = snap.child("ruta").getValue().toString();

        }

        if (snap.hasChild("horasServicio")){
            nuevaOrden.horasServicio = snap.child("horasServicio").getValue().toString();
        }

        if (snap.hasChild("diasServicio")){
            nuevaOrden.diasServicio = snap.child("diasServicio").getValue().toString();
        }


        if(snap.hasChild("solicitadoPor")) {
            nuevaOrden.setSolicitadoPor(snap.child("solicitadoPor").getValue().toString());
        }

        // nuevaOrden.setTarifa(snap.child("tarifa").getValue().toString());

        if (snap.hasChild("tarifa")){
            nuevaOrden.setTarifa(snap.child("tarifa").getValue().toString());

        }else{
            nuevaOrden.setTarifa("");
        }

        if (snap.hasChild("timestamp")){
            Long l  = (Long)snap.child("timestamp").getValue();
            nuevaOrden.setTimeStamp(l);
        }

        nuevaOrden.setId(snap.getKey());


        DataSnapshot snapNotificaones;

        if (snap.hasChild("mensajes")){
            snapNotificaones = (DataSnapshot) snap.child("mensajes");//mensajes
            for (DataSnapshot notificaciones : snapNotificaones.getChildren()){


                Mensajes newMensaje = new Mensajes();
                newMensaje.setId(notificaciones.getKey());
                newMensaje.setHora(notificaciones.child("hora").getValue().toString());
                newMensaje.setTexto(notificaciones.child("texto").getValue().toString());
                Long lg  = (Long)snap.child("timestamp").getValue();
                newMensaje.setTimestamp(lg);
                nuevaOrden.notificaciones.add(0, newMensaje);//

            }

        }



        if (snap.hasChild("pasajeros")){
            DataSnapshot snapPasajeros = (DataSnapshot) snap.child("pasajeros");//mensajes
            for (DataSnapshot pasa : snapPasajeros.getChildren()){

                String tipo = pasa.getValue().toString();
                if (tipo.equals("empresarial")){
                    if (pasa.getKey().equals(modelo.uid)){
                        continue;
                    }

                    Pasajero pasajero = modelo.empresa.getPasajeroEmpresarialById(pasa.getKey());
                    nuevaOrden.pasajerosEmp.add(pasajero);
                }

                if (tipo.equals("particular")){
                    Pasajero pasajero = modelo.pasajero.getPasajeroAmigoById(pasa.getKey());
                    nuevaOrden.pasajerosPart.add(pasajero);
                }
            }
        }

        if (snap.hasChild("calificacion")){
            DataSnapshot snapPasajeros = (DataSnapshot) snap.child("calificacion");//mensajes
            for (DataSnapshot pasa : snapPasajeros.getChildren()){

                if (pasa.child("pasajero").getValue().toString().equals(modelo.uid)){
                    nuevaOrden.yaCalificado = true;
                }

            }
        }



        if (snap.hasChild("ubicacionOrigen")){
            DataSnapshot snapUbi = snap.child("ubicacionOrigen");
            Ubicacion ubi = new Ubicacion();
            ubi.latitud = (double)snapUbi.child("lat").getValue();
            ubi.longitud = (double)snapUbi.child("lon").getValue();
            nuevaOrden.ubiOrigen = ubi;
        }


        if (snap.hasChild("ubicacionDestino")){
            DataSnapshot snapUbi = snap.child("ubicacionDestino");
            Ubicacion ubi = new Ubicacion();
            ubi.latitud = (double)snapUbi.child("lat").getValue();
            ubi.longitud = (double)snapUbi.child("lon").getValue();
            nuevaOrden.ubiDestino = ubi;
        }



        //nuevo arbol snapUbicacion
        DataSnapshot snapUbicacion;
        snapUbicacion = (DataSnapshot) snap.child("ubicacion");//ubicacion
        nuevaOrden.ubicacionPasajeroGPsses.clear();
        for (DataSnapshot gps : snapUbicacion.getChildren()) {
            UbicacionPasajero newUbicacionPasajero = new UbicacionPasajero();
            newUbicacionPasajero.setPathUbicacion(gps.getKey());
            double latd  = (double)gps.child("lat").getValue();
            newUbicacionPasajero.setLat(latd);
            double lon  = (double)gps.child("lon").getValue();
            newUbicacionPasajero.setLon(lon);
            Long lu  = (Long)gps.child("timestamp").getValue();
            newUbicacionPasajero.setTimestamp(lu);
            newUbicacionPasajero.setPasajero(gps.child("pasajero").getValue().toString());

            nuevaOrden.ubicacionPasajeroGPsses.add(0, newUbicacionPasajero);
        }




        if (nuevaOrden.destino != null && nuevaOrden.destino.equals("ABIERTO")
                && nuevaOrden.diasServicio.equals("") && nuevaOrden.horasServicio.equals("")){

            nuevaOrden.indefinido = true;
        }

        return  nuevaOrden;

    }


    public interface OnValidarListener {

        void valido();

    }


    public static void validarUsuario(final OnValidarListener listener){
        final Modelo modelo = Modelo.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("usuarios/"+modelo.uid);//ruta path
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snap) {
                if(snap.getValue() == null){
                    System.out.print("No exise");
                    modelo.validaUsuario ="No exise";
                }else{
                    System.out.print("Si exise");
                    modelo.validaUsuario ="Si exise";
                }

                listener. valido();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }




    public static void crearOrden(){

        final Modelo modelo = Modelo.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("ordenes/pendientes/");
        DatabaseReference key = ref.push();
        ref = database.getReference("ordenes/pendientes/"+key.getKey());


        final DatabaseReference finalRef = ref;
        reservarNumeroOrden(new OnReservaNumeroOrden() {
            @Override
            public void reservaExistosa(int numero) {


                Empresa emp = modelo.empresa;
                Servicio ser = modelo.currentService;
                Map<String, Object> dicc = new HashMap<String, Object>();
                dicc.put("consecutivoOrden",  Utility.llenarCeros(emp.inicialesConsecutivo,numero));
                dicc.put("destino", ser.destino);
                dicc.put("diasServicio", ser.diasServicio);
                dicc.put("direccionDestino", ser.direccionDestino);
                dicc.put("direccionOrigen",ser.direccionOrigen);
                dicc.put("fechaEnOrigen",ser.fechaEnOrigen);
                dicc.put("fechaGeneracion", Utility.convertDateToString(new Date()));
                dicc.put("horaEnOrigen", ser.horaEnOrigen);
                dicc.put("horaGeneracion", Utility.getHora());
                dicc.put("horasServicio",ser.horasServicio);
                dicc.put("idCliente",emp.id);
                dicc.put("observaciones",ser.observaciones);
                dicc.put("origen",ser.origen);
                dicc.put("solicitadoPor", modelo.uid);
                dicc.put("timestamp", ServerValue.TIMESTAMP);

                if (modelo.pasajero.categoria.equals("A")) {
                    dicc.put("estado","NoAsignado");
                }else {
                    dicc.put("estado","SinConfirmar");
                }


                Map<String, Object> ubiOrigen = new HashMap<String, Object>();
                ubiOrigen.put("lat", ser.ubiOrigen.latitud);
                ubiOrigen.put("lon", ser.ubiOrigen.longitud);
                Map<String, Object> ubiDestino = new HashMap<String, Object>();

                if (!ser.abierto){
                    ubiDestino.put("lat", ser.ubiDestino.latitud);
                    ubiDestino.put("lon", ser.ubiDestino.longitud);
                    dicc.put("ubicacionOrigen",ubiOrigen);
                    dicc.put("ubicacionDestino",ubiDestino);
                }


                dicc.put("ruta",ser.getRuta());
                dicc.put("tipoVehiculo",ser.tipoVehiculo);

                Map<String, Object> diccPasajeros = new HashMap<String, Object>();
                diccPasajeros.put(modelo.uid, "empresarial");

                for (Pasajero pasajero : modelo.currentService.pasajerosEmp ){
                    diccPasajeros.put(pasajero.uid,"empresarial");
                }

                for (Pasajero pasajero : modelo.currentService.pasajerosPart){
                    diccPasajeros.put(pasajero.uid,"particular");
                }

                dicc.put("pasajeros", diccPasajeros);

                finalRef.setValue(dicc);

            }

        });

    }

    private interface OnReservaNumeroOrden {
        void reservaExistosa(int numero);

    }


    public static void reservarNumeroOrden( final OnReservaNumeroOrden listener) {

        final Modelo modelo = Modelo.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("clientes/" + modelo.empresa.id + "/consecutivoOrden");


        ref.runTransaction(new Handler() {
            @Override
            public Result doTransaction(MutableData mutableData) {
                Object o = mutableData.getValue();

                if (o == null) {
                    return Transaction.success(mutableData);
                }

                int numero = Integer.valueOf(o.toString());

                mutableData.setValue(numero + 1);


                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean commited, DataSnapshot snap) {
                if (commited) {

                    listener.reservaExistosa(Integer.valueOf(snap.getValue().toString()));

                }

            }
        });
    }

}

