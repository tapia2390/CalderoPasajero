package transcalderonpasajero.transcalderonpasajero.Comandos;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;

/**
 * Created by tactomotion on 7/12/16.
 */
public class ComandoRegistrarte {


    Modelo modelo = Modelo.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    DatabaseReference referencia = database.getReference();
    private FirebaseUser user = mAuth.getCurrentUser();

    /**
     * Interfaz para avisar de eventos a los interesados
     */
    public interface OnRegistroChangeListener {

        void cargoRegistro();

    }

    //interface del listener de la actividad interesada
    private OnRegistroChangeListener mListener;

    public ComandoRegistrarte(OnRegistroChangeListener mListener){

        this.mListener = mListener;

    }


    public  void enviarRegistro(final String nombre, final String apellido, final String celular, final String correo, final String foto, final String idCliente, final String tokenDevice,final String uid){

        final DatabaseReference ref = database.getReference("usuarios/"+uid+"/" );//ruta path
        Map<String, Object> enviarRegistro = new HashMap<String, Object>();
        enviarRegistro.put("activo", true);
        enviarRegistro.put("apellido", apellido);
        enviarRegistro.put("celular", celular );
        enviarRegistro.put("conApp", true );
        enviarRegistro.put("correo", correo );
        enviarRegistro.put("estado", "pendiente" );
        enviarRegistro.put("foto", foto );
        enviarRegistro.put("idCliente", idCliente );
        enviarRegistro.put("nombre", nombre);
        //enviarRegistro.put("passwordTemporal", "passwordTemporal");
        enviarRegistro.put("rol", "Pasajero");
        enviarRegistro.put("tokenDevice", tokenDevice);
        enviarRegistro.put("timestamp", ServerValue.TIMESTAMP);
        ref.setValue(enviarRegistro);
        mListener.cargoRegistro();
    }

    /**
     * Para evitar nullpointerExeptions
     */
    private static OnRegistroChangeListener sDummyCallbacks = new OnRegistroChangeListener()
    {
        @Override
        public void cargoRegistro()
        {}



    };
}
