package transcalderonpasajero.transcalderonpasajero;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.Date;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden;
import transcalderonpasajero.transcalderonpasajero.home.NewHomeNav;

public class ServiceBoot extends Service  {

    Modelo modelo = Modelo.getInstance();
    private static final String TAG = "EjemploServicio";
    CmdOrden comandoP  = new CmdOrden();



    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        //Toast.makeText(this, "Servicio ServiceBoot", Toast.LENGTH_LONG).show();
        Log.v("servicio", "iniciado ServiceBoot");
        //comandoP.getTodasLasOrdenesPasajero();

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        Toast.makeText(this, "Servicio destruido ServiceBoot", Toast.LENGTH_LONG).show();

    }

    public void notifi(){


        for (int j= 0; j< modelo.getOrdenes().size(); j++){
            int ids =  j;
            String   consecutivo = modelo.getOrdenes().get(j).getCosecutivoOrden();
            String  destino = modelo.getOrdenes().get(j).getDestino();
            String tiker = ""+modelo.getOrdenes().get(j).getFechaEnDestino();
            notifficacion(tiker, ids, consecutivo, destino);
        }



    }

    public void notifficacion(String tiker1, int ids, String consecutivo, String destino){
        int idNotificacion = 0;
        //Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
        Log.v("android", "ENTROOOOOOOOO1  Notificacion");

        Date date = new Date();
        // CharSequence ticker ="Fecha Destino "+tiker1;

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Intent i = new Intent(this, NewHomeNav.class);
        i.putExtra("vistaPosicion","dos");
        i.putExtra("notificationID", ids);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, i, 0);
        NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);




        //Log.e("android", "ENTROOOOOOOOO2");
        Notification noti = new NotificationCompat.Builder(this)
                .setContentIntent(pendingIntent)
                //.setTicker(""+ticker)
                .setContentTitle(""+consecutivo)
                .setContentText(""+destino)
                .setSmallIcon(R.mipmap.ic_launcher)
                // .addAction(R.mipmap.ic_launcher, ticker, pendingIntent)
                .setVibrate(new long[] {100, 250, 100, 500})
                .setSound(soundUri)
                .build();
        noti.flags |= Notification.FLAG_AUTO_CANCEL;
        nm.notify(idNotificacion, noti);

    }




}
