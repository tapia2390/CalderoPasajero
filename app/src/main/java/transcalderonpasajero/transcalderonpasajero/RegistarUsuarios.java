package transcalderonpasajero.transcalderonpasajero;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import static android.Manifest.permission.CAMERA;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Pasajero;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdEmpresa;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdEmpresa.OnEmpresaListener;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoPasajero;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoPasajero.OnPasajeroChangeListener;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoRegistrarte;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoValidarUsuario;
import transcalderonpasajero.transcalderonpasajero.home.NewHomeNav;
import transcalderonpasajero.transcalderonpasajero.sistema.Utility;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
public class RegistarUsuarios extends Activity implements ComandoRegistrarte.OnRegistroChangeListener, ComandoValidarUsuario.OnOValidarUsuarioChangeListener {


    EditText txt_tipodocumento,nombres,apellidos,celular,correo,password;
    final Context context = this;
    Modelo modelo = Modelo.getInstance();
    String listadoClientesEmpresa ="";
    String listadoClientesId ="";
    LinearLayout liusta_layout;
    ComandoRegistrarte comandoRegistrarte;
    //foto
    String userChoosenTask="";
    boolean img_cam1 = false;
    private int REQUEST_CAMERA = 0;
    int SELECT_FILE = 1;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1 ;
    ImageView camara1;
    TextView txt_camara1;

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser user = mAuth.getCurrentUser();
    private ProgressDialog progressDialog;
    private static final String TAG ="AndroidBash";
    String token ="";
    String idClientes ="";
    String foto ="";


    ComandoValidarUsuario comandoValidarUsuario = new ComandoValidarUsuario(this);

    private static String APP_DIRECTORY = "MyPictureApp/";
    private static String MEDIA_DIRECTORY = APP_DIRECTORY + "PictureApp";

    private final int MY_PERMISSIONS = 100;
    private final int PHOTO_CODE = 200;
    private final int SELECT_PICTURE = 300;
    private String mPath;
    private LinearLayout mRlView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_registar_usuarios);

        txt_tipodocumento = (EditText)findViewById(R.id.txt_tipodocumento);
        nombres = (EditText)findViewById(R.id.nombres);
        apellidos = (EditText)findViewById(R.id.apellidos);
        celular = (EditText)findViewById(R.id.celular);
        correo = (EditText)findViewById(R.id.correo);
        password = (EditText)findViewById(R.id.password);
        liusta_layout = (LinearLayout)findViewById(R.id.liusta_layout);
        camara1 = (ImageView)findViewById(R.id.camara1);
        txt_camara1 = (TextView) findViewById(R.id.txt_camara1);
        mRlView = (LinearLayout) findViewById(R.id.rl_view);

        comandoRegistrarte = new ComandoRegistrarte(this);

        CmdEmpresa.listadoClienteEmpresa();

        token = FirebaseInstanceId.getInstance().getToken();

        progressDialog = new ProgressDialog(this);


    }



    public void finalizar(View v){

        if(!modelo.existeNit(txt_tipodocumento.getText().toString())){
            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                    RegistarUsuarios.this);
            alertDialogBuilder.setTitle("Lo sentimos");
            alertDialogBuilder
                    .setMessage("El NIT inscrito no coincide con ninguna empresa registrada. Revise que los datos ingresados estén correctos")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            return;
                        }
                    });

            // create alert dialog
            android.app.AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
            return;
        }

        if(nombres.getText().toString().length() < 3){
            nombres.setError("Información muy corta");
        }
        else if(apellidos.getText().toString().length() < 3){
            apellidos.setError("Información muy corta");
        }
        else if(celular.getText().toString().length() != 10 || !Utility.isNumeric(celular.getText().toString())){
            celular.setError("El celular debe tener 10 digitos");
        }
        else if(!Utility.isEmailValid(correo.getText().toString()) ){
            correo.setError("Correo invalido");
        }
        else if(password.getText().toString().length() < 3){
            password.setError("Información muy corta");

        }else{

            if(estaConectado()){
                idClientes = modelo.getIdEmpresa(txt_tipodocumento.getText().toString());
                validarDatos();
            }else {
                showAlertDialog(RegistarUsuarios.this, "Conexion a Internet",
                        "Tu Dispositivo no tiene Conexion a Internet.", false);
            }

        }

    }



    //inicio validar datos
    private void validarDatos() {
            // OK, se pasa a la siguiente acción
            progressDialog.setMessage("Registrando correo, por favor espere...");
            progressDialog.show();

            mAuth.createUserWithEmailAndPassword(correo.getText().toString(), password.getText().toString())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            //checking if success
                            Log.d(TAG, "signInWithEmail:onComplete:"+task.isSuccessful());

                            FirebaseAuthException ex = (FirebaseAuthException) task.getException();
                            if (ex!=null){
                                Log.v("error re", "registro error"+ex.getLocalizedMessage());

                                //Toast.makeText(getApplicationContext(), "Ocurrio un error al registrarse, intente nuevamente.", Toast.LENGTH_LONG).show();
                                System.out.print(""+ex.getLocalizedMessage());

                                String error = ex.getErrorCode();

                                progressDialog.dismiss();
                                Log.v("log re","error re"+error);
                                if(error.equals("ERROR_EMAIL_ALREADY_IN_USE")){
                                    Toast.makeText(getApplicationContext(), " Correo ya  existente.", Toast.LENGTH_LONG).show();
                                }//ERROR_WEAK_PASSWORD

                                if(error.equals("ERROR_WEAK_PASSWORD")){
                                    Toast.makeText(getApplicationContext(), " Contraseña muy debil.", Toast.LENGTH_LONG).show();
                                }

                                return;
                            }


                            if(task.isSuccessful()){
                                Toast.makeText(getApplicationContext(), "Registrado exitosamente", Toast.LENGTH_LONG).show();
                                createNewUser(task.getResult().getUser());


                            }else{
                                //display some message here
                                Toast.makeText(getApplicationContext(),"error de registro, intentelo de nuevo",Toast.LENGTH_LONG).show();
                            }

                        }
                    });


    }


    private void createNewUser(FirebaseUser userFromRegistration) {
        String email = userFromRegistration.getEmail();
        String userId = userFromRegistration.getUid();

        ingresar2(userId);
    }


    public void ingresar2(String userUid ){

        comandoRegistrarte.enviarRegistro(nombres.getText().toString(), apellidos.getText().toString(),
                celular.getText().toString(),correo.getText().toString(),foto, idClientes,
                token, userUid);
    }

    protected Boolean estaConectado(){
        if(conectadoWifi()){
            Log.v("wifi","Tu Dispositivo tiene Conexion a Wifi.");
            return true;
        }else{
            if(conectadoRedMovil()){
                Log.v("Datos", "Tu Dispositivo tiene Conexion Movil.");
                return true;
            }else{
                showAlertDialog(RegistarUsuarios.this, "Conexion a Internet",
                        "Tu Dispositivo no tiene Conexion a Internet.", false);
                return false;
            }
        }
    }

    protected Boolean conectadoWifi(){
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    protected Boolean conectadoRedMovil(){
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        alertDialog.show();
    }

    public void showAlertRegistroExitoso() {
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                RegistarUsuarios.this);
        alertDialogBuilder.setTitle("Registro recibido");
        alertDialogBuilder
                .setMessage("Hemos reibido tu solicitud, tus datos serán validados por el responsable de la empresa para que puedas acceder a los servicios")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Intent i = new Intent(RegistarUsuarios.this, NewHomeNav.class);
                        startActivity(i);
                        finish();
                        return;
                    }
                });

        // create alert dialog
        android.app.AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
        return;
    }




    @Override
    public void cargoRegistro() {

        FirebaseUser user = mAuth.getCurrentUser();

        if (user != null ){
            modelo.uid = user.getUid();
            validarUsuario();


        }


        showAlertRegistroExitoso();

    }

    private void validarUsuario() {
        comandoValidarUsuario.validarUsuario();
    }

    @Override
    public void validandoPasajeroOK() {

        ComandoPasajero.getPasajero(modelo.uid, new OnPasajeroChangeListener() {
            @Override
            public void cargoPasajero(Pasajero pasajero) {
                modelo.context = getApplicationContext();
                modelo.pasajero = pasajero;

                CmdEmpresa.getEmpresa(new OnEmpresaListener() {
                    @Override
                    public void cargoEmpresa() {
                        CmdOrden.getCurrentService();
                        Intent i = new Intent(RegistarUsuarios.this, NewHomeNav.class);
                        startActivity(i);
                        overridePendingTransition(R.anim.zoom_back_in, R.anim.zoom_back_out);
                        finish();

                    }
                });
            }


        });



    }

    @Override
    public void validandoPasajeroError() {

    }

    @Override
    public void pasajeroPendiente() {
        ComandoPasajero.getPasajero(modelo.uid, new OnPasajeroChangeListener() {
            @Override
            public void cargoPasajero(Pasajero pasajero) {
                modelo.context = getApplicationContext();
                modelo.pasajero = pasajero;

                CmdEmpresa.getEmpresa(new OnEmpresaListener() {
                    @Override
                    public void cargoEmpresa() {
                        CmdOrden.getCurrentService();
                        Intent i = new Intent(RegistarUsuarios.this, NewHomeNav.class);
                        startActivity(i);
                        overridePendingTransition(R.anim.zoom_back_in, R.anim.zoom_back_out);
                        finish();

                    }
                });
            }


        });
    }





    public void atras2(){
        Intent i  = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }

    public void atras(View v){
        atras2();
    }

    public void camara1(View v){
        if(mayRequestStoragePermission()){
            camara1.setEnabled(true);
            showOptions();
        }

        else{
            camara1.setEnabled(false);

        }

    }

    //manejo camara y galeria
    private boolean mayRequestStoragePermission() {

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return true;

        if((checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) &&
                (checkSelfPermission(CAMERA) == PackageManager.PERMISSION_GRANTED))
            return true;

        if((shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) || (shouldShowRequestPermissionRationale(CAMERA))){
            Snackbar.make(mRlView, "Los permisos son necesarios para poder usar la aplicación",
                    Snackbar.LENGTH_INDEFINITE).setAction(android.R.string.ok, new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.M)
                @Override
                public void onClick(View v) {
                    requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, CAMERA}, MY_PERMISSIONS);
                }
            });
        }else{
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, CAMERA}, MY_PERMISSIONS);
        }

        return false;
    }

    private void showOptions() {
        final CharSequence[] option = {"Tomar foto", "Elegir de galeria", "Cancelar"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(RegistarUsuarios.this);
        builder.setTitle("Eleige una opción");
        builder.setItems(option, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(option[which] == "Tomar foto"){
                    openCamera();
                }else if(option[which] == "Elegir de galeria"){
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent.createChooser(intent, "Selecciona app de imagen"), SELECT_PICTURE);
                }else {
                    dialog.dismiss();
                }
            }
        });

        builder.show();
    }

    private void openCamera() {
        File file = new File(Environment.getExternalStorageDirectory(), MEDIA_DIRECTORY);
        boolean isDirectoryCreated = file.exists();

        if(!isDirectoryCreated)
            isDirectoryCreated = file.mkdirs();

        if(isDirectoryCreated){
            Long timestamp = System.currentTimeMillis() / 1000;
            String imageName = timestamp.toString() + ".jpg";

            mPath = Environment.getExternalStorageDirectory() + File.separator + MEDIA_DIRECTORY
                    + File.separator + imageName;

            File newFile = new File(mPath);

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(newFile));
            startActivityForResult(intent, PHOTO_CODE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("file_path", mPath);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        mPath = savedInstanceState.getString("file_path");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            switch (requestCode){
                case PHOTO_CODE:
                    MediaScannerConnection.scanFile(this,
                            new String[]{mPath}, null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                @Override
                                public void onScanCompleted(String path, Uri uri) {
                                    Log.i("ExternalStorage", "Scanned " + path + ":");
                                    Log.i("ExternalStorage", "-> Uri = " + uri);
                                }
                            });


                    Bitmap bitmap = BitmapFactory.decodeFile(mPath);
                    camara1.setImageBitmap(getCircularBitmap(bitmap));

                    txt_camara1.setText("Cambiar Foto");
                     foto = encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG, 100);

                    break;
                case SELECT_PICTURE:
                    Uri path = data.getData();

                    try {
                        Bitmap imagen = getBitmapFromUri (path);
                        camara1.setImageBitmap(getCircularBitmap(imagen));
                        foto = encodeToBase64(imagen, Bitmap.CompressFormat.JPEG, 100);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                   // camara1.setImageURI(path);
                    txt_camara1.setText("Cambiar Foto");


                    break;

            }
        }
    }


    //convertir uri en bitmap
    private Bitmap getBitmapFromUri ( Uri uri ) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getContentResolver (). openFileDescriptor ( uri , "r" );
        FileDescriptor fileDescriptor = parcelFileDescriptor . getFileDescriptor ();
        Bitmap image = BitmapFactory . decodeFileDescriptor ( fileDescriptor );
        parcelFileDescriptor . close ();
        return image ;
    }

    //convertir bitmap a base64
    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality)
    {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    public static Bitmap decodeBase64(String input)
    {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == MY_PERMISSIONS){
            if(grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(RegistarUsuarios.this, "Permisos aceptados", Toast.LENGTH_SHORT).show();
                camara1.setEnabled(true);
            }
        }else{
            showExplanation();
        }
    }

    private void showExplanation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(RegistarUsuarios.this);
        builder.setTitle("Permisos denegados");
        builder.setMessage("Para usar las funciones de la app necesitas aceptar los permisos");
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });

        builder.show();
    }
    //fin manejo de galeria y camara




    //imagen circular
    public static Bitmap getCircularBitmap(Bitmap bitmap) {
        Bitmap output;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        } else {
            output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        float r = 0;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            r = bitmap.getHeight() / 2;
        } else {
            r = bitmap.getWidth() / 2;
        }

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(r, r, r, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }
}
