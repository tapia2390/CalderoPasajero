package transcalderonpasajero.transcalderonpasajero;


import android.app.Activity;
import android.content.Intent;

public class Classservice extends Activity{


    /* mi Servicios */
    public void Start_Service(){
        Intent intent = new Intent(Classservice.this, MiService.class);
        intent.putExtra("primerplano", false);
        startService(intent);
    }

    /* mi MiServiceForeground */
    private void MiServiceForeground() {

    }

    public void Stop_Service(){
        stopService(new Intent(Classservice.this, MiService.class));
    }
	/*fin*/

}