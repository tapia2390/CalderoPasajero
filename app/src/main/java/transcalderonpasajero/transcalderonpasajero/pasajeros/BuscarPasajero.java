package transcalderonpasajero.transcalderonpasajero.pasajeros;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.SectionIndexer;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


import transcalderonpasajero.transcalderonpasajero.Clases.IndexableListView;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Pasajero;

import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden.OnOrdenesListener;
import transcalderonpasajero.transcalderonpasajero.MainActivity;
import transcalderonpasajero.transcalderonpasajero.R;
import transcalderonpasajero.transcalderonpasajero.Splash;

public class BuscarPasajero extends Activity {


        private static final List<Pasajero> pasajeroList = new ArrayList<Pasajero>();
        private IndexableListView listView;
        private EditText search;
        private EfficientAdapter adapter;
        TextView nombres, titulo;

        Modelo modelo = Modelo.getInstance();;
        final Context context = this;
        String tipo = "EMPRESARIAL";


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
            setContentView(R.layout.activity_buscar_pasajero);

            if (savedInstanceState != null){

                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                finish();
                return;

            }


            titulo = findViewById(R.id.titulo);

            tipo = getIntent().getStringExtra("TIPO");

            if (tipo.equals("EMPRESARIAL")){
                titulo.setText("Pasajeros Empresariales");
            }
            else {
                titulo.setText("Pasajeros Particulares");
            }




        }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }


        private void listaCiudades() {


            if (estaConectado()) {

                pasajeroList.clear();
                modelo.dataPasajerosE.clear();

                if (tipo.equals("EMPRESARIAL")) {
                    for (int i = 0; i < modelo.empresa.empresariales.size(); i++) {
                        pasajeroList.add(modelo.empresa.empresariales.get(i));
                    }
                }

                else {
                    for (int i = 0; i < modelo.pasajero.amigos.size(); i++) {
                        pasajeroList.add(modelo.pasajero.amigos.get(i));
                    }
                }

                for (char c = 'A'; c <= 'Z'; c++) {
                    Pasajero letra = new Pasajero();
                    letra.nombre = "" + c;
                    pasajeroList.add(letra);
                }

                listView = (IndexableListView) findViewById(R.id.listView_main_menu);
                nombres = findViewById(R.id.nombres);
                setNombre(tipo);


                //Collections.sort(countryList);
                adapter = new EfficientAdapter(this);
                listView.setAdapter(adapter);
                listView.setFastScrollEnabled(true);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapter, View view, int pos, long id) {
                        String titulo = modelo.dataPasajerosE.get(pos).nombre;


                        for (char c = 'A'; c <= 'Z'; c++) {
                            if (titulo.equals(c)) {
                                return;
                            } else {
                                continue;
                            }
                        }

                        if (titulo.equalsIgnoreCase("A") || titulo.equalsIgnoreCase("B") || titulo.equalsIgnoreCase("C") || titulo.equalsIgnoreCase("D") ||
                                titulo.equalsIgnoreCase("E") || titulo.equalsIgnoreCase("F") || titulo.equalsIgnoreCase("G") || titulo.equalsIgnoreCase("H") ||
                                titulo.equalsIgnoreCase("I") || titulo.equalsIgnoreCase("J") || titulo.equalsIgnoreCase("K") || titulo.equalsIgnoreCase("L") ||
                                titulo.equalsIgnoreCase("M") || titulo.equalsIgnoreCase("N") || titulo.equalsIgnoreCase("O") || titulo.equalsIgnoreCase("P") ||
                                titulo.equalsIgnoreCase("Q") || titulo.equalsIgnoreCase("R") || titulo.equalsIgnoreCase("S") || titulo.equalsIgnoreCase("T") ||
                                titulo.equalsIgnoreCase("U") || titulo.equalsIgnoreCase("V") || titulo.equalsIgnoreCase("W") || titulo.equalsIgnoreCase("X") ||
                                titulo.equalsIgnoreCase("Y") || titulo.equalsIgnoreCase("Z")) {

                        } else {
                            modelo.currentService.addEliminarPasajero(tipo, modelo.dataPasajerosE.get(pos));
                            CmdOrden.saveCurrentService();
                            setNombre(tipo);
                        }

                    }
                });



                search = findViewById(R.id.editText_main_search);
                search.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count,
                                                  int after) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                        adapter.getFilter().filter(s);

                    }
                });

            } else {
                showAlertSinInternet();
            }
        }



        public  void setNombre(String tipo){

            String pasajeros = modelo.currentService.getListaPasajeros(tipo);

            if (pasajeros.equals("")){
                nombres.setVisibility(View.GONE);
            }else{
                nombres.setVisibility(View.VISIBLE);
                nombres.setText(pasajeros);
            }
        }


        public void atras() {
            modelo.data.clear();
            finish();
        }

    public void didTapNuevo(View view) {

            Intent intent = new Intent(getApplicationContext(), CrearPasajero.class);
            startActivity(intent);

    }



    @Override
    protected void onStart() {
        super.onStart();
        listaCiudades();


        CmdOrden.getOrdenesPasajero(new OnOrdenesListener() {
            @Override
            public void nueva() {

            }


            @Override
            public void modificada(String idServicio , boolean cambioEstado) {

                if (cambioEstado){

                    Servicio servicio  = modelo.getOrdenById(idServicio);
                    if (servicio == null){
                        return;
                    }

                    android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                            BuscarPasajero.this);
                    alertDialogBuilder.setTitle("Estado de servicio");
                    alertDialogBuilder
                            .setMessage("El servicio " + servicio.cosecutivoOrden + " cambió de estado, se encuentra " + servicio.getEstadoLeible())
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    return;
                                }
                            });

                    // create alert dialog
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                }

            }


            @Override
            public void eliminada() {

            }
        });


    }


    @Override
    protected void onPause() {
        super.onPause();
        modelo.pararListeners();

    }




    /******************************** EfficientAdapter ************************************/

        public class EfficientAdapter extends BaseAdapter implements SectionIndexer, Filterable {

            private Filter filter;
            private String mSections = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            private LayoutInflater mInflater;
            @SuppressWarnings("unused")
            private String TAG = EfficientAdapter.class.getSimpleName();
            @SuppressWarnings("unused")
            private Context context;

            Modelo modelo = Modelo.getInstance();


            public EfficientAdapter(Context context) {
                mInflater = LayoutInflater.from(context);
                this.context = context;
                this.modelo.dataPasajerosE.addAll(pasajeroList);

            }


            @Override
            public View getView(final int position, View convertView, final ViewGroup parent) {

                EfficientAdapter.ViewHolder holder;

                if (convertView == null) {
                    convertView = mInflater.inflate(R.layout.main_list, parent, false);
                    holder = new EfficientAdapter.ViewHolder();
                    holder.textLine = (TextView) convertView.findViewById(R.id.textView_main_item);
                    holder.textDep = (TextView) convertView.findViewById(R.id.textView_main_dep);
                    holder.textSeperater = (TextView) convertView.findViewById(R.id.textView_main_seperater);
                    convertView.setTag(holder);
                }
                holder = (EfficientAdapter.ViewHolder) convertView.getTag();


                Collections.sort(modelo.dataPasajerosE, new Comparator<Pasajero>() {
                            @Override
                            public int compare(Pasajero p1, Pasajero p2) {
                                return p1.nombre.compareTo(p2.nombre);
                            }
                        }
                );

                Pasajero pas = (Pasajero) getItem(position);
                if (pas.nombre.length() <= 1) {
                    holder.textLine.setVisibility(View.GONE);
                    holder.textDep.setVisibility(View.GONE);
                    holder.textSeperater.setVisibility(View.VISIBLE);
                    holder.textSeperater.setText(pas.nombre.substring(0,1));


                } else {
                    holder.textLine.setVisibility(View.VISIBLE);
                    holder.textDep.setVisibility(View.VISIBLE);
                    holder.textSeperater.setVisibility(View.GONE);
                    holder.textLine.setText(pas.nombre + " " + pas.getApellido());
                    holder.textDep.setText(pas.getCelular());


                }

                return convertView;
            }

            @Override
            public int getCount() {
                // TODO Auto-generated method stub
                return modelo.dataPasajerosE.size();
            }

            @Override
            public Object getItem(int position) {


                return modelo.dataPasajerosE.get(position);
            }

            @Override
            public long getItemId(int position) {
                // TODO Auto-generated method stub
                return 0;
            }

            class ViewHolder {
                TextView textLine;
                TextView textDep;
                TextView textSeperater;

            }

            @Override
            public Filter getFilter() {
                if (filter == null)
                    filter = new EfficientAdapter.MangaNameFilter();
                return filter;
            }

            /************** sectionIndexer Overriding Functions **************/

            @Override
            public int getPositionForSection(int section) {
                // If there is no item for current section, previous section will be selected
                for (int i = section; i >= 0; i--) {
                    for (int j = 0; j < getCount(); j++) {
                        if (i == 0) {
                            // For numeric section
                            for (int k = 0; k <= 9; k++) {
                                if (match(String.valueOf(((Pasajero)getItem(j)).nombre.charAt(0)), String.valueOf(k)))
                                    return j;
                            }
                        } else {
                            if (match(String.valueOf(((Pasajero)getItem(j)).nombre.charAt(0)), String.valueOf(mSections.charAt(i))))
                                return j;
                        }
                    }
                }
                return 0;
            }

            @Override
            public int getSectionForPosition(int position) {
                return position;
            }

            @Override
            public Object[] getSections() {
                String[] sections = new String[mSections.length()];
                for (int i = 0; i < mSections.length(); i++)
                    sections[i] = String.valueOf(mSections.charAt(i));
                return sections;
            }

            /************** MangaNameFilter Class **************/
            private class MangaNameFilter extends Filter {

                final List<Pasajero> list = new ArrayList<>();

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    constraint = constraint.toString().toLowerCase();
                    FilterResults result = new FilterResults();

                    if (constraint != null && constraint.toString().length() > 0) {

                        ArrayList<Pasajero> allItems = null;
                        synchronized (this) {
                            allItems = new ArrayList<>(pasajeroList);
                        }

                        list.clear();

                        for (int i = 0; i < allItems.size(); i++) {
                            String item = allItems.get(i).nombre.toLowerCase();
                            if (item.startsWith(constraint + "")) {
                                list.add(allItems.get(i));
                            }
                        }

                        result.count = list.size();
                        result.values = list;

                    } else {
                        synchronized (this) {
                            list.clear();
                            for (int i = 0; i < pasajeroList.size(); i++) {
                                list.add(pasajeroList.get(i));
                            }

                            result.values = list;
                            result.count = list.size();
                        }
                    }
                    return result;
                }

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    modelo.dataPasajerosE = (ArrayList<Pasajero>) results.values;
                    notifyDataSetChanged();
                }
            }

        } //EfficientAdapter class ends

        /************** Function for sectionIndexer **************/

        public boolean match(String value, String keyword) {
            if (value == null || keyword == null)
                return false;
            if (keyword.length() > value.length())
                return false;

            int i = 0, j = 0;
            do {
                if (keyword.charAt(j) == value.charAt(i)) {
                    i++;
                    j++;
                } else if (j > 0)
                    break;
                else
                    i++;
            } while (i < value.length() && j < keyword.length());

            return (j == keyword.length()) ? true : false;
        }


//validacion a internet


        //validacion conexion internet
        protected Boolean estaConectado() {
            if (conectadoWifi()) {
                Log.v("wifi", "Tu Dispositivo tiene Conexion a Wifi.");
                return true;
            } else {
                if (conectadoRedMovil()) {
                    Log.v("Datos", "Tu Dispositivo tiene Conexion Movil.");
                    return true;
                } else {
                    showAlertSinInternet();
                    // Toast.makeText(getApplicationContext(),"Sin Conexión a Internet", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        }

        protected Boolean conectadoWifi() {
            ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                if (info != null) {
                    if (info.isConnected()) {
                        return true;
                    }
                }
            }
            return false;
        }

        protected Boolean conectadoRedMovil() {
            ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                if (info != null) {
                    if (info.isConnected()) {
                        return true;
                    }
                }
            }
            return false;
        }


        public void showAlertSinInternet() {

            android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                    context);

            // set title
            alertDialogBuilder.setTitle("Sin Internet");

            // set dialog message
            alertDialogBuilder
                    .setMessage("Sin Conexión a Internet")
                    .setCancelable(false)
                    .setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            Intent inte = new Intent(getBaseContext(), Splash.class);
                            startActivity(inte);
                        }
                    });

            // create alert dialog
            android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
        }
    }