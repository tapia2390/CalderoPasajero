package transcalderonpasajero.transcalderonpasajero.pasajeros;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ListView;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden.OnOrdenesListener;
import transcalderonpasajero.transcalderonpasajero.ListaCiudadRutas;
import transcalderonpasajero.transcalderonpasajero.MainActivity;
import transcalderonpasajero.transcalderonpasajero.R;
import transcalderonpasajero.transcalderonpasajero.servicio.ConfirmarServicio;
import transcalderonpasajero.transcalderonpasajero.servicio.listados.ListaServiciosAdapter;

public class AddPasajeros extends Activity {


    private ListView lisView;
    private ListaReportePasajerosAdapter mAdapter;
    Modelo modelo = Modelo.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pasajeros);


        if (savedInstanceState != null){

            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
            return;

        }

        mAdapter = new ListaReportePasajerosAdapter(this);
        lisView = findViewById(R.id.pasajeros_list_view);
        lisView.setAdapter(mAdapter);
        lisView.setEmptyView(findViewById(R.id.vacio));

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAdapter.notifyDataSetChanged();

        CmdOrden.getOrdenesPasajero(new OnOrdenesListener() {
            @Override
            public void nueva() {

            }


            @Override
            public void modificada(String idServicio , boolean cambioEstado) {

                if (cambioEstado){

                    Servicio servicio  = modelo.getOrdenById(idServicio);
                    if (servicio == null){
                        return;
                    }

                    android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                            AddPasajeros.this);
                    alertDialogBuilder.setTitle("Estado de servicio");
                    alertDialogBuilder
                            .setMessage("El servicio " + servicio.cosecutivoOrden + " cambió de estado, se encuentra " + servicio.getEstadoLeible())
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    return;
                                }
                            });

                    // create alert dialog
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                }

            }


            @Override
            public void eliminada() {

            }
        });


    }


    @Override
    protected void onPause() {
        super.onPause();
        modelo.pararListeners();

    }


    public void didTapAddEmpresarial(View view) {

        Intent intent = new Intent(getApplicationContext(), BuscarPasajero.class);
        intent.putExtra("TIPO","EMPRESARIAL");
        startActivity(intent);
    }

    public void didTapAddParticular(View view) {
        Intent intent = new Intent(getApplicationContext(), BuscarPasajero.class);
        intent.putExtra("TIPO","PARTICULAR");
        startActivity(intent);
    }
}
