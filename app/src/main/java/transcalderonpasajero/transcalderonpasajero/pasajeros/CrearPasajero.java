package transcalderonpasajero.transcalderonpasajero.pasajeros;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Pasajero;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden.OnOrdenesListener;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoPasajero;
import transcalderonpasajero.transcalderonpasajero.MainActivity;
import transcalderonpasajero.transcalderonpasajero.R;
import transcalderonpasajero.transcalderonpasajero.sistema.Utility;

public class CrearPasajero extends Activity {

    EditText nombre, celular, apellido;
    Button btnAceptar;
    Modelo modelo = Modelo.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_pasajero);

        nombre = findViewById(R.id.nombre);
        celular = findViewById(R.id.celular);
        apellido = findViewById(R.id.apellido);

        btnAceptar = findViewById(R.id.btnCrearPasajero);

        if (savedInstanceState != null){

            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
            return;

        }

        btnAceptar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (nombre.getText().toString().length() < 3) {
                    nombre.setError("Debes incluir el nombre");
                    nombre.requestFocus();
                    return;
                }

                if (celular.getText().toString().length() < 7) {
                    celular.setError("Debes incluir el celular");
                    celular.requestFocus();
                    return;
                }
                if (apellido.getText().toString().length() < 3) {
                    apellido.setError("Debes incluir el apellido");
                    apellido.requestFocus();
                    return;
                }

                Pasajero pasajero = new Pasajero();
                pasajero.uid = ComandoPasajero.getNewFirebaseId();
                pasajero.nombre = nombre.getText().toString();
                pasajero.setApellido(apellido.getText().toString());
                pasajero.setCelular(celular.getText().toString());

                ComandoPasajero.crearAmigo(pasajero);
                Modelo modelo = Modelo.getInstance();
                modelo.pasajero.amigos.add(pasajero);
                modelo.currentService.addEliminarPasajero("PARTICULAR", pasajero);
                finish();


            }
        });




    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }

    @Override
    protected void onStart() {
        super.onStart();



        CmdOrden.getOrdenesPasajero(new OnOrdenesListener() {
            @Override
            public void nueva() {

            }


            @Override
            public void modificada(String idServicio , boolean cambioEstado) {

                if (cambioEstado){

                    Servicio servicio  = modelo.getOrdenById(idServicio);
                    if (servicio == null){
                        return;
                    }

                    android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                            CrearPasajero.this);
                    alertDialogBuilder.setTitle("Estado de servicio");
                    alertDialogBuilder
                            .setMessage("El servicio " + servicio.cosecutivoOrden + " cambió de estado, se encuentra " + servicio.getEstadoLeible())
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    return;
                                }
                            });

                    // create alert dialog
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                }

            }


            @Override
            public void eliminada() {

            }
        });

    }


    @Override
    protected void onPause() {
        super.onPause();
        modelo.pararListeners();

    }


}
