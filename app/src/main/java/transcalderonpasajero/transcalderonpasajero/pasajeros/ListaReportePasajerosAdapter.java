package transcalderonpasajero.transcalderonpasajero.pasajeros;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Pasajero;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.R;
import transcalderonpasajero.transcalderonpasajero.sistema.Utility;

/**
 * Created by andres on 3/7/18.
 */

public class ListaReportePasajerosAdapter extends BaseAdapter {


    private Context mContext;
    private LayoutInflater mInflater;
    Modelo modelc = Modelo.getInstance();

    TextView nombre, celular, separador;
    ImageView estado;



    public ListaReportePasajerosAdapter(Context context){

        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        return modelc.currentService.getFullPasajeros().size();
    }

    @Override
    public Object getItem(int i) {

        return modelc.currentService.getFullPasajeros().get(i);

    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int i, View view, ViewGroup parent) {

        View rowView = mInflater.inflate(R.layout.main_list_sin_separador, parent, false);
        nombre = rowView.findViewById(R.id.textView_main_item);
        celular = rowView.findViewById(R.id.textView_main_dep);
        separador =  rowView.findViewById(R.id.textView_main_seperater);



        Pasajero pasajero = modelc.currentService.getFullPasajeros().get(i);
        nombre.setText( pasajero.nombre + " " + pasajero.getApellido());
        celular.setText( pasajero.getCelular());
        separador.setVisibility(View.GONE);


        return rowView;

    }
}
