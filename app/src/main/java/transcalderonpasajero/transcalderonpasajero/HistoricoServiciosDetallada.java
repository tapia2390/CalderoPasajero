package transcalderonpasajero.transcalderonpasajero;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.AdapterView;
import java.util.Date;

import transcalderonpasajero.transcalderonpasajero.Adapter.HistorialAdapterDetallada;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.Comandos.ComandoOrdenesPasajeroUltimosDiez;

public class HistoricoServiciosDetallada extends Activity implements HistorialAdapterDetallada.AdapterCallback, ComandoOrdenesPasajeroUltimosDiez.OnOrdenesPasajeroHistorialChangeListener {

    private HistorialAdapterDetallada mAdapter;
    ListView lv;
    public ProgressBar progressBar;
    Modelo modelo  = Modelo.getInstance();
    TextView fecha_historico;
    TextView empresa_historico;
    String fechaHistorco,razonSocial;
    Date date;
    Servicio servicio;
    String idServicio;
    TextView sindatos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_historico_servicios_detallada);


        lv = (ListView) findViewById(R.id.lv);
        fecha_historico = (TextView)findViewById(R.id.fecha_historico);
        progressBar = (ProgressBar)findViewById(R.id.progressBar3);
        sindatos= (TextView) findViewById(R.id.sindatos);

        Bundle bundle = getIntent().getExtras();
        fechaHistorco = bundle.getString("fecha");

        servicio = modelo.getOrdenHistorial(idServicio);
        ComandoOrdenesPasajeroUltimosDiez comandoHistorial = new ComandoOrdenesPasajeroUltimosDiez(this);
        comandoHistorial.getOrdenesPasajeroHistorial();


        mostrarMes(fechaHistorco);




        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getApplicationContext(),""+parent[position]+"-"+position, Toast.LENGTH_SHORT);
                //se captura la posicion
                Servicio ordenH = modelo.filtrohistorialmes.get(position);

                Intent intent = new Intent(getApplicationContext(), InformacionDelServicioDetallada.class);
                intent.putExtra("id", "" +ordenH.getId());// se envia el id de la orden segun la posicion
                intent.putExtra("fecha", "" +fechaHistorco);
                startActivity(intent);
                //finish();
            }
        });


        timerload();
    }

    private void displayHistorial(){
        mAdapter = new HistorialAdapterDetallada(this,this,fechaHistorco);
        lv.setAdapter(mAdapter);
    }




    @Override
    public void cargoUnaOrdenesPasajeroHistorial() {
        // mAdapter.notifyDataSetChanged();
        displayHistorial();
        ocultartexto();

    }

    @Override
    public void cargoHisTorialP() {

    }

    public void mostrarMes(String fechaHistorco){
        String[] separated = fechaHistorco.split("/");

        if (separated[0].equals("01")){
            fecha_historico.setText("Enero de "+separated[1]);
        }
        else if (separated[0].equals("02")){
            fecha_historico.setText("Febrero de "+separated[1]);
        }
        else if (separated[0].equals("03")){
            fecha_historico.setText("Marzo de "+separated[1]);
        }
        else if (separated[0].equals("04")){
            fecha_historico.setText("Abril de "+separated[1]);
        }
        else if (separated[0].equals("05")){
            fecha_historico.setText("Mayo de "+separated[1]);
        }
        else if (separated[0].equals("06")){
            fecha_historico.setText("Junio de "+separated[1]);
        }
        else if (separated[0].equals("07")){
            fecha_historico.setText("Julio de "+separated[1]);
        }
        else if (separated[0].equals("08")){
            fecha_historico.setText("Agosto de "+separated[1]);
        }
        else if (separated[0].equals("09")){
            fecha_historico.setText("Septiembre de "+separated[1]);
        }
        else if (separated[0].equals("10")){
            fecha_historico.setText("Octubre de "+separated[1]);
        }
        else if (separated[0].equals("11")){
            fecha_historico.setText("Noviembre de "+separated[1]);
        }
        else if (separated[0].equals("12")){
            fecha_historico.setText("Diciembre de "+separated[1]);
        }
    }




    public void timerload(){

        Thread  thread = new Thread(){
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(2000);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(modelo.filtrohistorialmes.size() < 1){
                                    progressBar.setVisibility(View.INVISIBLE);
                                    sindatos.setVisibility(View.VISIBLE);
                                    return;
                                }else{
                                    progressBar.setVisibility(View.INVISIBLE);
                                }
                            }
                        });

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            };
        };
        thread.start();
    }

    public void ocultartexto(){

        Thread  thread = new Thread(){
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(500);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if(modelo.filtrohistorialmes.size() < 1){
                                    sindatos.setVisibility(View.VISIBLE);
                                    return;
                                }
                            }
                        });

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            };
        };
        thread.start();
    }



    //ciclo de vida


    @Override
    protected void onStart() {
        super.onStart();
        modelo.appcerradaAbierta = false;
    }




    @Override
    protected void onStop() {
        super.onStop();
        Log.wtf("Ciclo de vida", "onStop");
        Start_Service();
    }

    /* mi Servicios */
    public void Start_Service(){
        modelo.appcerradaAbierta  = true;
        Intent intent = new Intent(getApplicationContext(), MiService.class);
        intent.putExtra("primerplano", false);
        startService(intent);
    }

    /* mi MiServiceForeground */
    private void MiServiceBoot() {

        Intent intent = new Intent(getApplicationContext(), ServiceBoot.class);
        intent.putExtra("primerplano", false);
        startService(intent);
    }

    public void Stop_Service(){
        modelo.appcerradaAbierta  = false;
        stopService(new Intent(getApplicationContext(), MiService.class));
    }

}
