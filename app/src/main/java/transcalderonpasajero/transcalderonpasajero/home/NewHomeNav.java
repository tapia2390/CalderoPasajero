package transcalderonpasajero.transcalderonpasajero.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo.OnModelListener;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden.OnOrdenesListener;
import transcalderonpasajero.transcalderonpasajero.Historico;
import transcalderonpasajero.transcalderonpasajero.ListaCiudadOrigenDestino;
import transcalderonpasajero.transcalderonpasajero.MainActivity;
import transcalderonpasajero.transcalderonpasajero.Perfil;
import transcalderonpasajero.transcalderonpasajero.R;
import transcalderonpasajero.transcalderonpasajero.Servicios;
import transcalderonpasajero.transcalderonpasajero.Splash;

public class NewHomeNav extends AppCompatActivity {

    private TextView mTextMessage, sinCalificar;

    private Fragment fragment, fragServicios;
    private FragmentManager fragmentManager;
    public Modelo model = Modelo.getInstance();


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_servicio:
                    fragment = fragServicios;
                    break;
                case R.id.navigation_miPerfil:
                    fragment = new Perfil();
                    break;
                case R.id.navigation_historial:
                   fragment = new Historico();
                    break;
            }
            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.main_container, fragment).commit();
            return true;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_home_nav);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        fragmentManager = getSupportFragmentManager();


        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);

        fragServicios = new Servicios();

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_servicio);


        if (savedInstanceState != null){

            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;

        }

        BottomNavigationMenuView bottomNavigationMenuView =
                (BottomNavigationMenuView) navigation.getChildAt(0);
        View v = bottomNavigationMenuView.getChildAt(2);
        BottomNavigationItemView itemView = (BottomNavigationItemView) v;

        View badge = LayoutInflater.from(this)
                .inflate(R.layout.badge, bottomNavigationMenuView, false);

        itemView.addView(badge);

        sinCalificar = badge.findViewById(R.id.sinCalificar);
        sinCalificar.setText("4");

        pintarSinCalificar();







    }


    @Override
    protected void onStart() {
        super.onStart();

        CmdOrden.getOrdenesHistorialPasajero(new OnOrdenesListener() {
            @Override
            public void nueva() {
                pintarSinCalificar();
            }

            @Override
            public void modificada(String idServicio, boolean cambioEstado) {
                pintarSinCalificar();
            }

            @Override
            public void eliminada() {
                pintarSinCalificar();
            }
        });
    }

    public void pintarSinCalificar(){
        int i = model.sinCalificar();
        if (i == 0){
            sinCalificar.setVisibility(View.GONE);
        }
        else {
            sinCalificar.setVisibility(View.VISIBLE);
            sinCalificar.setText("" + i);
        }

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }



}
