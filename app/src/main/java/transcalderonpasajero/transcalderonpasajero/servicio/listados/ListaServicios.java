package transcalderonpasajero.transcalderonpasajero.servicio.listados;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdNoti;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdNoti.OnCmdNotiListener;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden.OnOrdenesListener;
import transcalderonpasajero.transcalderonpasajero.InformacionDelServicioDetallada;
import transcalderonpasajero.transcalderonpasajero.MainActivity;
import transcalderonpasajero.transcalderonpasajero.R;
import transcalderonpasajero.transcalderonpasajero.Servicios;
import transcalderonpasajero.transcalderonpasajero.Splash;
import transcalderonpasajero.transcalderonpasajero.servicio.AyudaServicios;
import transcalderonpasajero.transcalderonpasajero.servicio.ConfirmarServicio;


public class ListaServicios extends Activity {


    private ListView lisView;
    private ListaServiciosAdapter mAdapter;
    Modelo model = Modelo.getInstance();
    FrameLayout historial;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_servicios);


        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }


        historial = findViewById(R.id.historial);
        mAdapter = new ListaServiciosAdapter(this);
        lisView = findViewById(R.id.servicios_list_view);
        lisView.setAdapter(mAdapter);
        lisView.setEmptyView(findViewById(R.id.vacio));



        lisView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent intent = new Intent(getApplicationContext(), ConfirmarServicio.class);
                intent.putExtra("IDSERVICIO",model.servicios.get(i).id);
                startActivity(intent);

            }
        });


        historial.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), AyudaServicios.class);
                startActivity(i);
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
            }
        });



    }




    @Override
    protected void onStart() {
        super.onStart();

        mAdapter.notifyDataSetChanged();

        CmdOrden.getOrdenesPasajero(new OnOrdenesListener() {
            @Override
            public void nueva() {
                mAdapter.notifyDataSetChanged();

            }


            @Override
            public void modificada(final String idServicio , boolean cambioEstado) {

                if (cambioEstado){

                    Servicio servicio  = model.getOrdenById(idServicio);
                    if (servicio == null){
                        return;
                    }

                    if (servicio.estado.equals("Finalizado")){
                        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                                ListaServicios.this);
                        alertDialogBuilder.setTitle("Servicio Finalizado");
                        alertDialogBuilder
                                .setMessage("El servicio " + servicio.cosecutivoOrden + " ha terminado, por favor dale una calificación para poder mejorar el servicio")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent i = new Intent(ListaServicios.this, InformacionDelServicioDetallada.class);
                                        i.putExtra("id",idServicio);
                                        startActivity(i);
                                        return;
                                    }
                                });

                        // create alert dialog
                        android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();

                        return;

                    }


                    android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                            ListaServicios.this);
                    alertDialogBuilder.setTitle("Estado de servicio");
                    alertDialogBuilder
                            .setMessage("El servicio " + servicio.cosecutivoOrden + " cambió de estado, se encuentra " + servicio.getEstadoLeible())
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    return;
                                }
                            });

                    // create alert dialog
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                }
                mAdapter.notifyDataSetChanged();

            }


            @Override
            public void eliminada() {
                mAdapter.notifyDataSetChanged();

            }
        });


    }

    @Override
    protected void onPause() {
        super.onPause();
        model.pararListeners();


    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }



}
