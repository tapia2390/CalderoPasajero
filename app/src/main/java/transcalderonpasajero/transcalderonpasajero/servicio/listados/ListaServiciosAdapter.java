package transcalderonpasajero.transcalderonpasajero.servicio.listados;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;



import java.util.ArrayList;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.R;
import transcalderonpasajero.transcalderonpasajero.sistema.Utility;

/**
 * Created by andres on 3/7/18.
 */

public class ListaServiciosAdapter extends BaseAdapter {


    private Context mContext;
    private LayoutInflater mInflater;
    Modelo modelc = Modelo.getInstance();
    ArrayList<Servicio> servicios;

    TextView origen, destino, fechaOrigen, fechaDestino, horaOrigen, consecutivo;
    ImageView estado;



    public ListaServiciosAdapter(Context context){

        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        servicios = modelc.servicios;

    }


    @Override
    public int getCount() {
        return servicios.size();
    }

    @Override
    public Object getItem(int i) {

        return servicios.get(i);

    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int i, View view, ViewGroup parent) {

        View rowView = mInflater.inflate(R.layout.item_lista_servicio, parent, false);
        origen =  rowView.findViewById(R.id.origen);
        destino =  rowView.findViewById(R.id.destino);
        fechaDestino =  rowView.findViewById(R.id.fechaDestino);
        fechaOrigen  =  rowView.findViewById(R.id.fechaOrigen);
        horaOrigen = rowView.findViewById(R.id.horaOrigen);
        consecutivo = rowView.findViewById(R.id.superestado);
        estado = rowView.findViewById(R.id.img_estados);


        Servicio serv = servicios.get(i);
        origen.setText( serv.origen);
        destino.setText( serv.destino);
        fechaDestino.setText( Utility.getFotmatoFechaDestino(Utility.convertStringToDate(serv.fechaEnOrigen)));
        fechaOrigen.setText( serv.fechaEnOrigen);
        horaOrigen.setText( serv.horaEnOrigen);




        if (serv.estado.equals("SinConfirmar") || serv.estado.equals("NoAsignado") || serv.estado.equals("Cotizar")){
            estado.setImageResource(R.drawable.estado_sin_confirmar_i5);
            consecutivo.setText(serv.cosecutivoOrden + "  " + "No asignado");
        }
        if (serv.estado.equals("Asignado") ){
            estado.setImageResource(R.drawable.estado_confirmado_i5);
            consecutivo.setText(serv.cosecutivoOrden + "  " + "Confirmado");
        }

        if (serv.estado.equals("En Camino") ){
            estado.setImageResource(R.drawable.estado_en_camino_i5);
            consecutivo.setText(serv.cosecutivoOrden + "  " + "En camino");
        }

        if (serv.estado.equals("Transportando") ){
            estado.setImageResource(R.drawable.estado_transportando_i5);
            consecutivo.setText(serv.cosecutivoOrden + "  " + "Transportando");
        }

        if (serv.estado.equals("Finalizado") ){
            estado.setImageResource(R.drawable.estado_finalizado_i5);
            consecutivo.setText(serv.cosecutivoOrden + "  " + "Finalizado");
        }
        if ((serv.estado.equals("Anulado")) || (serv.estado.equals("Cancelado")  )){
            estado.setImageResource(R.drawable.estado_cancelado_i5);
            consecutivo.setText(serv.cosecutivoOrden + "  " + "Cancelado");
        }



        return rowView;
    }
}
