package transcalderonpasajero.transcalderonpasajero.servicio;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden.OnOrdenesListener;
import transcalderonpasajero.transcalderonpasajero.ListaCiudadRutas;
import transcalderonpasajero.transcalderonpasajero.MainActivity;
import transcalderonpasajero.transcalderonpasajero.R;
import transcalderonpasajero.transcalderonpasajero.Splash;
import transcalderonpasajero.transcalderonpasajero.pasajeros.AddPasajeros;

public class OtrasOpciones extends Activity {

    ImageView imgParadas, imgVehiculo, imgPasajeros, imgObservaciones;

    TextView  elimParadas, elimVehiculo, elimPasajeros, elimObservaciones;
    TextView  resParadas, resVehiculo, resPasajeros, resObservaciones;

    Modelo modelo = Modelo.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otras_opciones);

        imgParadas = findViewById(R.id.imgParadas);
        imgVehiculo = findViewById(R.id.imgVehiculo);
        imgPasajeros = findViewById(R.id.imgPasajeros);
        imgObservaciones = findViewById(R.id.imgObservaciones);

        elimParadas = findViewById(R.id.elimParados);
        elimVehiculo = findViewById(R.id.elimVehiculo);
        elimPasajeros = findViewById(R.id.elimPasajeros);
        elimObservaciones = findViewById(R.id.elimObservaciones);

        resParadas = findViewById(R.id.resParadas);
        resVehiculo = findViewById(R.id.resVehiculo);
        resPasajeros = findViewById(R.id.resPasajeros);
        resObservaciones = findViewById(R.id.resObservaciones);

        if (savedInstanceState != null){

            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;

        }



    }




    @Override
    protected void onStart() {
        super.onStart();

        CmdOrden.getOrdenesPasajero(new OnOrdenesListener() {
            @Override
            public void nueva() {

            }


            @Override
            public void modificada(String idServicio , boolean cambioEstado) {

                if (cambioEstado){

                    Servicio servicio  = modelo.getOrdenById(idServicio);
                    if (servicio == null){
                        return;
                    }

                    android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                            OtrasOpciones.this);
                    alertDialogBuilder.setTitle("Estado de servicio");
                    alertDialogBuilder
                            .setMessage("El servicio " + servicio.cosecutivoOrden + " cambió de estado, se encuentra " + servicio.getEstadoLeible())
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    return;
                                }
                            });

                    // create alert dialog
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                }


            }


            @Override
            public void eliminada() {


            }
        });

        if (modelo.currentService.hasSetParadasAdicionales() ){
            imgParadas.setImageResource(R.drawable.img_paradas_verde);
            elimParadas.setVisibility(View.VISIBLE);
            resParadas.setVisibility(View.VISIBLE);
            resParadas.setText(modelo.currentService.getRuta());
        }else {
            imgParadas.setImageResource(R.drawable.img_paradas_gris);
            elimParadas.setVisibility(View.GONE);
            resParadas.setVisibility(View.GONE);

        }

        if(modelo.currentService.hasSetVehiculo()){
            imgVehiculo.setImageResource(R.drawable.img_vehiculo_verde);
            elimVehiculo.setVisibility(View.VISIBLE);
            resVehiculo.setVisibility(View.VISIBLE);
            resVehiculo.setText(modelo.currentService.tipoVehiculo);
        }
        else{
            imgVehiculo.setImageResource(R.drawable.img_vehiculo_gris);
            elimVehiculo.setVisibility(View.GONE);
            resVehiculo.setVisibility(View.GONE);
        }

        if (modelo.currentService.hasPasajeros() ){
            imgPasajeros.setImageResource(R.drawable.img_pasajeros_verde);
            elimPasajeros.setVisibility(View.VISIBLE);
            resPasajeros.setVisibility(View.VISIBLE);
            resPasajeros.setText(modelo.currentService.getListaPasajerosCompleta());
        }else {
            imgPasajeros.setImageResource(R.drawable.img_pasajeros_gris);
            elimPasajeros.setVisibility(View.GONE);
            resPasajeros.setVisibility(View.GONE);
        }


        if(modelo.currentService.hasObservaciones()){
            imgObservaciones.setImageResource(R.drawable.img_comentarios_verde);
            elimObservaciones.setVisibility(View.VISIBLE);
            resObservaciones.setVisibility(View.VISIBLE);
            resObservaciones.setText(modelo.currentService.observaciones);
        }else{
            imgObservaciones.setImageResource(R.drawable.img_comentarios_gris);
            elimObservaciones.setVisibility(View.GONE);
            resObservaciones.setVisibility(View.GONE);
        }


    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }


    public void didTapParadas(View view) {

        Intent intent =  new Intent(getApplicationContext(),ListaCiudadRutas.class);
        startActivity(intent);


    }

    public void didTapVehiculo(View view) {
        Intent intent =  new Intent(getApplicationContext(),VehiculosActivity.class);
        startActivity(intent);
    }

    public void eliminarVehiculo(View view) {
        modelo.currentService.tipoVehiculo ="Sin preferencia";
        onStart();
    }


    public void eliminarObservaciones(View view) {
        modelo.currentService.observaciones =null;
        onStart();
    }


    public void didTapPasajeros(View view) {

        Intent intent =  new Intent(getApplicationContext(),AddPasajeros.class);
        startActivity(intent);
    }

    public void didTapObservaciones(View view) {

        Intent intent =  new Intent(getApplicationContext(),SetObservaciones.class);
        startActivity(intent);
    }


    public void eliminarPasajeros(View view) {
        modelo.currentService.eliminarPasajeros();
        onStart();
    }


    public void eliminarParadas(View view) {
        modelo.currentService.eliminarTrayectos();
        onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        modelo.pararListeners();

    }


}
