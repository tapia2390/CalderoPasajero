package transcalderonpasajero.transcalderonpasajero.servicio;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden.OnOrdenesListener;
import transcalderonpasajero.transcalderonpasajero.MainActivity;
import transcalderonpasajero.transcalderonpasajero.R;
import transcalderonpasajero.transcalderonpasajero.Splash;

public class Duracion extends Activity {

    LinearLayout linIndefinido, linDias, linHoras;
    SwitchCompat switchIndefinido;
    TextView dias, horas;

    Modelo modelo = Modelo.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_duracion);

        linIndefinido = findViewById(R.id.linearIndefinido);
        linDias = findViewById(R.id.linearDias);
        linHoras = findViewById(R.id.linearHoras);
        switchIndefinido = findViewById(R.id.swichIndefinido);
        dias = findViewById(R.id.dias);
        horas = findViewById(R.id.horas);


        if (savedInstanceState != null){
            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;
        }


        switchIndefinido.setChecked(modelo.currentService.indefinido);

        if (modelo.currentService.horasServicio.equals("")){
            horas.setText("--");
            dias.setText(modelo.currentService.diasServicio);
        }
        else {
            dias.setText("--");
            horas.setText(modelo.currentService.horasServicio);
        }

        if (modelo.currentService.indefinido) {
            horas.setText("--");
            dias.setText("--");
        }



        switchIndefinido.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                //switchIndefinido.setChecked(!switchIndefinido.isChecked());
                if (b){
                    horas.setText("--");
                    dias.setText("--");
                    modelo.currentService.indefinido = true;
                    modelo.currentService.horasServicio= "";
                    modelo.currentService.diasServicio= "";
                    CmdOrden.saveCurrentService();
                }
            }
        });

        linIndefinido.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                switchIndefinido.setChecked(!switchIndefinido.isChecked());
                if (switchIndefinido.isChecked()){
                    horas.setText("--");
                    dias.setText("--");
                    modelo.currentService.indefinido = true;
                }
            }
        });

        linDias.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                showOpcionesDias();
            }
        });

        linHoras.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                showOpcionesHoras();

            }
        });



    }

    private void showOpcionesDias() {
        final String[] items = {"1","2","3", "4", "5", "6","7","8", "9","10"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Número de días");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                String tipo =  items[item];
                dias.setText(tipo);
                horas.setText("--");
                switchIndefinido.setChecked(false);

                modelo.currentService.indefinido = false;
                modelo.currentService.diasServicio = tipo;
                modelo.currentService.horasServicio= "";
                CmdOrden.saveCurrentService();

            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }



    private void showOpcionesHoras() {
        final String[] items = {"1","2","3", "4", "5", "6","7","8", "9","10","11","12","13", "14", "15", "16","17","18", "19","20", "21","22","23"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Número de horas");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                String tipo =  items[item];
                horas.setText(tipo);
                dias.setText("--");
                switchIndefinido.setChecked(false);
                modelo.currentService.indefinido = false;
                modelo.currentService.diasServicio = "";
                modelo.currentService.horasServicio= tipo;
                CmdOrden.saveCurrentService();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    @Override
    protected void onStart() {
        super.onStart();

        CmdOrden.getOrdenesPasajero(new OnOrdenesListener() {
            @Override
            public void nueva() {

            }


            @Override
            public void modificada(String idServicio , boolean cambioEstado) {

                if (cambioEstado){

                    Servicio servicio  = modelo.getOrdenById(idServicio);
                    if (servicio == null){
                        return;
                    }

                    android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                            Duracion.this);
                    alertDialogBuilder.setTitle("Estado de servicio");
                    alertDialogBuilder
                            .setMessage("El servicio " + servicio.cosecutivoOrden + " cambió de estado, se encuentra " + servicio.getEstadoLeible())
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    return;
                                }
                            });

                    // create alert dialog
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                }


            }


            @Override
            public void eliminada() {


            }
        });


    }

    @Override
    protected void onPause() {
        super.onPause();
        modelo.pararListeners();


    }


}
