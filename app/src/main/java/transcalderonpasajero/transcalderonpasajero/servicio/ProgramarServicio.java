package transcalderonpasajero.transcalderonpasajero.servicio;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden.OnOrdenesListener;
import transcalderonpasajero.transcalderonpasajero.MainActivity;
import transcalderonpasajero.transcalderonpasajero.R;
import transcalderonpasajero.transcalderonpasajero.Servicios;
import transcalderonpasajero.transcalderonpasajero.Splash;
import transcalderonpasajero.transcalderonpasajero.sistema.Utility;

public class ProgramarServicio extends Activity {


    EditText fechaServcio, horaServicio;
    TextView duracion;

    Date fecha;
    private int mHour, mMinute;
    Modelo modelc = Modelo.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_programar_servicio);


        fechaServcio = (EditText) findViewById(R.id.fecha);
        horaServicio = (EditText) findViewById(R.id.hora);
        fechaServcio.setInputType(InputType.TYPE_NULL);
        horaServicio.setInputType(InputType.TYPE_NULL);
        duracion = (TextView) findViewById((R.id.duracion));


        duracion.setText("El tiempo de respuesta del servicio es de:  " + modelc.empresa.horasRespuesta  + " horas");

        if (savedInstanceState != null){

            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;

        }

    }


    @Override
    protected void onStart() {
        super.onStart();

        CmdOrden.getOrdenesPasajero(new OnOrdenesListener() {
            @Override
            public void nueva() {

            }


            @Override
            public void modificada(String idServicio , boolean cambioEstado) {

                if (cambioEstado){

                    Servicio servicio  = modelc.getOrdenById(idServicio);
                    if (servicio == null){
                        return;
                    }

                    android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                            ProgramarServicio.this);
                    alertDialogBuilder.setTitle("Estado de servicio");
                    alertDialogBuilder
                            .setMessage("El servicio " + servicio.cosecutivoOrden + " cambió de estado, se encuentra " + servicio.getEstadoLeible())
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    return;
                                }
                            });

                    // create alert dialog
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                }

            }


            @Override
            public void eliminada() {


            }
        });


    }


    @Override
    protected void onPause() {
        super.onPause();
        modelc.pararListeners();

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }

    public void didTapFecha(View v){

        fechaServcio.setError(null);

        final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Calendar newCalendar =  Calendar.getInstance();

        DatePickerDialog StartTime = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fecha = newDate.getTime();
                fechaServcio.setText(dateFormat.format(newDate.getTime()));
                horaServicio.requestFocus();


            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        StartTime.setTitle("Fecha del servicio");
        StartTime.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis());
        StartTime.show();

    }


    public void didTapHora(View v) {

        horaServicio.setError(null);
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY );
        mHour = mHour + modelc.empresa.horasRespuesta;
        mMinute = c.get(Calendar.MINUTE);
        mMinute = mMinute + 1;

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        String hora, minutos;

                        if (hourOfDay == 0) {
                            hora = "12";
                        }
                        else if (hourOfDay == 13) {
                            hora = "1";
                        }else if (hourOfDay == 14){
                            hora = "2";
                        }
                        else if (hourOfDay == 15){
                            hora = "3";
                        }
                        else if (hourOfDay == 16){
                            hora = "4";
                        }
                        else if (hourOfDay == 17){
                            hora = "5";
                        }
                        else if (hourOfDay == 18){
                            hora = "6";
                        }
                        else if (hourOfDay == 19){
                            hora = "7";
                        }
                        else if (hourOfDay == 20){
                            hora = "8";
                        }
                        else if (hourOfDay == 21){
                            hora = "9";
                        }
                        else if (hourOfDay == 22){
                            hora = "10";
                        }
                        else if (hourOfDay == 23){
                            hora = "11";
                        }
                        else if (hourOfDay == 24){
                            hora = "12";
                        }
                        else {
                            hora = "" + hourOfDay;
                        }


                        if (minute < 10 ){
                            minutos = "0" + minute;
                        }else{
                            minutos = "" + minute;
                        }

                        if (hourOfDay > 11) {
                            horaServicio.setText(hora + ":" + minutos + " PM");
                        }
                        else {
                            horaServicio.setText(hora + ":" + minutos + " AM");
                        }

                    }
                }, mHour, mMinute, false);

        timePickerDialog.show();

    }

    public void didTapAceptar(View v) {


        modelc.currentService.fechaEnOrigen = fechaServcio.getText().toString();
        modelc.currentService.horaEnOrigen  = horaServicio.getText().toString();

        if (modelc.currentService.fechaEnOrigen.length() < 5 ) {
            fechaServcio.setError("Debes ingresar una fecha");
            return;
        }

        if (modelc.currentService.horaEnOrigen.length() < 3) {
            horaServicio.setError("Debes ingresar la hora");
            return;
        }

        String fechayHora =  fechaServcio.getText().toString() + " " +  horaServicio.getText().toString();

        Date fechaPropuesta;
        try {
            fechaPropuesta = modelc.dfull.parse(fechayHora);

        } catch (Exception e) {

            try {
                fechaPropuesta = modelc.dfull2.parse(fechayHora);
            }catch (Exception e2){
                fechaPropuesta = new Date();
            }
        }

        Date fechaMinima = Utility.addHorasAFechaActual(modelc.empresa.horasRespuesta);

        if (fechaPropuesta.before(fechaMinima)) {
            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                    ProgramarServicio.this);
            alertDialogBuilder.setTitle("Los sentimos");
            alertDialogBuilder
                    .setMessage("La fecha y hora no puede ser menor a la fecha y hora  actual más " + modelc.empresa.horasRespuesta + " horas")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            return;
                        }
                    });

            // create alert dialog
            android.app.AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
            return;
        }




        Intent i = new Intent(getApplicationContext(), ConfirmarServicio.class);
        i.putExtra("IDSERVICIO","CURRENT");
        startActivity(i);


    }


}
