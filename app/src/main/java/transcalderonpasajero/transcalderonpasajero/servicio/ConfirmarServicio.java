package transcalderonpasajero.transcalderonpasajero.servicio;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;

import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden.OnOrdenesListener;
import transcalderonpasajero.transcalderonpasajero.R;
import transcalderonpasajero.transcalderonpasajero.home.NewHomeNav;

public class ConfirmarServicio extends Activity {


    TextView origen, destino, dirOrigen, dirDestino, paradas, fecha, pasajeros, vehiculo, chofer;
    String idServicio;
    Servicio serv;
    Button btnContinuar;
    LinearLayout panelConductor;

    Modelo modelo = Modelo.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmar_servicio);

        origen  = findViewById(R.id.origen);
        destino = findViewById(R.id.destino);
        dirOrigen = findViewById(R.id.dirOrigen);
        dirDestino = findViewById(R.id.dirDestino);
        paradas = findViewById(R.id.paradas);
        fecha = findViewById(R.id.fecha);
        pasajeros = findViewById(R.id.pasajeros);
        vehiculo = findViewById(R.id.vehiculo);
        btnContinuar = findViewById(R.id.btnContinuar);
        chofer = findViewById(R.id.chofer);
        panelConductor = findViewById(R.id.panelConductor);


        idServicio = getIntent().getStringExtra("IDSERVICIO");

        if (idServicio.equals("CURRENT")){
            serv = modelo.currentService;
        } else {
           serv =  modelo.getOrdenById(idServicio);
        }


        origen.setText(serv.origen);
        destino.setText(serv.destino);
        dirOrigen.setText(serv.direccionOrigen);

        fecha.setText("Recoger " + serv.fechaEnOrigen + " " +  serv.horaEnOrigen);
        chofer.setText(serv.conductor.nombre + " " + serv.conductor.apellido);
        vehiculo.setText("Vehículo: " + serv.tipoVehiculo);

        String pasajerosTxt =  serv.getListaPasajerosCompleta();
        if (pasajerosTxt.equals("")){
            pasajeros.setText("Pasajeros adicionales: Ninguno");
        }
        else {
            pasajeros.setText("Pasajeros adicionales: \n    " + serv.getListaPasajerosCompleta());
        }


        String paradasTxt = serv.getRuta();
        if (paradasTxt.equals("")){
            paradas.setText("Paradas: Ninguna");
        }else {
            paradas.setText("Paradas:\n" +     serv.getRuta());
        }


        if (serv.getDuracionCadena().equals("")) {
            dirDestino.setText(serv.direccionDestino);
        }else {
           dirDestino.setText(serv.getDuracionCadena());
        }


        if (!idServicio.equals("CURRENT")){
            btnContinuar.setVisibility(View.GONE);
        }

        if (!serv.hasConductor()){
            panelConductor.setVisibility(View.GONE);

        }


    }



    public void didTapAceptar(View view) {

        CmdOrden.crearOrden();
        CmdOrden.deleteCurrenteService();


        if (modelo.pasajero.categoria.equals("B")){
            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                    ConfirmarServicio.this);
            alertDialogBuilder.setTitle("Solicitud recibida");
            alertDialogBuilder
                    .setMessage("El coordinador de transporte de su empresa validará la información enviada. Te llegará una notificación con la confirmación de tu servicio")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //modelo.currentService = new Servicio(); // limpiamos el servicio

                            modelo.currentService.reset();

                            Intent i = new Intent(getApplicationContext(), NewHomeNav.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.putExtra("vistaPosicion", "dos");
                            startActivity(i);
                            finish();

                        }
                    });

            // create alert dialog
            android.app.AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();


        }

        else {
            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                    ConfirmarServicio.this);
            alertDialogBuilder.setTitle("Solicitud enviada");
            alertDialogBuilder
                    .setMessage("El servicio que has requerido será confirmado en unos instantes")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            //modelo.currentService = new Servicio(); // limpiamos el servicio
                            modelo.currentService.reset();

                            Intent i = new Intent(getApplicationContext(), NewHomeNav.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.putExtra("vistaPosicion", "dos");
                            startActivity(i);
                            finish();

                        }
                    });

            // create alert dialog
            android.app.AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();

        }




    }

    public void didTapLlamar(View view) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + serv.conductor.celular));
        startActivity(intent);

    }

    @Override
    protected void onStart() {
        super.onStart();

        CmdOrden.getOrdenesPasajero(new OnOrdenesListener() {
            @Override
            public void nueva() {

            }


            @Override
            public void modificada(String idServicio , boolean cambioEstado) {

                if (cambioEstado){

                    Servicio servicio  = modelo.getOrdenById(idServicio);
                    if (servicio == null){
                        return;
                    }

                    android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                            ConfirmarServicio.this);
                    alertDialogBuilder.setTitle("Estado de servicio");
                    alertDialogBuilder
                            .setMessage("El servicio " + servicio.cosecutivoOrden + " cambió de estado, se encuentra " + servicio.getEstadoLeible())
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    return;
                                }
                            });

                    // create alert dialog
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                }

            }


            @Override
            public void eliminada() {


            }
        });


    }


    @Override
    protected void onPause() {
        super.onPause();
        modelo.pararListeners();

    }
}
