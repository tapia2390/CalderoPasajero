package transcalderonpasajero.transcalderonpasajero.servicio;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Toast;

import transcalderonpasajero.transcalderonpasajero.Adapter.VehiculosAdapter;
import transcalderonpasajero.transcalderonpasajero.Clases.HorizontalListView;
import transcalderonpasajero.transcalderonpasajero.Clases.Modelo;
import transcalderonpasajero.transcalderonpasajero.Clases.Servicio;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden;
import transcalderonpasajero.transcalderonpasajero.Comandos.CmdOrden.OnOrdenesListener;
import transcalderonpasajero.transcalderonpasajero.MainActivity;
import transcalderonpasajero.transcalderonpasajero.R;
import transcalderonpasajero.transcalderonpasajero.Splash;

public class VehiculosActivity extends Activity implements VehiculosAdapter.AdapterCallback{

    HorizontalListView listview;
    private Modelo modelo = Modelo.getInstance();
    private VehiculosAdapter mAdapter;
    String vehiculo = "";

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_vehiculos);


        listview = (HorizontalListView) findViewById(R.id.listview);

        if (savedInstanceState != null){

            Intent i = new Intent(getApplicationContext(), Splash.class);
            startActivity(i);
            finish();
            return;

        }




        displayVehiculos();

        listview.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                modelo.selectPosition = i;
                vehiculo = modelo.listavehiculos.get(i).getNombre();
                mAdapter.setSelected(i, true);
                modelo.selectPositionAnterior = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("OPT","RECARGAR");
    }


    public void atras(){
        finish();
    }


    public void atras(View view) {

        atras();
    }



    private void displayVehiculos() {

        mAdapter = new VehiculosAdapter(this, this);
        listview.setAdapter(mAdapter);
    }


    public void aceptar (View v){
        if(vehiculo.equals("")){
            Toast.makeText(getApplicationContext(),"Selecione un vehiculo", Toast.LENGTH_LONG).show();
        }else{
            modelo.currentService.tipoVehiculo = vehiculo;
            CmdOrden.saveCurrentService();
            atras();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        CmdOrden.getOrdenesPasajero(new OnOrdenesListener() {
            @Override
            public void nueva() {

            }


            @Override
            public void modificada(String idServicio , boolean cambioEstado) {

                if (cambioEstado){

                    Servicio servicio  = modelo.getOrdenById(idServicio);
                    if (servicio == null){
                        return;
                    }

                    android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(
                            VehiculosActivity.this);
                    alertDialogBuilder.setTitle("Estado de servicio");
                    alertDialogBuilder
                            .setMessage("El servicio " + servicio.cosecutivoOrden + " cambió de estado, se encuentra " + servicio.getEstadoLeible())
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    return;
                                }
                            });

                    // create alert dialog
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                }

            }


            @Override
            public void eliminada() {

            }
        });


    }


    @Override
    protected void onPause() {
        super.onPause();
        modelo.pararListeners();

    }


}
